﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Models.Registrar
{
    public class RegistrarClass
    {
        public List<UserViewModel> GetAllUser()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<UserViewModel> usrs = (from u in obj.tblUsers
                                            join t in obj.tblUserTypes
                                            on u.UserTypeId equals t.UserTypeId
                                            join e in obj.tblEvents
                                            on u.EventId equals e.EventId
                                            where u.UserTypeId == (int)(UserTypeEnum.Registrar)
                                            select new UserViewModel
                                            {
                                                UId = u.UserId,
                                                Username = u.UserName,
                                                UserType = t.Name,
                                                From = u.ValidFrom,
                                                To = u.ValidTo,
                                                EventName = e.EventTitle,
                                                Status = u.IsActive == true ? "Active" : "In Active"
                                            }).ToList();
                foreach (var usr in usrs)
                {
                    if (usr.From != null)
                        usr.ValidFrom = usr.From.Value.ToString("MM/dd/yyyy");
                    else
                        usr.ValidFrom = "";
                    if (usr.To != null)
                        usr.ValidTo = usr.To.Value.ToString("MM/dd/yyyy");
                    else
                        usr.ValidTo = "";
                }
                return usrs;
            }
        }

        public class UserViewModel
        {
            public string Username { get; set; }
            public string UserType { get; set; }
            public int UserTypeId { get; set; }
            public int UId { get; set; }
            public string Password { get; set; }
            public bool IsActive { get; set; }
            public DateTime? From { get; set; }
            public DateTime? To { get; set; }
            public string ValidFrom { get; set; }
            public string ValidTo { get; set; }
            public string EventName { get; set; }
            public int EventId { get; set; }
            public string Status { get; set; }
        }

        public bool IsUserNameExist(string UName)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblUsers.Any(x => x.UserName == UName);
            }
        }
        public bool AddUser(UserViewModel objuser)
        {
            try
            {
                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    var usr = new tblUser();
                    usr.UserId = objuser.UId;
                    usr.UserName = objuser.Username;
                    usr.FullName = objuser.Username;
                    usr.IsActive = objuser.IsActive;
                    usr.Password = Cryptography.passwordEncrypt(objuser.Password);
                    usr.UserTypeId = objuser.UserTypeId;
                    usr.ValidFrom = objuser.From;
                    usr.ValidTo = objuser.To;
                    usr.EventId = objuser.EventId;
                    usr.UserEmailId = objuser.Username;
                    obj.tblUsers.Add(usr);
                    if (obj.SaveChanges() > 0)
                    {
                        SendRegistrationEmail(objuser.Username, objuser.Username, objuser.Password, objuser.From.ToString(), objuser.To.ToString());
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SendRegistrationEmail(string Name, string EmailId, string pwd,string fromdate, string todate)
        {
            string subject = "Registration Successful | V Net";
            string body = @"Dear " + Name + ", <br />Your registrar account has been created successfully. Your login credentials are: <br />  Email ID: " + EmailId + "<br /> Password: " + pwd + "<br /> Note: Your account will be active from "+ fromdate+" to "+ todate+" <br /> Best Regards,<br /> VNet";
            bool isSent = MailSender.SendEmail(EmailId, subject, body);
            return isSent;
        }

        public bool ModifyUser(int uid, string uname, string password, int eventId, DateTime from, DateTime to,bool Active)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UserId == uid))
                {
                    tblUser usr = obj.tblUsers.FirstOrDefault(x => x.UserId == uid);
                    //usr.UserName = uname;
                    if (password != null && password.Trim().Length > 0)
                        usr.Password = Cryptography.passwordEncrypt(password);
                    usr.EventId = eventId;
                    usr.ValidFrom = from;
                    usr.ValidTo = to;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public bool DeleteUser(int uid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UserId == uid))
                {
                    tblUser usr = obj.tblUsers.FirstOrDefault(x => x.UserId == uid);
                    obj.tblUsers.Remove(usr);
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public UserViewModel GetUser(int uid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UserId == uid))
                {
                    UserViewModel usr = (from u in obj.tblUsers
                                         where u.UserId == uid
                                         select new UserViewModel
                                         {
                                             UId = u.UserId,
                                             Username = u.UserName,
                                             IsActive = u.IsActive.Value,
                                             From = u.ValidFrom,
                                             To = u.ValidTo,
                                             EventId = u.EventId.Value
                                         }).FirstOrDefault();
                    usr.ValidFrom = usr.From.Value.ToString("MM/dd/yyyy");
                    usr.ValidTo = usr.To.Value.ToString("MM/dd/yyyy");
                    return usr;
                }
                return null;
            }
        }
    }
}
