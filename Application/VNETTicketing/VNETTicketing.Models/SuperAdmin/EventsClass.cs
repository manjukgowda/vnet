﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Models.SuperAdmin
{
    public class EventsClass
    {
        CurrencyClass objcurr;
        public List<EventViewModel> GetAllEvents()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<EventViewModel> events = (from e in obj.tblEvents
                                               where e.IsActive == true
                                               select new EventViewModel
                                              {
                                                  EventDescription = e.EventDescription,
                                                  EventName = e.EventTitle,
                                                  EventStartDate = e.EventStartDate.Value,
                                                  EventId = e.EventId,
                                                  Venue = e.VenueDetails,
                                                  EventEndDate = e.EventEndDate.Value,
                                                  IsActive = e.IsActive.Value,
                                                  Status = e.Status,
                                                  AdminCharge = e.AdminCharge,
                                                  Address1 = e.Address1,
                                                  Address2 = e.Address2,
                                                  City = e.City,
                                                  PostalCode = e.PostalCode,
                                                  TotalTickets = e.TotalTickets
                                              }).OrderByDescending(x => x.EventId).ToList();
                foreach (var evt in events)
                {
                    evt.strEventStartDate = evt.EventStartDate.ToString("MM/dd/yyyy");
                    evt.strEventEndDate = evt.EventEndDate.ToString("MM/dd/yyyy");
                }
                return events;
            }
        }

        public string GetEventConfirmationTemplate(int EventId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.ConfirmationLetterTemplates.Any(x => x.EventId == EventId))
                {
                    return obj.ConfirmationLetterTemplates.FirstOrDefault(x => x.EventId == EventId).Template;
                }
                return "";
            }
        }


        public void AddEventConfirmationTemplate(int EventId,string Template)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.ConfirmationLetterTemplates.Any(x => x.EventId == EventId))
                {
                    var temp = obj.ConfirmationLetterTemplates.FirstOrDefault(x => x.EventId == EventId);
                    temp.Template = Template;
                    obj.SaveChanges();
                }
                else
                {
                    var temp = new ConfirmationLetterTemplate();
                    temp.EventId = EventId;
                    temp.Template = Template;
                    obj.ConfirmationLetterTemplates.Add(temp);
                    obj.SaveChanges();
                }
            }
        }
        public bool DeleteEvent(int EventId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEvents.Any(x => x.EventId == EventId))
                {
                    tblEvent eve = obj.tblEvents.FirstOrDefault(x => x.EventId == EventId);
                    //obj.tblEvents.Remove(eve);
                    eve.IsActive = false;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public class EventViewModel
        {
            public int PriceId { get; set; }
            public string EventName { get; set; }
            //public DateTime? EventStartDate { get; set; }
            public DateTime EventStartDate { get; set; }
            public string strEventStartDate { get; set; }
            public int? EventId { get; set; }
            public string EventDescription { get; set; }
            //public DateTime? EventEndDate { get; set; }
            public DateTime EventEndDate { get; set; }
            public string strEventEndDate { get; set; }
            public string Venue { get; set; }
            //public DateTime? BookingStartDate { get; set; }
            public string BookingStartDate { get; set; }
            //public DateTime? BookingEndDate { get; set; }
            public string BookingEndDate { get; set; }
            public string AdminCharge { get; set; }
            public string Status { get; set; }
            public bool TicketTypeStatus { get; set; }
            public bool IsActive { get; set; }
            public string Venuename { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string AdminTransferFee { get; set; }
            public string City { get; set; }
            public string PostalCode { get; set; }
            public long? TotalTickets { get; set; }

            public string EventPrefix { get; set; }
            public long StartBookingId { get; set; }
            public long StartTicketId { get; set; }
            public int CurrencyMode { get; set; }
            public string CurrencyName { get; set; }

            public string PromoName { get; set; }
            public string PromoPrice { get; set; }
            public int PromoCurrencyMode { get; set; }
            public string PromoCurrencyName { get; set; }
            public string PromoAllocation { get; set; }
            public string PromoMinAmount { get; set; }

            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string GroupNames { get; set; }

            public string TicketType { get; set; }
            public string TicketTypePrice { get; set; }
        }


        public bool AddEventr(EventViewModel objEvent, ref int neweventid)
        {
            try
            {
                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    var usr = SessionUsers.GetCurrentUser(1);
                    int userId = Convert.ToInt32(usr.UserId);
                    var result = obj.tblEvents.Add(new tblEvent
                    {
                        AdminCharge = objEvent.AdminTransferFee,
                        //BookingEndDate = DateTime.Parse(objEvent.BookingEndDate),
                        IsActive = objEvent.IsActive,
                        //BookingStartDate = DateTime.Parse(objEvent.BookingStartDate),
                        EventDescription = objEvent.EventDescription,
                        EventEndDate = objEvent.EventEndDate,
                        EventStartDate = objEvent.EventStartDate,
                        Address1 = objEvent.Address1,
                        Address2 = objEvent.Address2,
                        City = objEvent.City,
                        PostalCode = objEvent.PostalCode,
                        AdminTransferFee = objEvent.AdminTransferFee,
                        EventTitle = objEvent.EventName,
                        Status = objEvent.Status,
                        VenueDetails = objEvent.Venuename,
                        UserId = userId,
                        CreatedDate = DateTime.Now,
                        LastModifiedDate = DateTime.Now,
                        EventPrefix = objEvent.EventPrefix,
                        StartBookingId = objEvent.StartBookingId,
                        CurrentBookingId = 0,
                        StartTicketId = objEvent.StartTicketId,
                        CurrentTicketId = 0,
                        AdminCurrencyMode = objEvent.CurrencyMode,
                        ImageName = objEvent.FileName,
                        ImagePath = objEvent.FilePath,
                        TotalTickets = 0
                    });
                    if (obj.SaveChanges() > 0)
                    {
                        neweventid = result.EventId;
                        string[] groups = objEvent.GroupNames.Split(',');
                        foreach (var group in groups)
                        {
                            tblGroup g = new tblGroup();
                            g.GroupName = group;
                            g.IsActive = true;
                            g.CreatedDate = DateTime.Now;
                            g.EventId = neweventid;
                            obj.tblGroups.Add(g);
                            obj.SaveChanges();
                        }
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool ModifyEvent(EventViewModel objData)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEvents.Any(x => x.EventId == (int)(objData.EventId)))
                {
                    tblEvent evt = obj.tblEvents.FirstOrDefault(x => x.EventId == (int)(objData.EventId));
                    evt.EventTitle = objData.EventName;
                    evt.VenueDetails = objData.Venue;
                    evt.Address1 = objData.Address1;
                    evt.Address2 = objData.Address2;
                    evt.City = objData.City;
                    evt.PostalCode = objData.PostalCode;
                    evt.EventStartDate = objData.EventStartDate;
                    evt.EventEndDate = objData.EventEndDate;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public EventViewModel GetEvent(int EventId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEvents.Any(x => x.EventId == EventId))
                {
                    EventViewModel usr = (from u in obj.tblEvents
                                          where u.EventId == EventId && u.IsActive == true
                                          select new EventViewModel
                                          {
                                              EventId = u.EventId,
                                              EventDescription = u.EventDescription,
                                              EventName = u.EventTitle,
                                              Venuename = u.VenueDetails,
                                              Address1 = u.Address1,
                                              Address2 = u.Address2,
                                              City = u.City,
                                              //AdminCharge = u.AdminCharge,
                                              // EventFee = u.EventPrice,
                                              PostalCode = u.PostalCode,
                                              EventStartDate = u.EventStartDate.Value,
                                              EventEndDate = u.EventEndDate.Value,
                                              FileName = u.ImageName,
                                              FilePath = u.ImagePath
                                              //BookingStartDate = u.BookingStartDate.ToString(),
                                              //BookingEndDate = u.BookingEndDate.ToString()
                                          }).FirstOrDefault();
                    usr.strEventEndDate = usr.EventEndDate.ToString("MM/dd/yyyy");
                    usr.strEventStartDate = usr.EventStartDate.ToString("MM/dd/yyyy");
                    return usr;
                }
                return null;
            }
        }

        public bool AddEventPromo(EventViewModel objevent)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblDiscountCoupons.Any(x => x.EventId == objevent.EventId && x.Coupon == objevent.PromoName))
                {
                    return false;
                }
                else
                {

                    obj.tblDiscountCoupons.Add(new tblDiscountCoupon { Coupon = objevent.PromoName, CouponCurrencyMode = objevent.PromoCurrencyMode, CouponValue = objevent.PromoPrice, TotalAllocation = objevent.PromoAllocation, EventId = objevent.EventId, Status = "1", MinimumAmount = objevent.PromoMinAmount });
                    if (obj.SaveChanges() > 0)
                    {
                        return true;
                    }
                    else { return false; }
                }
            }
        }

        public List<EventViewModel> LoadEventPromo(int eventid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<EventViewModel> objresult = (from d in obj.tblDiscountCoupons
                                                  join c in obj.tblCurrencyModes
                                                  on d.CouponCurrencyMode equals c.CurrencyModeId
                                                  where d.EventId == eventid
                                                  select new EventViewModel
                                                  {
                                                      PromoName = d.Coupon,
                                                      PromoCurrencyName = c.CurrencyName,
                                                      PromoPrice = d.CouponValue,
                                                      PromoAllocation = d.TotalAllocation,
                                                      PromoMinAmount = d.MinimumAmount
                                                  }).ToList();
                return objresult;
                //foreach (var item in result)
                //{
                //    objresult.Add(new EventViewModel { PromoName = item.Coupon, PromoCurrencyName = "$ USD", PromoPrice = item.CouponValue, PromoAllocation = item.TotalAllocation });
                //}
            }

        }

        public bool AddEventTicketType(EventViewModel objevent)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (!obj.tblEventTicketTypePrices.Any(x => x.TypeOfTicket == objevent.TicketType && x.EventId == objevent.EventId))
                {
                    obj.tblEventTicketTypePrices.Add(new tblEventTicketTypePrice
                    {
                        CurrencyModeId = objevent.CurrencyMode,
                        EventId = objevent.EventId,
                        Price = objevent.TicketTypePrice,
                        TypeOfTicket = objevent.TicketType,
                        Status = true
                    });
                    if (obj.SaveChanges() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public List<EventViewModel> LoadEventTicketType(int eventid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<EventViewModel> objresult = (from d in obj.tblEventTicketTypePrices
                                                  join c in obj.tblCurrencyModes
                                                  on d.CurrencyModeId equals c.CurrencyModeId
                                                  where d.EventId == eventid && d.Status == true
                                                  select new EventViewModel
                                                  {
                                                      TicketType = d.TypeOfTicket,
                                                      TicketTypePrice = d.Price,
                                                      CurrencyName = c.CurrencyName,
                                                      PriceId = d.PriceId,
                                                      TicketTypeStatus = (bool)d.Status
                                                  }).ToList();
                return objresult;
                //foreach (var item in result)
                //{
                //    objresult.Add(new EventViewModel { PromoName = item.Coupon, PromoCurrencyName = "$ USD", PromoPrice = item.CouponValue, PromoAllocation = item.TotalAllocation });
                //}
            }
        }

        public List<EventViewModel> LoadEventTicketTypeSuperAdmin(int eventid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<EventViewModel> objresult = (from d in obj.tblEventTicketTypePrices
                                                  join c in obj.tblCurrencyModes
                                                  on d.CurrencyModeId equals c.CurrencyModeId
                                                  where d.EventId == eventid //&& d.Status == true
                                                  select new EventViewModel
                                                  {
                                                      TicketType = d.TypeOfTicket,
                                                      TicketTypePrice = d.Price,
                                                      CurrencyName = c.CurrencyName,
                                                      PriceId = d.PriceId,
                                                      TicketTypeStatus = (bool)d.Status
                                                  }).ToList();
                return objresult;
                //foreach (var item in result)
                //{
                //    objresult.Add(new EventViewModel { PromoName = item.Coupon, PromoCurrencyName = "$ USD", PromoPrice = item.CouponValue, PromoAllocation = item.TotalAllocation });
                //}
            }
        }

        public bool RemoveTicketPrice(int priceID)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEventTicketTypePrices.Any(x => x.PriceId == priceID))
                {
                    var price = obj.tblEventTicketTypePrices.FirstOrDefault(x => x.PriceId == priceID);
                    price.Status = false;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool IsEventExist(string eventname)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblEvents.Any(x => x.EventTitle == eventname);
            }
        }

        public bool EnableTicketPrice(int priceID)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEventTicketTypePrices.Any(x => x.PriceId == priceID))
                {
                    var price = obj.tblEventTicketTypePrices.FirstOrDefault(x => x.PriceId == priceID);
                    price.Status = true;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}