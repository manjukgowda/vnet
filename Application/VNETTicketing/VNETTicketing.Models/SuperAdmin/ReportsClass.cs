﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.SuperAdmin
{
    public class ReportsClass
    {
        public List<TicketsDetails> GetAllTickets()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<TicketsDetails> usrs = (from t in obj.tblBookingDetails
                                             join e in obj.tblEvents
                                             on t.EventId equals e.EventId
                                             join g in obj.tblGroups
                                             on t.GroupId equals g.GroupId
                                             select new TicketsDetails
                                             {
                                                 BookingId = t.BookingID,
                                                 EventId = t.EventId.Value,
                                                 EventTitle= e.EventTitle,
                                                 GroupId = t.GroupId.Value,
                                                 GroupName = g.GroupName,
                                                 NoOfTickets = t.NoOfTickets.Value,
                                                 BookedBy = t.BookedBy,
                                                 PaymentMode = t.PaymentMode,
                                                 TotalAmount = t.TotalAmount,
                                                 PromoCodeApplied = t.PromoCodeUsed,
                                                 Discount = t.Discount,
                                                 NetAmount = t.NetAmountPaid,
                                                 BookingDateTime = t.BookingDateTime.Value,
                                                 Status = t.Status
                                             }).ToList();
                foreach (var usr in usrs)
                {
                    usr.DateTime = usr.BookingDateTime.ToString("MM.dd.yyyy hh:mm tt");
                }
                return usrs;
            }
        }

        public class TicketsDetails
        {
            public long BookingId { get; set; }
            public int EventId { get; set; }
            public string EventTitle { get; set; }
            public int GroupId { get; set; }
            public string GroupName { get; set; }
            public decimal NoOfTickets { get; set; }
            public string BookedBy { get; set; }
            public string PaymentMode { get; set; }
            public decimal? TotalAmount { get; set; }
            public string PromoCodeApplied { get; set; }
            public decimal? Discount { get; set; }
            public decimal? NetAmount { get; set; }
            public DateTime BookingDateTime { get; set; }
            public string DateTime { get; set; }
            public string Status { get; set; }
        }
    }
}
