﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Models.SuperAdmin
{
    public class AdminClass
    {
        public List<UserViewModel> GetAllUser()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<UserViewModel> usrs = (from u in obj.tblUsers
                                            join t in obj.tblUserTypes
                                            on u.UserTypeId equals t.UserTypeId
                                            where u.UserTypeId == (int)(UserTypeEnum.Admin)
                                            select new UserViewModel
                                            {
                                                UId = u.UID,
                                                Username = u.UserName,
                                                UserType = t.Name,
                                                Status = u.IsActive == true ? "Active" : "In Active"
                                            }).ToList();
                return usrs;
            }
        }

        public class UserViewModel
        {
            public string Username { get; set; }
            public string UserType { get; set; }
            public int UserTypeId { get; set; }
            public string UId { get; set; }
            public string Password { get; set; }
            public string Status { get; set; }
            public bool IsActive { get; set; }
        }

        public bool IsUserIdExist(string UId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblUsers.Any(x => x.UID == UId);
            }
        }

        public bool IsEmailIdExist(string Email)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblUsers.Any(x => x.UserEmailId == Email || x.UserName == Email);
            }
        }

        public bool AddUser(UserViewModel objuser)
        {
            try
            {
                string password = Cryptography.passwordEncrypt(objuser.Password);
                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    obj.tblUsers.Add(new tblUser
                    {
                        UID = objuser.UId,
                        UserName = objuser.Username,
                        UserEmailId = objuser.Username,
                        FullName = objuser.Username,
                        IsActive = objuser.IsActive,
                        Password = password,
                        UserTypeId = objuser.UserTypeId
                    });
                    if (obj.SaveChanges() > 0)
                    {
                        SendRegistrationEmail(objuser.UId, objuser.Username, objuser.Password);
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SendRegistrationEmail(string Name, string EmailId,string pwd)
        {
            string subject = "Registration Successful | V Net";
            string body = @"Dear " + Name + ", <br />Your administrator account has been created successfully. Your login credentials are: <br />  Email ID: " + EmailId + "<br /> Password: "+ pwd+"<br />  Best Regards,<br /> VNet";
            bool isSent = MailSender.SendEmail(EmailId, subject, body);
            return isSent;
        }

        public bool ModifyUser(string uid, string uname, string password)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UID == uid))
                {
                    tblUser usr = obj.tblUsers.FirstOrDefault(x => x.UID == uid);
                    usr.UserName = uname;
                    usr.UserEmailId = uname;
                    usr.Password = Cryptography.passwordEncrypt(password);
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public bool DeleteUser(string uid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UID == uid))
                {
                    tblUser usr = obj.tblUsers.FirstOrDefault(x => x.UID == uid);
                    usr.UserTypeId = 4; 
                   // obj.tblUsers.Remove(usr);
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public UserViewModel GetUser(string uid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UID == uid))
                {
                    UserViewModel usr = (from u in obj.tblUsers
                                         where u.UID == uid
                                         select new UserViewModel
                                         {
                                             UId = u.UID,
                                             Username = u.UserName,
                                             IsActive = u.IsActive.Value
                                         }).FirstOrDefault();
                    return usr;
                }
                return null;
            }
        }
    }
}
