﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.SuperAdmin
{
    public class CurrencyClass
    {
        public class CurrencyViewModel
        {
            public int CurrencyModeId { get; set; }
            public string CurrencyName { get; set; }
            public string CurrenctRate { get; set; }
            public string IsBase { get; set; }
        }

        public List<CurrencyViewModel> GetAllCurrency()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                List<CurrencyViewModel> currs = (from e in obj.tblCurrencyModes
                                                 select new CurrencyViewModel
                                              {
                                                  CurrencyName = e.CurrencyName,
                                                  CurrenctRate = e.Rate,
                                                  IsBase = e.IsBase,
                                                  CurrencyModeId = e.CurrencyModeId
                                              }).OrderByDescending(x => x.IsBase).ToList();
                return currs;
            }
        }

        public decimal ConvertFromBaseToActual(int fromModeId, long rate, int toModeId)
        {
            try
            {
                // base RM
                // from mode = USD
                // rate = 10
                // to mode = SGD
                // to rate = 14.12


                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    List<CurrencyViewModel> currs = (from e in obj.tblCurrencyModes
                                                     select new CurrencyViewModel
                                                     {
                                                         CurrencyName = e.CurrencyName,
                                                         CurrenctRate = e.Rate,
                                                         IsBase = e.IsBase,
                                                         CurrencyModeId = e.CurrencyModeId
                                                     }).OrderByDescending(x => x.IsBase).ToList();

                    decimal baseConversionRate = Convert.ToDecimal(currs.FirstOrDefault(x => x.CurrencyModeId == fromModeId).CurrenctRate);
                    // 0.23
                    decimal toConversionRate = Convert.ToDecimal(currs.FirstOrDefault(x => x.CurrencyModeId == toModeId).CurrenctRate);
                    // 0.33
                    decimal whenItisConvertedToBase = rate / baseConversionRate;
                    string strWhenItisConvertedToBase = string.Format("{0:0.00}", whenItisConvertedToBase);
                    decimal whenItisConvertedToActual = whenItisConvertedToBase * toConversionRate;
                    string strWhenItisConvertedToActual = string.Format("{0:0.00}", whenItisConvertedToActual);
                    return Convert.ToDecimal(strWhenItisConvertedToActual);
                }
            }
            catch { return Convert.ToDecimal("99999999.99"); }
        }
    }
}
