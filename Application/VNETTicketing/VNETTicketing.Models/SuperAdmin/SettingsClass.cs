﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.SuperAdmin
{
    public class SettingsClass
    {
        public bool UpdateEvent(SettingsViewModel objuser)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblEvents.Any(x => x.EventId.Equals(objuser.EventId))) 
                {
                    tblEvent evt = obj.tblEvents.FirstOrDefault(x => x.EventId == objuser.EventId);
                    evt.TotalTickets = Convert.ToInt64(objuser.TicketReserved);
                    obj.SaveChanges();

                    var events = obj.tblTicketsDetails.Where(x => x.EventId == objuser.EventId);
                    foreach (var et in events)
                    {
                        obj.tblTicketsDetails.Remove(et);
                    }
                    obj.SaveChanges();
                    long startTicketId = evt.StartTicketId.Value;
                    for (int i = 1; i <= evt.TotalTickets; i++)
                    {
                        tblTicketsDetail ticket = new tblTicketsDetail();
                        ticket.EventId = objuser.EventId;
                        ticket.Status = "Not Booked";
                        ticket.TicketId = evt.EventPrefix + startTicketId;
                        obj.tblTicketsDetails.Add(ticket);
                        startTicketId++;
                    }
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool UpdateCurrency(int CurrencyId, string Rate)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblCurrencyModes.Any(x => x.CurrencyModeId == CurrencyId))
                {
                    var cur = obj.tblCurrencyModes.FirstOrDefault(x => x.CurrencyModeId == CurrencyId);
                    cur.Rate = Rate;
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

       

        public class SettingsViewModel
        {
            public int EventId { get; set; }
            public string TicketReserved { get; set; }
        }
    }
}
