﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class BarCodeHelper
    {
        public byte[] GenerateBarCode(string Code, string path)
        {
            string BarCode = "";
            for (int i = Code.Length; i <= 8; i++)
            {
                BarCode += "0";
            }
            BarCode += Code;
            Bitmap bitmap = new Bitmap(BarCode.Length * 40, 150);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                Font oFont = new Font("IDAutomationHC39M", 20);
                PointF oPoint = new PointF(2f, 2f);
                SolidBrush black = new SolidBrush(Color.Black);
                SolidBrush white = new SolidBrush(Color.White);
                graphics.FillRectangle(white, 0, 0, bitmap.Width, bitmap.Height);
                graphics.DrawString("*" + BarCode + "*", oFont, black, oPoint);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Bmp);
                bitmap.Save(path);
                byte[] bytes = new byte[ms.Length];
                ms.Write(bytes, 0, bytes.Length);
                return bytes;
            }
        }
    }
}
