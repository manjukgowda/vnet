﻿namespace VNETTicketing.Models.GeneralUtilities
{
    public enum UserTypeEnum
    {
        SuperAdmin = 1,
        Admin = 2,
        Registrar = 3,
        EndUser = 4
    }
}
