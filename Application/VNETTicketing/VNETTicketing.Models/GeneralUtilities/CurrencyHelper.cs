﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class CurrencyHelper
    {
        public List<tblCurrencyMode> GetAllCurrencyMode()
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                return db.tblCurrencyModes.ToList();
            }
        }

        public static decimal ConvertBaseCurrency(int fromCurrencty, decimal value)
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                var list=  db.tblCurrencyModes.ToList();
                decimal conversionRate = Convert.ToDecimal( list.FirstOrDefault(x => x.CurrencyModeId == fromCurrencty).Rate);
                return  value * conversionRate;

            }
        }

        //public enum CurrencyModeTypee = 
        //{
            
        //}
    }
}
