﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mail;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.EMailService;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class MailSender
    {
        public static bool SendEmail(string pTo, string pSubject, string pBody)
        {
            try
            {
                Service1SoapClient obj = new Service1SoapClient();
                string FromAddr = ConfigurationManager.AppSettings["FromAddr"].ToString();
                string BccAddr = ConfigurationManager.AppSettings["BccAddr"].ToString();
                string CCAddr = ConfigurationManager.AppSettings["CCAddr"].ToString();
                // string Subject = ConfigurationManager.AppSettings["Subject"].ToString();

                obj.eMailSendOut(FromAddr, pTo, CCAddr, BccAddr, pSubject, pBody, "");
                tblEmailTracker email = new tblEmailTracker();
                email.EmailBody = pBody;
                email.CreatedDate = DateTime.Now;
                email.EmailSubject = pSubject;
                email.ReceipientEmailId = pTo;
                VNETTicketEntities db = new VNETTicketEntities();
                email.EmailStatus = "Sent";
                db.tblEmailTrackers.Add(email);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                tblEmailTracker email = new tblEmailTracker();
                email.EmailBody = ex.Message + "-innerexception-" + ex.InnerException + "-Stack-" + ex.StackTrace;
                email.CreatedDate = DateTime.Now;
                email.EmailSubject = pSubject;
                email.ReceipientEmailId = pTo;
                VNETTicketEntities db = new VNETTicketEntities();
                email.EmailStatus = "Not Sent";
                db.tblEmailTrackers.Add(email);
                db.SaveChanges();
                return false;
            }
        }

        public static void SendTicketConfirmationEmail(string bookId, string EventName)
        {
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);
            foreach (var item in objdetails.BookingUserDetail)
            {
                string server = ConfigurationManager.AppSettings["server"].ToString();
                var link = "<a href='" + server + "/DownloadETicket.aspx?eid=" + bookId + "&tid=" + item.TicketId + "'> Click here </a>";
                string body = @"Dear " + item.UName + ", <br /><br />Your tickets are confirmed for the event " + EventName + ".<br /><br />Please " + link + @" to download the tickets and kindly carry the print out of the e-tickets to the venue for confirmation of ticket. <br /><br />
                

<br /> Best Regards,<br /> VNet";


                SendEmail(item.EmailId, "Your tickets", body);
            }
        }

        public static void SendTicketFailureEmail(string bookId,  string EventName)
        {
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);
            foreach (var item in objdetails.BookingUserDetail)
            {
                string subject = "Your payment is failed - " + bookId;
                string server = ConfigurationManager.AppSettings["server"].ToString();
                var link = "<a href='" + server + "/DownloadETicket.aspx?eid=" + bookId + "&tid=" + item.TicketId + "'> Click here </a>";
                string body = @"Dear " +item.UName + ", <br /><br />You payment has been not completed for the order id " + bookId + " for the event " + EventName + ".<br /><br/>Kindly complete the payment to confirm your tickets.<br/><br />Please " + link + @" to check the status of your transction/download the tickets and kindly carry the print out of the e-tickets to the venue for confirmation of ticket. <br /><br />
            
<br /> Best Regards,<br /> VNet";


                SendEmail(item. EmailId, subject, body);
            }

        }

        public static void SendTransactionIntiationMsg(string bookId, string EventName)
        {
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);
            foreach (var item in objdetails.BookingUserDetail)
            {
                string subject = "Payment initiated to book tickets - " + bookId;
                string server = ConfigurationManager.AppSettings["server"].ToString();
                var link = "<a href='" + server + "/DownloadETicket.aspx?eid=" + bookId + "&tid=" + objdetails.BookingUserDetail[0].TicketId + "'> Click here </a>";
                string body = @"Dear " + item.UName + ", <br /><br />You have successfully initiated the payment for the order id " + bookId + " for the event " + EventName + ".<br /><br/>Kindly complete the payment to confirm your tickets.<br/><br />Please " + link + @" to check the status of your transction/download the tickets and kindly carry the print out of the e-tickets to the venue for confirmation of ticket. <br /><br />
                
<br /> Best Regards,<br /> VNet";


                SendEmail(item.EmailId, subject, body);
            }


        }

    }
}