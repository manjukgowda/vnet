﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class DashboardHelper
    {

        public class MorisAreaData
        {
            public decimal? Users { get; set; }
            public string EventTitle { get; set; }
        }

        public class CollectionsData : MorisAreaData
        {
            public decimal Amount { get; set; }
            public int EventID { get; set; }
        }

        public int GetEventsCount()
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblEvents.ToList().Count;
        }
        public int GetAdminsCount()
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblUsers.Where(x => x.UserTypeId == 2).ToList().Count;
        }
        public int GetRegistrarsCount()
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblUsers.Where(x => x.UserTypeId == 3).ToList().Count;
        }

        public List<MorisAreaData> GetMorisAreaData()
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                var md = (from e in db.tblEvents
                          join bd in db.tblBookingDetails
                          on e.EventId equals bd.EventId
                          select new MorisAreaData
                          {
                              EventTitle = e.EventTitle,
                              Users = bd.NoOfTickets
                          }).ToList();
                return md;
            }
        }

        public List<CollectionsData> GetEventsCollections()
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                var events = from e in db.tblEvents
                             join bd in db.tblBookingDetails
                             on e.EventId equals bd.EventId
                             select new
                             {
                                 e.EventId,
                                 e.EventTitle,
                                 bd.NetAmountPaid,
                                 bd.NoOfTickets
                             };
                var md = from bd in events
                         group bd by bd.EventId into g
                         let amount = g.Sum(bd => bd.NetAmountPaid)
                         let users = g.Sum(bd => bd.NoOfTickets)
                         select new
                         {
                             eventid = g.Key,
                             amount = amount.Value,
                             users = users,
                             eventtitle = ""
                         };
                var eventsList = (from e in events
                                  join b in md
                                  on e.EventId equals b.eventid
                                  select new CollectionsData
                                  {
                                      EventID = e.EventId,
                                      EventTitle = e.EventTitle,
                                      Amount = b.amount,
                                      Users = b.users
                                  }).Distinct().ToList();
                return eventsList;
            }
        }

        public decimal GetTotalCollections()
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                var md = (from bd in db.tblBookingDetails
                          group bd by bd into g
                          let amount = g.Sum(bd => bd.NetAmountPaid)
                          select new
                          {
                              amount = amount.Value,
                          }).ToList();
                decimal total = 0;
                foreach (var item in md)
                {
                    total = total + item.amount;
                }
                return total;
            }
        }

        public DashboardStats GetAdminDashboardData()
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                DashboardStats ds = new DashboardStats();
                if (db.tblEvents.Any())
                {
                    ds.TotalTickets = db.tblEvents.Where(x => x.IsActive == true).Sum(x => x.TotalTickets).Value;
                }

                else
                {
                    ds.TotalTickets = 0;
                }
                if (db.tblBookingUserDetails.Any())
                {
                    ds.TotalAmount = db.tblBookingDetails.Sum(e => e.NetAmountPaid).Value;
                }
                else
                {
                    ds.TotalAmount = 0;
                }
                ds.ValidatedTickets = db.tblBookingUserDetails.Where(x => x.Status == "Confirmed" && x.ValidationStatus == "Validated").Count();
                ds.InvalidatedTickets = db.tblBookingUserDetails.Where(x => x.Status == "Confirmed" && x.ValidationStatus == "Invalidated").Count();
                ds.SoldTickets = db.tblBookingUserDetails.Where(x => x.Status == "Confirmed").Count();
                ds.UnSoldTickets = Convert.ToInt32(ds.TotalTickets) - ds.SoldTickets;
                return ds;
            }
        }

        public DashboardStats GetRegistrarDashboardData(int eventid)
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                DashboardStats ds = new DashboardStats();
                if (db.tblEvents.Any(x => x.EventId == eventid))
                {
                    ds.TotalTickets = db.tblEvents.FirstOrDefault(x => x.EventId == eventid).TotalTickets.Value;
                }
                else
                {
                    ds.TotalTickets = 0;
                }
                if (db.tblBookingUserDetails.Any(e => e.EventId == eventid && e.Status == "Confirmed"))
                {
                    ds.TotalAmount = db.tblBookingDetails.Where(e => e.EventId == eventid && e.Status == "Confirmed").Sum(e => e.NetAmountPaid).Value;
                }
                else
                {
                    ds.TotalAmount = 0;
                }
                ds.ValidatedTickets = db.tblBookingUserDetails.Where(x => x.EventId == eventid && x.Status == "Confirmed" && x.ValidationStatus == "Validated").Count();
                ds.InvalidatedTickets = db.tblBookingUserDetails.Where(x => x.EventId == eventid && x.Status == "Confirmed" && x.ValidationStatus == "Invalidated").Count();
                ds.SoldTickets = db.tblBookingUserDetails.Where(x => x.EventId == eventid && x.Status == "Confirmed").Count();
                ds.UnSoldTickets = Convert.ToInt32(ds.TotalTickets) - ds.SoldTickets;
                return ds;
            }
        }
        public int GetEventIdByRegistrar(long UserID)
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                return db.tblUsers.FirstOrDefault(x => x.UserId == UserID).EventId.Value;
            }
        }
    }

    public class DashboardStats
    {
        public long TotalTickets { get; set; }
        public int ValidatedTickets { get; set; }
        public int InvalidatedTickets { get; set; }
        public decimal TotalAmount { get; set; }
        public int SoldTickets { get; set; }
        public int UnSoldTickets { get; set; }
    }
}
