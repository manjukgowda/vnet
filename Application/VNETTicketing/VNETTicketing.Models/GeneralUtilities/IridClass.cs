﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.VConWS;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class IridClass
    {
        //VConWSSoapClient objws;
        public DataSet GetUserDetails(string useririd)
        {
            string wspassword = ConfigurationManager.AppSettings["iridpwd"].ToString();
            VConWSSoapClient objws = new VConWSSoapClient();
            return objws.wsGetIRInfo(wspassword, useririd);
        }
    }
}
