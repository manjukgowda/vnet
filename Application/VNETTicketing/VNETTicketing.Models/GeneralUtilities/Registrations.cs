﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class Registrations
    {
        Random r;
        public string AddGroup(string GroupName, string GroupLeader, string EmailId, int NoOfMembers)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblGroups.Any(x => x.GroupName == GroupName))
            {
                return "Group name is exist!";
            }
            else if (db.tblGroups.Any(x => x.EmailAddress == EmailId))
            {
                return "Email Id is exist!";
            }
            tblGroup grp = new tblGroup();
            grp.EmailAddress = EmailId;
            grp.NoOfMembers = NoOfMembers;
            grp.GroupName = GroupName;
            grp.GroupLeader = GroupLeader;
            grp.IsActive = true;
            grp.CreatedDate = DateTime.Now;
            grp.LastModifiedDate = DateTime.Now;
            db.tblGroups.Add(grp);
            db.SaveChanges();
            return "Success";
        }

        public List<tblGroup> LoadGroups()
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblGroups.ToList();
        }

        public bool RegisterUser(string Name, string Country, string EmailID, string password)
        {
            if (r == null)
                r = new Random();
            string code = r.Next(10000, 99999).ToString();
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblUsers.Any(x => x.UserEmailId == EmailID))
            {
                return false;
            }
            tblUser obj = new tblUser();
            obj.Country = Country;
            //obj.GroupId = Convert.ToInt32(GroupId);
            obj.IsActive = false;
            obj.UserEmailId = EmailID;
            obj.Password = Cryptography.passwordEncrypt(password);
            obj.UserName = EmailID;
            obj.FullName = Name;
            obj.LastModifiedDate = DateTime.Now;
            obj.CreatedDate = DateTime.Now;
            obj.EmailVerificationCode = code;
            obj.EmailVerified = false;
            obj.UserTypeId = 4;
            db.tblUsers.Add(obj);
            db.SaveChanges();
            SendRegistrationEmail(Name, EmailID, code);
            return true;
        }


        public bool SendRegistrationEmail(string Name, string EmailId, string Code)
        {
            string subject = "Registration Successful | V Net";
            string body = @"Dear " + Name + ", <br />Your Account for accessing Online Services provided by VNet, has been successfully created. <br />  Email verification code is :" + Code + "<br /> <br /> Best Regards,<br /> VNet";
            bool isSent = MailSender.SendEmail(EmailId, subject, body);
            return isSent;
        }

        public bool VerifyRegisteredUsersEmailID(string EmailId, string Code)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblUsers.Any(x => x.UserEmailId == EmailId && x.EmailVerificationCode ==Code && x.UserTypeId == 4))
            {
                tblUser usr = db.tblUsers.FirstOrDefault(x => x.UserEmailId == EmailId && x.EmailVerificationCode == Code && x.UserTypeId == 4);
                usr.EmailVerified = true;
                usr.IsActive = true;
                usr.EmailVerificationCode = "";
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public bool ForgetPassword(string EmailId)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblUsers.Any(x => x.UserName == EmailId && x.UserTypeId == 4))
            {
                var usr = db.tblUsers.FirstOrDefault(x => x.UserName == EmailId && x.UserTypeId == 4);
                var abc = Cryptography.passwordDecrypt(usr.Password);
                string subject = "Forgot Password | V Net";
                string body = @"Dear " + usr.FullName + ", <br />Please note make a note of your password: " + Cryptography.passwordDecrypt(usr.Password) + "<br /> <br /> Best Regards,<br /> VNet";
                bool isSent = MailSender.SendEmail(EmailId, subject, body);
                if (isSent)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
    }
}
