﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class SessionUsers 
    {
        public static SessionUser GetCurrentUser(int RoleId)
        {
            if (HttpContext.Current.Session["LoggedInUserId"] == null)
            {
                Logout();
            }
            int userId = Convert.ToInt32(HttpContext.Current.Session["LoggedInUserId"]);
            var context = new VNETTicketEntities();
            if (!context.tblUsers.Any(x => x.UserId == userId))
            {
                Logout();
            }

            SessionUser sessionUser = (from u in context.tblUsers
                                       where u.UserId == userId
                                       select new SessionUser
                                       {
                                           UserTypeId = u.UserTypeId,
                                           UserId = u.UserId,
                                           UserName = u.UserName,
                                           EmailId = u.UserEmailId
                                       }).FirstOrDefault();
            
            if (sessionUser.UserTypeId != RoleId )
            {
                Logout();
            }
            
            return sessionUser;

        }

        public static void Logout()
        {
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();

            HttpCookie cookie = new HttpCookie("ASP.NET_SessionId", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
            
            HttpContext.Current.Response.Redirect("/Login.aspx");
        }
    }

    public class SessionUser
    {
        public long UserId { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
    }

}
