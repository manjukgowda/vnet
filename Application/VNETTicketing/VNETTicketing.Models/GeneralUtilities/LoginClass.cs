﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.GeneralUtilities
{
    public class LoginClass
    {
        public bool IsValidUser(string UName, string Password, ref int UserTypeId, ref int UserId)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            string encryptedPass = Cryptography.passwordEncrypt(Password);
            if (db.tblUsers.Any(x => x.UserName == UName && x.Password == encryptedPass && x.IsActive == true))
            {
                var usr = (from u in db.tblUsers
                           where u.UserName == UName && u.Password == encryptedPass && u.IsActive == true
                           select new
                           {
                               u.UserId,
                               u.UserTypeId
                           }).FirstOrDefault();
                UserId = usr.UserId;
                UserTypeId = usr.UserTypeId;
                return true;
            }
            return false;
        }

        public bool IsValidUser(string UName, string Password)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            string encryptedPass = Cryptography.passwordEncrypt(Password);
            return db.tblUsers.Any(x => x.UserName == UName && x.Password == encryptedPass && x.UserTypeId == 4 && x.IsActive == true);
        }
    }
}
