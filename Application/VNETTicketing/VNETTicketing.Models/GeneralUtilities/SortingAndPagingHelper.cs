﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.GeneralUtilities
{
    public static class SortingAndPagingHelper
    {
        public static IEnumerable<TSource> SortingAndPaging<TSource>(this IEnumerable<TSource> source, SortingAndPagingInfo sortingModal)
        {
            // Gets the coloumn name that sorting to be done on
            PropertyInfo propertyInfo = source.GetType().GetGenericArguments()[0].GetProperty(sortingModal.SortColumnName);

            // sorts by ascending if sort criteria is Ascending otherwise sorts descending
            return sortingModal.SortOrder == "desc" ? source.OrderByDescending(x => propertyInfo.GetValue(x, null)).Skip(sortingModal.PageSelected).Take(sortingModal.PageSize)
                               : source.OrderBy(x => propertyInfo.GetValue(x, null)).Skip(sortingModal.PageSelected).Take(sortingModal.PageSize);
        }
    }

    public class SortingAndPagingInfo
    {
        public string SortColumnName { get; set; }
        public string SortOrder { get; set; }
        public int PageSelected { get; set; }
        public int PageSize { get; set; }
        public string SearchString { get; set; }
    }
}
