﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.User
{
    public class EventClass
    {
        public class EventViewModel
        {
            public int EventId { get; set; }
            public string EventName { get; set; }
            public string EventDescription { get; set; }
            public decimal EventPrice { get; set; }
            public string EventVenueDetail { get; set; }
            public List<TicketTypeViewModel> EventTicketType { get; set; }
            public List<EventGroupViewModel> EventGroup { get; set; }
            public string EventCurrencyType { get; set; }
            public string EventAddress1 { get; set; }

            public string EventAddress2 { get; set; }

            public string EventCity { get; set; }
            public string ImagePath { get; set; }
            public string ImageName { get; set; }
            public string EventPostalCode { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }

            public string EventTicketTypeSelected { get; set; }
            public int EventTicketTypeSelectedValue { get; set; }
            public string TicketCountSelected { get; set; }
            public string GroupSelected { get; set; }
            public string GroupSelectedValue { get; set; }

            public string PromoCodeApp { get; set; }
            public decimal NetAmount { get; set; }
            public decimal Discount { get; set; }
        }


        public class EventsServices
        {
            public int EventId { get; set; }
            public string EventName { get; set; }
            public string EventDescription { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string PostalCode { get; set; }
            public string City { get; set; }
            public string Venue { get; set; }
            public string ImagePath { get; set; }
            public string ImageName { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public long? TotalTickets { get; set; }
            public int AvailableTickets { get; set; }
            public List<TicketsPrices> TicketPrice { get; set; }
            public List<EventGroups> Groups { get; set; }
            public string PriceToDisplay { get; set; }
            public string Prefix { get; set; }
        }

        public class EventBookingDetail
        {
            public string BookingId { get; set; }
            public string IrId { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Country { get; set; }
            public string MyProperty { get; set; }
        }

        public class EventGroups
        {
            public int GroupId { get; set; }
            public string GroupName { get; set; }
        }

        public class TicketsPrices
        {
            public int TicketTypeId { get; set; }
            public string TicketType { get; set; }
            public string Currency { get; set; }
            public int CurrencyId { get; set; }
            public string TicketPrice { get; set; }
        }

        public class TicketTypeViewModel
        {
            public string TicketType { get; set; }
            public int CurrencyModeId { get; set; }
            public string TicketTypePrice { get; set; }
            public string TicketTypeCurrency { get; set; }
            public string EventPrice { get; set; }
        }

        public class EventGroupViewModel
        {
            public string GroupId { get; set; }
            public string GroupName { get; set; }
        }

        public class TicketsReport
        {
            public string EventName { get; set; }
            public DateTime EventDate { get; set; }
            public string BookingId { get; set; }
            public string BookingEventId { get; set; }
            public string TicketId { get; set; }
            public string TicketNo { get; set; }
            public string Price { get; set; }
            public string Status { get; set; }
            public string strBookingDate { get; set; }
            public int BookedUserId { get; set; }
        }

        public List<EventsServices> GetAllEventsToServices()
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {

                List<EventsServices> objresult = (from e in obj.tblEvents
                                                  join t in obj.tblEventTicketTypePrices
                                                  on e.EventId equals t.EventId
                                                  where (e.IsActive == true && t.Status == true && e.EventEndDate > DateTime.Now)
                                                  select new EventsServices
                                                  {
                                                      EventId = e.EventId,
                                                      EventName = e.EventTitle,
                                                      EventDescription = e.EventDescription,
                                                      Address1 = e.Address1,
                                                      Address2 = e.Address2,
                                                      City = e.City,
                                                      Venue = e.VenueDetails,
                                                      ImagePath = e.ImagePath,
                                                      ImageName = e.ImageName,
                                                      TotalTickets = e.TotalTickets,
                                                      StartDate = e.EventStartDate.Value,
                                                      EndDate = e.EventEndDate.Value,
                                                      PostalCode = e.PostalCode
                                                  }).OrderByDescending(x => x.EventId).ToList();

                bool flag = false;
            myreturn: List<EventsServices> objtemp = objresult;
                for (int i = 0; i < objresult.Count; i++)
                {
                    int temp = GetAvailableTicketsCount(objresult[i].EventId);
                    if (temp == 0)
                    {
                        objtemp.RemoveAll(x => x.EventId == objresult[i].EventId);
                        flag = false;
                        break;
                    }
                    else
                    {
                        flag = true;
                    }
                }

                objresult = objtemp;
                if (!flag && objresult.Count > 0)
                    goto myreturn;

                foreach (var eve in objresult)
                {
                    //int temp = GetAvailableTicketsCount(eve.EventId);
                    //if (temp == 0)
                    //objresult.Remove(eve);
                    //if (temp != 0)
                    {
                        if (obj.tblEventTicketTypePrices.Any(x => x.EventId == eve.EventId))
                        {
                            eve.TicketPrice = (from p in obj.tblEventTicketTypePrices
                                               join c in obj.tblCurrencyModes
                                               on p.CurrencyModeId equals c.CurrencyModeId
                                               where p.EventId == eve.EventId && p.Status == true
                                               select new TicketsPrices
                                               {
                                                   TicketPrice = p.Price,
                                                   Currency = c.CurrencyName,
                                                   CurrencyId = c.CurrencyModeId,
                                                   TicketType = p.TypeOfTicket,
                                                   TicketTypeId = p.PriceId
                                               }).ToList();
                            if (eve.TicketPrice.Count > 0)
                                eve.PriceToDisplay = eve.TicketPrice.FirstOrDefault().Currency + " " + eve.TicketPrice.FirstOrDefault().TicketPrice;
                        }
                        if (obj.tblGroups.Any(x => x.EventId == eve.EventId))
                        {
                            eve.Groups = (from g in obj.tblGroups
                                          where g.EventId == eve.EventId
                                          select new EventGroups
                                          {
                                              GroupId = g.GroupId,
                                              GroupName = g.GroupName
                                          }).ToList();
                        }
                        eve.AvailableTickets = obj.tblTicketsDetails.Where(x => x.EventId == eve.EventId && x.Status == "Not Booked").Count();
                    }
                }
                return objresult;
            }
        }

        public int GetAvailableTicketsCount(int eventId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblTicketsDetails.Where(x => x.EventId == eventId && x.Status == "Not Booked").Count();
            }
        }

        public EventsServices GetEventsDetailsByEventId(int eventId)
        {
            try
            {
                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    EventsServices eve = (from e in obj.tblEvents
                                          where (e.EventId == eventId)
                                          select new EventsServices
                                          {
                                              EventId = e.EventId,
                                              EventName = e.EventTitle,
                                              EventDescription = e.EventDescription,
                                              Address1 = e.Address1,
                                              Address2 = e.Address2,
                                              City = e.City,
                                              Venue = e.VenueDetails,
                                              ImagePath = e.ImagePath,
                                              ImageName = e.ImageName,
                                              TotalTickets = e.TotalTickets,
                                              StartDate = e.EventStartDate.Value,
                                              EndDate = e.EventEndDate.Value,
                                              PostalCode = e.PostalCode,
                                              Prefix = e.EventPrefix
                                          }).FirstOrDefault();

                    if (obj.tblEventTicketTypePrices.Any(x => x.EventId == eve.EventId))
                    {
                        eve.TicketPrice = (from p in obj.tblEventTicketTypePrices
                                           join c in obj.tblCurrencyModes
                                           on p.CurrencyModeId equals c.CurrencyModeId
                                           where p.EventId == eve.EventId & p.Status == true
                                           select new TicketsPrices
                                           {
                                               TicketPrice = p.Price,
                                               Currency = c.CurrencyName,
                                               CurrencyId = c.CurrencyModeId,
                                               TicketType = p.TypeOfTicket,
                                               TicketTypeId = p.PriceId
                                           }).ToList();
                        if (eve.TicketPrice.Count > 0)
                            eve.PriceToDisplay = eve.TicketPrice.FirstOrDefault().Currency + " " + eve.TicketPrice.FirstOrDefault().TicketPrice;
                        else
                        {
                            return null;
                        }
                    }
                    if (obj.tblGroups.Any(x => x.EventId == eve.EventId))
                    {
                        eve.Groups = (from g in obj.tblGroups
                                      where g.EventId == eve.EventId
                                      select new EventGroups
                                      {
                                          GroupId = g.GroupId,
                                          GroupName = g.GroupName
                                      }).ToList();
                    }
                    eve.AvailableTickets = obj.tblTicketsDetails.Where(x => x.EventId == eve.EventId && x.Status == "Not Booked").Count();
                    return eve;
                }
            }
            catch { return null; }
        }

        public bool IsIRIdDuplicated(int eventid, string irid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblBookingUserDetails.Any(x => x.EventId == eventid && x.IrId == irid && (x.Status == "Confirmed" || x.Status == "Pending"));
            }
        }

        //public List<EventViewModel> GetAllEvents()
        //{
        //    using (VNETTicketEntities obj = new VNETTicketEntities())
        //    {
        //        List<EventViewModel> objresult = (from e in obj.tblEvents
        //                                          where (e.IsActive == true && e.EventEndDate > DateTime.Now)
        //                                          select new EventViewModel
        //                                          {
        //                                              EventId = e.EventId,
        //                                              EventName = e.EventTitle,
        //                                              EventDescription = e.EventDescription,
        //                                              ImagePath = e.ImagePath,
        //                                              ImageName = e.ImageName,
        //                                              StartDate = e.EventStartDate.Value,
        //                                              EndDate = e.EventEndDate.Value
        //                                          }).ToList();
        //        foreach (var eve in objresult)
        //        {
        //            if (obj.tblEventTicketTypePrices.Any(x => x.EventId == eve.EventId))
        //            {
        //                var e = (from p in obj.tblEventTicketTypePrices
        //                         join c in obj.tblCurrencyModes
        //                         on p.CurrencyModeId equals c.CurrencyModeId
        //                         where p.EventId == eve.EventId
        //                         select new
        //                         {
        //                             p.Price,
        //                             c.CurrencyName
        //                         }).FirstOrDefault();
        //                if (e != null)
        //                    eve.EventPrice = e.CurrencyName + " " + e.Price;
        //            }
        //        }
        //        return objresult;
        //    }
        //}

        //public EventViewModel GetEventDetail(int eid)
        //{
        //    try
        //    {
        //        using (VNETTicketEntities obj = new VNETTicketEntities())
        //        {
        //            if (obj.tblEvents.Any(x => x.EventId == eid && x.IsActive == true))
        //            {
        //                EventViewModel objevent = (from e in obj.tblEvents
        //                                           join c in obj.tblCurrencyModes
        //                                           on e.AdminCurrencyMode equals c.CurrencyModeId
        //                                           where (e.IsActive == true && e.EventId == eid)
        //                                           select new EventViewModel
        //                                           {
        //                                               EventId = e.EventId,
        //                                               EventName = e.EventTitle,
        //                                               EventDescription = e.EventDescription,
        //                                               EventVenueDetail = e.VenueDetails,
        //                                               EventAddress1 = e.Address1,
        //                                               EventAddress2 = e.Address2,
        //                                               EventCity = e.City,
        //                                               EventPostalCode = e.PostalCode,
        //                                               ImagePath = e.ImagePath,
        //                                               ImageName = e.ImageName,
        //                                               StartDate = e.EventStartDate.Value,
        //                                               EndDate = e.EventEndDate.Value
        //                                           }).FirstOrDefault();

        //                objevent.EventTicketType = (from e in obj.tblEventTicketTypePrices
        //                                            join c in obj.tblCurrencyModes
        //                                            on e.CurrencyModeId equals c.CurrencyModeId
        //                                            where (e.EventId == eid)
        //                                            select new TicketTypeViewModel
        //                                            {
        //                                                TicketType = e.TypeOfTicket,
        //                                                TicketTypeCurrency = c.CurrencyName,
        //                                                TicketTypePrice = e.Price,
        //                                                EventPrice = c.CurrencyName + " " + e.Price,
        //                                            }).ToList();
        //                objevent.EventPrice = objevent.EventTicketType.FirstOrDefault().TicketTypePrice;
        //                objevent.EventCurrencyType = objevent.EventTicketType.FirstOrDefault().TicketTypeCurrency;
        //                objevent.EventGroup = obj.tblGroups.Where(x => x.IsActive == true && x.EventId == eid).Select(b => new EventGroupViewModel { GroupId = b.GroupId.ToString(), GroupName = b.GroupName }).ToList();
        //                return objevent;
        //            }
        //            else
        //            {
        //                return null;//new EventViewModel();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //throw;
        //        return null;// new EventViewModel();
        //    }
        //}

        public TicketTypeViewModel GetEventPrice(int eventid, int tickettype)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                //var price = obj.tblEventTicketTypePrices.Where(x => x.EventId == eventid && x.TypeOfTicket == tickettype).FirstOrDefault().Price;

                var price = (from ett in obj.tblEventTicketTypePrices
                             join cm in obj.tblCurrencyModes
                             on ett.CurrencyModeId equals cm.CurrencyModeId
                             where ett.EventId == eventid && ett.PriceId == tickettype && ett.Status == true
                             select new TicketTypeViewModel
                             {
                                 CurrencyModeId = cm.CurrencyModeId,
                                 TicketTypeCurrency = cm.CurrencyName,
                                 EventPrice = ett.Price
                             }).FirstOrDefault();
                //obj.tblEventTicketTypePrices.Where(x => x.EventId == eventid && x.TypeOfTicket == tickettype).FirstOrDefault().Price;
                //if (price == null)
                //{
                //    return "";
                //}
                return price;
            }
        }

        public void AddBookingUserDetail()
        {

        }

        public List<TicketsReport> GetUserTicketsReports()
        {
            DateTime date = DateTime.Now.AddHours(-DateTime.Now.Hour).AddMinutes(-DateTime.Now.Minute);
            //DateTime endDate = date.AddDays(1);
            VNETTicketEntities db = new VNETTicketEntities();
            {
                var reports = (from u in db.tblBookingUserDetails
                               join b in db.tblBookingDetails
                               on u.BookingId equals b.BookingID
                               join e in db.tblEvents
                               on b.EventId equals e.EventId
                               //where b.BookingDateTime > date && b.BookingDateTime <= endDate
                               select new TicketsReport
                               {
                                   EventName = e.EventTitle,
                                   BookingId = u.BookingId.ToString(),
                                   BookingEventId = b.EventBookingID,
                                   TicketId = u.TicketId,
                                   Status = b.Status,
                                   EventDate = e.EventStartDate.Value,
                                   TicketNo = b.NoOfTickets.Value.ToString(),
                                   Price = u.TicketPrice.Value.ToString(),
                                   BookedUserId = u.BookingUserId
                               }).OrderByDescending(x=>x.BookingId).ToList();
                foreach (var item in reports)
                {
                    item.strBookingDate = item.EventDate.ToString("MM.dd.yyyy");
                }
                return reports;
            }
        }

    }
}
