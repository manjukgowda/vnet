﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Models.User
{
    public class SettingsClass
    {
        public class Profiles
        {
            public int UserId { get; set; }
            public string FullName { get; set; }
            public string  EmailId { get; set; }
            public string Country { get; set; }
        }

        public Profiles GetProfile(long UserID)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                Profiles prof = (from p in obj.tblUsers
                             where p.UserId == UserID
                             select new Profiles
                             {
                                 UserId = p.UserId,
                                 FullName = p.FullName,
                                 EmailId = p.UserEmailId,
                                 Country = p.Country
                             }).FirstOrDefault();
                return prof;
            }
        }

        public Profiles GetProfile(string EmailId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UserEmailId == EmailId && x.UserTypeId == 4))
                {
                    Profiles prof = (from p in obj.tblUsers
                                     where p.UserEmailId == EmailId && p.UserTypeId == 4
                                     select new Profiles
                                     {
                                         UserId = p.UserId,
                                         FullName = p.FullName,
                                         EmailId = p.UserEmailId,
                                         Country = p.Country
                                     }).FirstOrDefault();
                    return prof;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool UpdateProfile(string EmailId, string fullname, string country)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                if (obj.tblUsers.Any(x => x.UserEmailId == EmailId && x.UserTypeId == 4))
                {
                    var prof = obj.tblUsers.FirstOrDefault(x => x.UserEmailId == EmailId && x.UserTypeId == 4);
                    prof.FullName = fullname;
                    prof.Country = country;
                    obj.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool ChangePassword(long UserId, string OldPassword, string NewPassword)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                string encryptedOldPwd = Cryptography.passwordEncrypt(OldPassword);
                if (obj.tblUsers.Any(x => x.UserId == UserId && x.Password == encryptedOldPwd))
                {
                    tblUser u = obj.tblUsers.FirstOrDefault(x => x.UserId == UserId && x.Password == encryptedOldPwd);
                    u.Password = Cryptography.passwordEncrypt(NewPassword);
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool ChangePassword(string EmailId, string OldPassword, string NewPassword)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                string encryptedOldPwd = Cryptography.passwordEncrypt(OldPassword);
                if (obj.tblUsers.Any(x => x.UserEmailId == EmailId && x.UserTypeId == 4 && x.Password == encryptedOldPwd))
                {
                    tblUser u = obj.tblUsers.FirstOrDefault(x => x.UserEmailId == EmailId && x.Password == encryptedOldPwd);
                    u.Password = Cryptography.passwordEncrypt(NewPassword);
                    obj.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}
