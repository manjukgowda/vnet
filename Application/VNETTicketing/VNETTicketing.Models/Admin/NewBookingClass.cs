﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNETTicketing.Models.User;

namespace VNETTicketing.Models.Admin
{
    public class NewBookingClass
    {
        public class TicketPrices
        {
            public int PriceId { get; set; }
            public string TypeOfTicket { get; set; }
            public string Price { get; set; }
            public int CurrencyTypeId { get; set; }
            public string CurrencyType { get; set; }
            public string PriceWithCurrency { get; set; }
        }

        public class TicketsReport
        {
            public string Event { get; set; }
            public string UserName { get; set; }
            public string ModeOfPayment { get; set; }
            public string TransferedTicket { get; set; }
            public string TicketType { get; set; }
            public string IRID { get; set; }
            public DateTime BookingDate { get; set; }
            public string strBookingDate { get; set; }
        }

        public class NewBookingDetails
        {
            public long BookingId { get; set; }
            public string EventBookingId { get; set; }
            public int EventId { get; set; }
            public string EventName { get; set; }
            public string Venue { get; set; }
            public DateTime EventDateTime { get; set; }
            public decimal? TotalAmount { get; set; }
            public decimal? Discount { get; set; }
            public decimal? NetAmount { get; set; }
            public string PaymentMode { get; set; }
            public string Status { get; set; }
            public string TicketType { get; set; }
            public decimal? Price { get; set; }
            public string TypePrice { get; set; }
            public int CurrencyMode { get; set; }
            public int GroupId { get; set; }
            public int NoTicket { get; set; }
            public int UserId { get; set; }
            public string IRID { get; set; }
            public string UName { get; set; }
            public string Reference { get; set; }
            public decimal? AR { get; set; }
            public decimal? CashPaid { get; set; }
            public string EmailId { get; set; }
            public string Country { get; set; }
            // public List<int> UserIds { get; set; }
            public List<NewBookingDetails> BookingUserDetail { get; set; }
            public string TicketId { get; set; }
            public string ValidationStatus { get; set; }
            public string PromoCodeUsed { get; set; }
            public string ImagePath { get; set; }
            public string ImageName { get; set; }
        }

        public class GroupDetails
        {
            public int GroupId { get; set; }
            public string GroupName { get; set; }
        }

        public List<GroupDetails> GetGroups(int EventId)
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                var groups = (from g in db.tblGroups
                              where g.EventId == EventId
                              select new GroupDetails
                              {
                                  GroupId = g.GroupId,
                                  GroupName = g.GroupName
                              }).ToList();
                return groups;
            }
        }
        public List<TicketPrices> GetTicketTypes(int EventId, int PriceId = 0)
        {
            using (VNETTicketEntities db = new VNETTicketEntities())
            {
                List<TicketPrices> prices = (from t in db.tblEventTicketTypePrices
                                             join c in db.tblCurrencyModes
                                             on t.CurrencyModeId equals c.CurrencyModeId
                                             where t.EventId == EventId && t.Status == true
                                             select new TicketPrices
                                             {
                                                 PriceId = t.PriceId,
                                                 TypeOfTicket = t.TypeOfTicket,
                                                 Price = t.Price,
                                                 CurrencyType = c.CurrencyName,
                                                 PriceWithCurrency = c.CurrencyName + " " + t.Price,
                                                 CurrencyTypeId = t.CurrencyModeId.Value
                                             }).ToList();

                return PriceId == 0 ? prices : prices.Where(x => x.PriceId == PriceId).ToList();
            }
        }

        public long AddUserTransaction(List<NewBookingDetails> bookingList, int bookingBy, string bookedBy)
        {
            //try
            //{
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                int eventId = bookingList[0].EventId;
                EventClass objEvent = new EventClass();
                string prepix = objEvent.GetEventsDetailsByEventId(eventId).Prefix;
                int available = objEvent.GetAvailableTicketsCount(eventId);
                if (bookingList.Count <= available)
                {
                    int totalPrice = Convert.ToInt32(bookingList[0].Price) * bookingList.Count;
                    tblBookingDetail book = new tblBookingDetail();
                    book.BookedBy = bookedBy;
                    book.BookingDateTime = DateTime.Now;
                    book.BUserId = bookingBy;
                    book.EventId = bookingList[0].EventId;
                    book.GroupId = bookingList[0].GroupId;
                    book.TotalAmount = totalPrice;
                    book.NetAmountPaid = bookingList[0].NetAmount == null ? 0 : bookingList[0].NetAmount;
                    book.Discount = bookingList[0].Discount == null ? 0 : Convert.ToInt32(bookingList[0].Discount);
                    book.AR = bookingList[0].AR;
                    book.CashPaid = bookingList[0].CashPaid;
                    book.PaymentMode = bookingList[0].PaymentMode;
                    book.NoOfTickets = bookingList[0].NoTicket;
                    book.Status = "Pending";
                    if (bookingList[0].Discount != null)
                        book.PromoCodeUsed = bookingList[0].PromoCodeUsed;
                    book = obj.tblBookingDetails.Add(book);
                    obj.SaveChanges();
                    book.EventBookingID = prepix + book.BookingID;
                    obj.SaveChanges();
                    int eventid = bookingList[0].EventId;
                    var bTickets = obj.tblTicketsDetails.Where(x => x.EventId == eventid && x.Status == "Not Booked");
                    foreach (var b in bookingList)
                    {
                        tblBookingUserDetail booking = new tblBookingUserDetail();
                        booking.BookingId = Convert.ToInt32(book.BookingID);
                        booking.IrId = b.IRID;
                        booking.Name = b.UName;
                        booking.Status = "Pending";
                        var bTicket = bTickets.FirstOrDefault(x => x.Status == "Not Booked");
                        booking.TicketId = bTicket.TicketId;
                        bTicket.Status = "Booked";
                        booking.EventId = b.EventId;
                        booking.TicketPrice = b.Price;
                        booking.TicketType = Convert.ToString(b.TicketType);
                        booking.Reference = b.Reference;
                        booking.EmailId = b.EmailId;
                        booking.Country = b.Country;
                        obj.tblBookingUserDetails.Add(booking);
                        // obj.Entry(bTicket).Property(x => x.Status).IsModified = true;
                        obj.SaveChanges();
                    }
                    return book.BookingID;
                }
                else
                {
                    return 0;
                }
            }

            //}
            //catch (Exception e)
            //{
            //    throw;
            //}
        }

        public tblBookingDetail GetBookingDetails(long BookingId)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblBookingDetails.FirstOrDefault(x => x.BookingID == BookingId);
        }

        public int GetEventIdByUserId(int userId)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblUsers.Any(x => x.UserId == userId))
            {
                return db.tblUsers.FirstOrDefault(x => x.UserId == userId && x.UserTypeId == 3).EventId.Value;
            }
            return 0;
        }

        public int GetGroupId(string GroupName)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (!db.tblGroups.Any(x => x.GroupName == GroupName))
            {
                db.tblGroups.Add(new tblGroup() { GroupName = GroupName, IsActive = true, LastModifiedDate = DateTime.Now });
                db.SaveChanges();
            }
            var group = db.tblGroups.FirstOrDefault(x => x.GroupName == GroupName);
            return group.GroupId;
        }

        public string UpdateBookingDetails(long BookingId, string paymentmode, string status, string promocode, decimal DiscountUsed, decimal NetAmount, decimal? ar, decimal? cash)
        {
            VNETTicketEntities db = new VNETTicketEntities();

            var book = db.tblBookingDetails.FirstOrDefault(x => x.BookingID == BookingId);
            book.PaymentMode = paymentmode;
            book.PromoCodeUsed = promocode;
            book.Discount = DiscountUsed;
            book.NetAmountPaid = NetAmount;
            book.AR = ar;
            book.CashPaid = cash;
            book.Status = status;
            db.SaveChanges();
            var bookedUsers = db.tblBookingUserDetails.Where(x => x.BookingId == BookingId);
            foreach (var usr in bookedUsers)
            {
                usr.Status = status;
            }
            db.SaveChanges();
            return book.EventBookingID;
        }

        public string UpdateBookingDetails(string BookingId, string paymentmode, string status)
        {
            VNETTicketEntities db = new VNETTicketEntities();

            var book = db.tblBookingDetails.FirstOrDefault(x => x.EventBookingID == BookingId);
            book.PaymentMode = paymentmode;
            book.Status = status;
            db.SaveChanges();
            var bookedUsers = db.tblBookingUserDetails.Where(x => x.BookingId == book.BookingID);
            foreach (var usr in bookedUsers)
            {
                usr.Status = status;
            }
            db.SaveChanges();
            return book.EventBookingID;
        }

        public class PromoCodeValidation
        {
            public int EventId { get; set; }
            public string PromoCode { get; set; }
            public string Value { get; set; }
            public bool IsValid { get; set; }
        }

        public PromoCodeValidation ValidatePromoCode(int EventId, string PromoCode)
        {
            PromoCodeValidation obj = new PromoCodeValidation();
            VNETTicketEntities db = new VNETTicketEntities();
            obj.EventId = EventId;
            obj.PromoCode = PromoCode;
            if (db.tblDiscountCoupons.Any(x => x.EventId == EventId && x.Coupon == PromoCode))
            {
                obj.IsValid = true;
                var dis = db.tblDiscountCoupons.FirstOrDefault(x => x.EventId == EventId && x.Coupon == PromoCode);
                obj.Value = dis.CouponValue;
            }
            else
            {
                obj.IsValid = false;
                obj.Value = "0";
            }
            return obj;
        }

        public bool IsValidPromoCode(int EventId, string PromoCode)
        {
            VNETTicketEntities db = new VNETTicketEntities();

            bool result = db.tblDiscountCoupons.Any(x => x.EventId == EventId && x.Coupon == PromoCode);
            if (result)
            {
                int usedcount = db.tblBookingDetails.Where(x => x.PromoCodeUsed == PromoCode).Count();
                int allocated = int.Parse(db.tblDiscountCoupons.Where(x => x.Coupon == PromoCode && x.EventId == EventId).FirstOrDefault().TotalAllocation);
                if (usedcount <= allocated)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public tblDiscountCoupon GetPromoCode(int EventId, string PromoCode)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            return db.tblDiscountCoupons.FirstOrDefault(x => x.EventId == EventId && x.Coupon == PromoCode);
        }

        public List<NewBookingDetails> GetBookingDetailsToServices(string username)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            var ids = (from u in db.tblUsers
                       join b in db.tblBookingDetails
                       on u.UserId equals b.BUserId
                       where u.UserName == username && b.BookedBy == "User"
                       select new
                       {
                           b.EventBookingID
                       }).ToList();
            List<NewBookingDetails> list = new List<NewBookingDetails>();
            foreach (var id in ids)
            {
                string bid = id.EventBookingID;
                list.Add(GetBookingDetailsToPrint(bid));
            }
            return list;
        }

        public NewBookingDetails GetBookingDetailsToPrint(string BookingId)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            if (db.tblBookingDetails.Any(x => x.EventBookingID == BookingId))
            {
                var list = (from b in db.tblBookingDetails
                            join e in db.tblEvents
                            on b.EventId equals e.EventId
                            join et in db.tblEventTicketTypePrices
                            on e.EventId equals et.EventId
                            //join be in db.tblBookingUserDetails
                            //on b.EventId equals be.EventId
                            where b.EventBookingID == BookingId && b.Status != "Transfered" && et.Status == true//&& be.BookingId == BookingId
                            select new NewBookingDetails
                            {
                                BookingId = b.BookingID,
                                EventId = e.EventId,
                                EventName = e.EventTitle,
                                Venue = e.VenueDetails,
                                EventDateTime = (DateTime)e.EventStartDate,
                                NoTicket = (int)b.NoOfTickets,
                                //TicketType = be.TicketType,
                                TotalAmount = b.TotalAmount,
                                Discount = b.Discount,
                                NetAmount = b.NetAmountPaid,
                                PaymentMode = b.PaymentMode,
                                Status = b.Status,
                                ImagePath = e.ImagePath,
                                ImageName = e.ImageName,
                                TypePrice = et.Price
                            }).ToList().FirstOrDefault();
                if (list == null)
                    return null;
                long bookId = list.BookingId;
                NewBookingDetails zobj = new NewBookingDetails();

                zobj.BookingUserDetail = (from be in db.tblBookingUserDetails
                                          where be.BookingId == bookId
                                          select new NewBookingDetails
                                          {
                                              TicketId = be.TicketId,
                                              IRID = be.IrId.ToString(),
                                              UName = be.Name,
                                              EmailId = be.EmailId,
                                              TicketType = be.TicketType,
                                              Status = be.Status,
                                              ValidationStatus = be.ValidationStatus
                                          }).ToList();
                list.BookingUserDetail = zobj.BookingUserDetail;
                return list;
            }
            else
            {
                return null;
            }
        }
        public NewBookingDetails GetBookingDetailsByTicketId(string BookingId, string TicketId)
        {
            VNETTicketEntities db = new VNETTicketEntities();

            var details = (from be in db.tblBookingUserDetails
                           join b in db.tblBookingDetails
                           on be.BookingId equals b.BookingID
                           join ev in db.tblEvents
                           on b.EventId equals ev.EventId
                           where b.EventBookingID == BookingId && be.TicketId == TicketId

                           select new NewBookingDetails
                           {
                               TicketId = be.TicketId,
                               IRID = be.IrId,
                               UName = be.Name,
                               EmailId = be.EmailId,
                               TicketType = be.TicketType,
                               Status = be.Status,
                               ValidationStatus = be.ValidationStatus,
                               EventId = b.EventId.Value,
                               ImagePath = ev.ImagePath
                           }).FirstOrDefault();
            return details;
        }
        public List<NewBookingDetails> GetUsersDetailsByBookingId(string BookingID)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            {
                List<NewBookingDetails> usrs = (from be in db.tblBookingUserDetails
                                                join b in db.tblBookingDetails
                                                on be.BookingId equals b.BookingID
                                                where b.EventBookingID == BookingID
                                                select new NewBookingDetails
                                                {
                                                    TicketId = be.TicketId,
                                                    IRID = be.IrId.ToString(),
                                                    UName = be.Name,
                                                    TicketType = be.TicketType,
                                                    Status = be.Status
                                                }).ToList();
                return usrs;
            }

        }

        public List<TicketsReport> GetAdminTicketsReports()
        {
            DateTime date = DateTime.Now.AddHours(-DateTime.Now.Hour).AddMinutes(-DateTime.Now.Minute);
            //DateTime endDate = date.AddDays(1);
            VNETTicketEntities db = new VNETTicketEntities();
            {
                var reports = (from u in db.tblBookingUserDetails
                               join b in db.tblBookingDetails
                               on u.BookingId equals b.BookingID
                               join e in db.tblEvents
                               on b.EventId equals e.EventId
                               //where b.BookingDateTime > date && b.BookingDateTime <= endDate
                               select new TicketsReport
                               {
                                   Event = e.EventTitle,
                                   IRID = u.IrId,
                                   ModeOfPayment = b.PaymentMode,
                                   TicketType = u.TicketType,
                                   UserName = u.Name,
                                   BookingDate = b.BookingDateTime.Value
                               }).ToList();
                foreach (var item in reports)
                {
                    item.strBookingDate = item.BookingDate.ToString("MM.dd.yyyy");
                }
                return reports;
            }
        }

        public bool ValidateTicketsStatus(long BookingID, string Status)
        {
            VNETTicketEntities db = new VNETTicketEntities();
            {
                var usrs = db.tblBookingUserDetails.Where(x => x.BookingId == BookingID);
                foreach (var usr in usrs)
                {
                    usr.ValidationStatus = Status;
                }
                db.SaveChanges();
                return true;
            }

        }

        public bool IsIRIdDuplicated(int eventid, string irid)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblBookingUserDetails.Any(x => x.EventId == eventid && x.IrId == irid && (x.Status == "Confirmed" || x.Status == "Pending"));
            }
        }
    }
}
