﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNETTicketing.Models.Admin
{
    public class TransferProcess
    {
        public int GetAvailableTicketsCount(int eventId)
        {
            using (VNETTicketEntities obj = new VNETTicketEntities())
            {
                return obj.tblTicketsDetails.Where(x => x.EventId == eventId && x.Status == "Not Booked").Count();
            }
        }

        public bool TransferBooking(string BookingId, int eventid)
        {
            try
            {
                using (VNETTicketEntities obj = new VNETTicketEntities())
                {
                    tblBookingDetail objbookdetail = obj.tblBookingDetails.Where(x => x.EventBookingID == BookingId).FirstOrDefault();
                    long bookId = objbookdetail.BookingID;
                    int bookedusercount = obj.tblBookingUserDetails.Where(x => x.BookingId == bookId).Count();
                    int ticketavailable = obj.tblTicketsDetails.Where(x => x.EventId == eventid && x.Status == "Not Booked").Count();
                    if (ticketavailable >= bookedusercount)
                    {
                        objbookdetail.Status = "Transfered";
                        obj.SaveChanges();

                        List<tblBookingUserDetail> objbookuserdetail = obj.tblBookingUserDetails.Where(x => x.BookingId == bookId).ToList();
                        foreach (var item in objbookuserdetail)
                        {
                            item.Status = "Transfered";
                            tblTicketsDetail objexistticket = obj.tblTicketsDetails.Where(x => x.TicketId == item.TicketId).FirstOrDefault();
                            objexistticket.Status = "Not Booked";
                        }
                        obj.SaveChanges();

                        tblBookingDetail objnew = new tblBookingDetail();
                        objnew = objbookdetail;
                        objnew.EventId = eventid;
                        objnew.BookingDateTime = DateTime.Now;
                        objnew.Status = "Confirmed";
                        obj.tblBookingDetails.Add(objnew);
                        obj.SaveChanges();

                        long newbookid = objnew.BookingID;
                        List<long> newbookuserid = new List<long>();

                        foreach (var item in objbookuserdetail)
                        {
                            tblBookingUserDetail objnewbookdetail = new tblBookingUserDetail();
                            objnewbookdetail = item;
                            string evtprefix = obj.tblEvents.Where(x => x.EventId == eventid && x.Status.ToLower() == "active").FirstOrDefault().EventPrefix;
                            string newticketid = obj.tblTicketsDetails.Where(x => x.EventId == eventid && x.TicketId.StartsWith(evtprefix) && x.Status == "Not Booked").FirstOrDefault().TicketId;
                            objnewbookdetail.TicketId = newticketid;
                            objnewbookdetail.Status = "Confirmed";
                            objnewbookdetail.BookingId = newbookid;
                            obj.tblBookingUserDetails.Add(objnewbookdetail);
                            obj.SaveChanges();

                            tblTicketsDetail objticketdetails = obj.tblTicketsDetails.Where(x => x.EventId == eventid && x.TicketId == newticketid).FirstOrDefault();
                            objticketdetails.Status = "Booked";
                            obj.SaveChanges();
                            newbookuserid.Add(objnewbookdetail.BookingUserId);
                        }

                        obj.tblTransferDetails.Add(new tblTransferDetail() { FromBookingId = (int)bookId, ToBookingId = (int)newbookid, CreatedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Status = "Success" });
                        obj.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
