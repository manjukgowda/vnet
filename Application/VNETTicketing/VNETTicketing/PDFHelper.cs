﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing
{
    public class PDFHelper : System.Web.UI.Page
    { 
        public string GeneratePDF(string bookId, string ticketId)
        {
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsByTicketId(bookId, ticketId);
            if (objdetails == null)
                return null;
            string server = ConfigurationManager.AppSettings["server"].ToString();
            EventsClass eventObj = new EventsClass();
            string template = eventObj.GetEventConfirmationTemplate(objdetails.EventId);

            //server folder path which is stored your PDF documentsring
            string imagepath = server + objdetails.ImagePath.Substring(2);
            string guid = Guid.NewGuid().ToString().Substring(1, 5);
           string filename = "~\\Images\\Barcodespdf\\" + ticketId + "_" + guid + ".pdf";
            string fullpathpdf = server + "/Images/Barcodespdf/" + ticketId + "_" + guid + ".pdf";

            //Create new PDF document
            string footerImgPath = Server.MapPath("~\\Assets\\Uploads\\vnetfooter.png");
            Document document = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
            iTextSharp.text.Font fontTitle = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            //try
            //{

            PdfWriter.GetInstance(document, new FileStream(Server.MapPath( filename), FileMode.Create));

            iTextSharp.text.Image headerImage = iTextSharp.text.Image.GetInstance(imagepath);
            headerImage.ScaleAbsolute(550f, 75f);
            headerImage.Alignment = 0;
            document.Open();

            document.Add(headerImage);


            Paragraph pParticipant = new Paragraph("Dear V-UAE Participant, Greetings from the VNet");
            pParticipant.Alignment = Element.ALIGN_CENTER;
            pParticipant.Font = fontTitle;
            document.Add(new Paragraph(pParticipant));
            document.Add(new Paragraph("IR Name : " + objdetails.UName, (FontFactory.GetFont("Calibri", 12, iTextSharp.text.Font.BOLD, BaseColor.BLUE))));

            string barcodePath = GenerateBarcode(ticketId);
            document.Add(new Paragraph("Registration Id", (FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLUE))));
            iTextSharp.text.Image bImage = iTextSharp.text.Image.GetInstance(barcodePath);
            bImage.Alignment = 0;
            bImage.ScaleToFit(150f, 60f);
            document.Add(bImage);

            string barcodeIRId = GenerateBarcode(objdetails.IRID);
            document.Add(new Paragraph("IR Id", (FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, BaseColor.BLUE))));
            iTextSharp.text.Image irImage = iTextSharp.text.Image.GetInstance(barcodeIRId);
            irImage.Alignment = 0;
            irImage.ScaleToFit(150f, 60f);
            document.Add(irImage);

            Paragraph paragraph = new Paragraph(template);
            paragraph.Alignment = Element.ALIGN_JUSTIFIED;
            paragraph.Font = fontTitle;
            document.Add(new Paragraph(paragraph));

            iTextSharp.text.Image footerImage = iTextSharp.text.Image.GetInstance(footerImgPath);
            footerImage.Alignment = 1;
            footerImage.Bottom = 1;
            footerImage.ScaleToFit(800f, 75f);
            document.Add(footerImage);
            //}

            //catch (Exception ex)
            //{

            //}

            //finally
            //{

            document.Close();

            //}      
            return fullpathpdf;
        }

        public string GenerateBarcode(string TicketId)
        {
            string vpath = "~\\Images";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            vpath = "~\\Images\\Barcodes";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            string guid = Guid.NewGuid().ToString().Substring(1, 10);
            vpath = "~\\Images\\Barcodes\\" + guid + ".png";
            string server = ConfigurationManager.AppSettings["server"].ToString();
            string vpdf = "~\\Images\\Barcodespdf\\" + guid + ".pdf";

            string fullpath = server + "/Images/Barcodes/" + guid + ".png";

            BarCodeHelper obj = new BarCodeHelper();
            obj.GenerateBarCode(TicketId, Server.MapPath(vpath));

            return fullpath;
        }

    }
}