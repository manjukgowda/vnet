﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadETicket.aspx.cs" Inherits="VNETTicketing.DownloadETicket" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Registration</title>
    <!-- Bootstrap Core CSS -->
    <link href="Assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="Assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="Assets/bower_components/bootstrap-select-1.10.0/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="Assets/dist/css/enduser.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="Assets/css/login.css" rel="stylesheet" type="text/css">
</head>


<body>
    <div class="account-container" style="width: 800px; height: 200px;">
        <form id="form1" runat="server">
            <div id="maindiv" runat="server" style="background-color: white;">
                <div class="login_header center-block" style="width: 800px; height: 200px;">
                    <asp:Image ID="imgEvent" Width="800px" Height="200px" ImageUrl="Assets/images/V-Logo.png" runat="server" CssClass="img-responsive center-block" />
                </div>
                <div class="content clearfix">



                    <div class="col-lg-12" style="flex-align: center;">
                        <asp:Label ID="lblPaymentStatus" runat="server" Font-Bold="True" Font-Size="Larger"></asp:Label>
                    </div>
                    <div class="col-lg-12" style="flex-align: center;">
                        <asp:Panel ID="pnlDownload" Visible="false" runat="server">
                            <asp:Button ID="btnDownload"  CssClass="btn btn-primary"  runat="server" Text="Download Confmration Letter" OnClick="btnDownload_Click" />
                            &nbsp; &nbsp; &nbsp; &nbsp; 
                            <asp:HyperLink ID="hlPrint"  CssClass="btn btn-primary"  Target="_blank" runat="server">Print Conrimation Letter</asp:HyperLink>
                        </asp:Panel>
                        <asp:Panel ID="pnlPay" Visible="false" runat="server">
                            <asp:Button ID="btnPAy"  CssClass="btn btn-primary"  runat="server" Text="Make Payment" OnClick="btnPAy_Click" />
                        </asp:Panel> 
                    </div>
                    <div class="container-fluid padding_null page_content" id="DivIdToPrint">
                        <h2 class="page-header">Booking Details</h2>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Booking ID</td>
                                    <td>
                                        <asp:Label Text="" ID="lblBookingId" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Event</td>
                                    <td>
                                        <asp:Label Text="" ID="lblEvent" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Venue</td>
                                    <td>
                                        <asp:Label Text="" ID="lblVenue" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Event Datetime</td>
                                    <td>
                                        <asp:Label Text="" ID="lblEventDateTime" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Number of ticket</td>
                                    <td>
                                        <asp:Label Text="" ID="lblNoOfTicket" runat="server" /></td>
                                </tr>
                                <%--<tr>
                    <td>Ticket Type</td>
                    <td>
                        <asp:Label Text="" ID="lblTicketType" runat="server" /></td>
                </tr>--%>
                                <tr>
                                    <td>Total Amount</td>
                                    <td>
                                        <asp:Label Text="" ID="lblTotalAmount" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <td>
                                        <asp:Label Text="" ID="lblDiscount" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>Net Amount</td>
                                    <td>
                                        <asp:Label Text="" ID="lblNetAmount" runat="server" /></td>
                                </tr>

                                <tr>
                                    <td>Mode of payment</td>
                                    <td>
                                        <asp:Label Text="" ID="lblModeOfPayment" runat="server" /></td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <asp:Label Text="" ID="lblStatus" runat="server" /></td>
                                </tr>
                            </tbody>
                        </table>

                        <h2 class="page-header">User Details</h2>
                        <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
                            <Columns>
                                <asp:BoundField DataField="TicketId" HeaderText="Ticket Id" />
                                <asp:BoundField DataField="IrId" HeaderText="Ir Id" />
                                <asp:BoundField DataField="UName" HeaderText="Name" />
                                <asp:BoundField DataField="TicketType" HeaderText="Ticket Type" />
                                <asp:BoundField DataField="Status" HeaderText="Status" />
                            </Columns>
                        </asp:GridView>
                        <br />
                        <br />
                        <br />
                        <asp:Image ID="imgBarCode" runat="server" />
                    </div>
                </div>

                <br />
            </div>
        </form>
    </div>
    <!-- /content -->

    <!-- /account-container -->

    <!-- jQuery -->
    <script src="Assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="Assets/bower_components/bootstrap-select-1.10.0/dist/js/bootstrap-select.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="Assets/dist/js/enduser.js"></script>
</body>

</html>
