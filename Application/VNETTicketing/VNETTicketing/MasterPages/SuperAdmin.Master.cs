﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.MasterPages
{
    public partial class SuperAdmin : BaseMaster
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var usr = SessionUsers.GetCurrentUser(1);
            lbl_loggeduser.Text = usr.UserName;
        }
    }

    public class BaseMaster : System.Web.UI.MasterPage
    {
        public BaseMaster()
        {
            var usr = SessionUsers.GetCurrentUser(1);
        }
    }
}