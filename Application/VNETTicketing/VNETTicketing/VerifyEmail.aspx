﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerifyEmail.aspx.cs" Inherits="VNETTicketing.VerifyEmail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The V Verification</title>

    <!-- Bootstrap Core CSS -->
    <link href="../Assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../Assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../Assets/dist/css/enduser.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../Assets/css/login.css" rel="stylesheet" type="text/css">
    <link href="../Assets/css/common.css" rel="stylesheet" type="text/css">

</head>

<body>
    <div class="account-container">
        <div class="login_header center-block"><img src="../Assets/images/V-Logo.png" class="img-responsive center-block">
</div>
        <div class="content clearfix">
            <form id="form1" runat="server">
                <h2>Verification</h2>
                <div class="signup-fields">
                    <h4>Verification code has been sent your mail</h4>
                    <div class="form-group">
                        <label>Please enter verification code</label>
                        <asp:TextBox ID="txtCode" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <!-- /login-fields -->
                <div class="login-actions center-block text-center">
                    <asp:Button ID="btnResend" runat="server"  CssClass="button btn btn-success btn-large" Text="Resend" OnClick="btnResend_Click" />
                    <asp:Button ID="btnContinue" runat="server"  CssClass="button btn btn-success btn-large" Text="Continue" OnClick="btnContinue_Click" />
                </div>
                <!-- .actions -->
            </form>
        </div>
        <!-- /content -->
    </div>
    <!-- /account-container -->
       <!-- jQuery -->
    <script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../Assets/dist/js/sb-admin-2.js"></script>
</body>

</html>

