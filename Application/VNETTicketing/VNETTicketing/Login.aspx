﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="VNETTicketing.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>The V Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="../Assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="../bower_components/bootstrap/dist/css/bootstrap-responsive.min.css" rel="stylesheet"> -->
    <link href="../Assets/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="../Assets/css/login.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <div class="account-container">
            <div class="login_header center-block">
                <img src="Assets/images/V-Logo.png" class="img-responsive center-block">
            </div>
            <div class="content clearfix">

                <form action="#" method="post">
                    <asp:Panel ID="p" runat="server" DefaultButton="btnSignIn">
                    <h2>Login</h2>
                    <div class="login-fields">
                        <p>Please provide your details</p>
                        <div class="field FormFields">
                            <label for="username">Username</label>
                            <asp:TextBox runat="server" ID="username" name="username" value="" placeholder="Username" class="login username-field form-control"></asp:TextBox>
                            <label id="dis"></label>
                        </div>
                        <!-- /field -->
                        <div class="field FormFields">
                            <label for="password">Password:</label>
                            <asp:TextBox runat="server" type="password" ID="password" name="password" value="" placeholder="Password" class="login password-field form-control"></asp:TextBox>
                            <label id="dis1"></label>
                        </div>
                        <!-- /password -->
                    </div>
                    <!-- /login-fields -->
                    <div class="login-actions">

                        <asp:LinkButton ID="btnSignIn" runat="server" class="button btn btn-success btn-large center-block" OnClick="btnSignIn_Click">Sign In</asp:LinkButton>
                        <div class="login-extra">
                            <a href="#" class="pull-left">Forgot Password</a>
                            <a href="Registration.aspx" class="pull-right">New here? Signup!</a>
                        </div>
                    </div>
                        </asp:Panel>
                    <!-- .actions -->
                </form>
            </div>
            <!-- /content -->
        </div>
        <!-- /account-container -->
        <div id="modalError" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Username/Password you entered is incorrect!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="Assets/js/jquery-2.1.2.min.js"></script>
        <script src="Assets/bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="Assets/Validations/login.js"></script>
        <script type="text/javascript">
            function goto(e) {
                window.location.href = 'index.html';
                console.log(e);
            }
            function openModalerror() {
                $('#modalError').modal('show');
            }
        </script>
    </form>
</body>
</html>
