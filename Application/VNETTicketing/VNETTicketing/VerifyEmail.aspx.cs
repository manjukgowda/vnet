﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing
{
    public partial class VerifyEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (Session["useremail"] != null)
            {
                string email = Convert.ToString(Session["useremail"]);
                Registrations obj = new Registrations();
                if (obj.VerifyRegisteredUsersEmailID(email, txtCode.Text))
                {
                    Session["message"] = "1";
                    Response.Redirect("Message.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('The code you entered is incorrect!')", true);
                }
            }
        }

        protected void btnResend_Click(object sender, EventArgs e)
        {

        }
    }
}