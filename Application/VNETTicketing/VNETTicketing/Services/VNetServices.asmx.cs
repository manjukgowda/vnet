﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Services
{
    /// <summary>
    /// Summary description for VNetServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VNetServices : System.Web.Services.WebService
    {
        [WebMethod]
        public string RegisterUser(string VNetCode, string Name, string Country, string EmailID, string password)
        {
            Registrations obj = new Registrations();
            Result result = new Result();
            if (VNetCode != "15963")
            {
                result.Success = false;
                result.Message = "Unauthorised access";
            }
            else
            {
                if (obj.RegisterUser(Name, Country, EmailID, password))
                {
                    result.Success = true;
                    result.Message = "Registration is successful!";
                }
                else
                {
                    result.Success = false;
                    result.Message = "Email Id is already exist!";
                }
            }
            return JsonConvert.SerializeObject(result);
        }

        [WebMethod]
        public string IsValidUser(string VNetCode, string UName, string Password)
        {
            Result result = new Result();
            if (VNetCode != "15963")
            {
                result.Success = false;
                result.Message = "Unauthorised access";
            }
            else
            {
                LoginClass obj = new LoginClass();

                if (obj.IsValidUser(UName, Password))
                {
                    result.Success = true;
                    result.Message = "Valid user!";
                }
                else
                {
                    result.Success = false;
                    result.Message = "Invalid user!";
                }
            }
            return JsonConvert.SerializeObject(result);
        }

        [WebMethod]
        public string VerifyEmailId(string VNetCode, string EmailId, string Code)
        {
            Result result = new Result();
            if (VNetCode != "15963")
            {
                result.Success = false;
                result.Message = "Unauthorised access";
            }
            else
            {
                Registrations obj = new Registrations();
                if (obj.VerifyRegisteredUsersEmailID(EmailId, Code))
                {
                    result.Success = true;
                    result.Message = "Validation success!";
                }
                else
                {
                    result.Success = false;
                    result.Message = "Validation unsuccess!";
                }
            }
            return JsonConvert.SerializeObject(result);
        }

        public class Result
        {
            public bool Success { get; set; }
            public string Message { get; set; }
        }
    }
}
