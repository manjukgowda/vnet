﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;
using System.Configuration;
using VNETTicketing.Models.Admin;
using System.Data;

namespace VNETTicketing.Services
{
    public class VNetServiceController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public string VerifyEmail(Verify objverifyemail)
        {
            try
            {
                Result result = new Result();
                if (objverifyemail.vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    Registrations obj = new Registrations();
                    if (obj.VerifyRegisteredUsersEmailID(objverifyemail.emailid, objverifyemail.code))
                    {
                        result.Success = true;
                        result.Message = "Validation success!";
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Validation unsuccess!";
                    }
                }
                return JsonConvert.SerializeObject(result).Replace("\\", "");
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string VerifyUser(Verify objverifyuser)
        {
            try
            {
                Result result = new Result();
                if (objverifyuser.vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    LoginClass obj = new LoginClass();

                    if (obj.IsValidUser(objverifyuser.uname, objverifyuser.password))
                    {
                        result.Success = true;
                        result.Message = "Valid user!";
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Invalid user!";
                    }
                }
                return JsonConvert.SerializeObject(result);
            }
            catch { return "error"; }
        }

        // POST api/<controller>
        //public void Post([FromBody]string value)
        [HttpPost]
        public string RegisterUser(Register value)
        {
            try
            {
                Register val = (Register)value;
                Registrations obj = new Registrations();
                Result result = new Result();
                if (val.vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    if (obj.RegisterUser(val.name, val.country, val.emailid, val.password))
                    {
                        result.Success = true;
                        result.Message = "Registration is successful!";
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Email Id is already exist!";
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string UserLogin(Login objuserlogin)
        {
            try
            {
                Result result = new Result();
                if (objuserlogin.vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    LoginClass obj = new LoginClass();
                    int usertype = int.Parse(objuserlogin.usertypeid);
                    int userid = 0;
                    if (obj.IsValidUser(objuserlogin.emailid, objuserlogin.password, ref usertype, ref userid))
                    {
                        result.Success = true;
                        result.Message = "Valid user";
                        result.UserId = userid.ToString();
                    }
                    else
                    {
                        result.Success = true;
                        result.Message = "Invalid user";
                    }
                }
                return JsonConvert.SerializeObject(result);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string GetAllEvents(Base b)
        {
            try
            {
                Base val = (Base)b;
                EventClass obj = new EventClass();
                EventList events = new EventList();
                if (val.vnetcode != "15963")
                {
                    events.Success = false;
                    events.Message = "Unauthorised access";
                }
                else
                {
                    events.Events = obj.GetAllEventsToServices();
                    string server = ConfigurationManager.AppSettings["server"];
                    foreach (var item in events.Events)
                    {
                        item.ImagePath = item.ImagePath.Replace("~", server);
                    }
                    events.NoOfEvents = events.Events.Count;
                    events.Success = true;
                    events.Message = "Success";
                }
                return JsonConvert.SerializeObject(events);
            }
            catch { return "error"; }
        }

        public class PromoCodes : Base
        {
            public int EventId { get; set; }
            public string PromoCode { get; set; }
            public string Value { get; set; }
            public bool IsValid { get; set; }
        }

        [HttpPost]
        public string ValidatePromoCode(PromoCodes value)
        {
            try
            {
                PromoCodes val = (PromoCodes)value;
                NewBookingClass obj = new NewBookingClass();
                PromoCodes result = new PromoCodes();
                if (val.vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    var dis = obj.ValidatePromoCode(val.EventId, val.PromoCode);
                    result.EventId = dis.EventId;
                    result.IsValid = dis.IsValid;
                    result.Value = dis.Value;
                    result.Success = true;
                    result.Message = "Success";
                }
                return JsonConvert.SerializeObject(result);
            }
            catch { return "error"; }
        }


        //[HttpPost]
        //public string GetEventDetail(Event value)
        //{
        //    try
        //    {
        //        Event val = (Event)value;
        //        EventClass obj = new EventClass();
        //        EventClass.EventViewModel result;
        //        if (val.vnetcode != "15963")
        //        {
        //            result = new EventClass.EventViewModel();
        //            result.Success = false;
        //            result.Message = "Unauthorised access";
        //        }
        //        else
        //        {
        //            result = obj.GetEventDetail(int.Parse(val.id));
        //            string server = ConfigurationManager.AppSettings["server"];

        //            result.ImagePath = result.ImagePath.Replace("~", server);

        //            result.Success = true;
        //            result.Message = "Success";
        //        }
        //        return JsonConvert.SerializeObject(result);
        //    }
        //    catch { return "error"; }
        //}

        [HttpPost]
        public string GetProfileDetails(ProfileDetails value)
        {
            try
            {
                ProfileDetails val = (ProfileDetails)value;
                EventClass obj = new EventClass();
                if (val.vnetcode != "15963")
                {
                    val.Success = false;
                    val.Message = "Unauthorised access";
                }
                else
                {
                    SettingsClass settings = new SettingsClass();
                    var p = settings.GetProfile(value.EmailId);
                    if (p == null)
                    {
                        val.Success = false;
                        val.Message = "Invalid email id";
                    }
                    else
                    {
                        val.Country = p.Country;
                        val.EmailId = p.EmailId;
                        val.FullName = p.FullName;
                        val.Success = true;
                        val.Message = "Success";
                    }
                }
                return JsonConvert.SerializeObject(val);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string UpdateProfile(ProfileDetails value)
        {
            try
            {
                ProfileDetails val = (ProfileDetails)value;
                EventClass obj = new EventClass();
                if (val.vnetcode != "15963")
                {
                    val.Success = false;
                    val.Message = "Unauthorised access";
                }
                else
                {
                    SettingsClass settings = new SettingsClass();
                    var result = settings.UpdateProfile(value.EmailId, val.FullName, val.Country);
                    if (result)
                    {
                        val.Success = true;
                        val.Message = "Success";
                    }
                    else
                    {
                        val.Success = false;
                        val.Message = "Invalid email id";
                    }
                }
                return JsonConvert.SerializeObject(val);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string UpdatePassword(PasswordInfo value)
        {
            try
            {
                PasswordInfo val = (PasswordInfo)value;
                if (val.vnetcode != "15963")
                {
                    val.Success = false;
                    val.Message = "Unauthorised access";
                }
                else
                {
                    SettingsClass settings = new SettingsClass();
                    var result = settings.ChangePassword(value.EmailId, val.OldPassword, val.NewPassword);
                    if (result)
                    {
                        val.Success = true;
                        val.Message = "Success";
                    }
                    else
                    {
                        val.Success = false;
                        val.Message = "Invalid email id / old password";
                    }
                }
                val.NewPassword = "";
                val.OldPassword = "";
                return JsonConvert.SerializeObject(val);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string ForgetPassword(PasswordInfo value)
        {
            try
            {
                PasswordInfo val = (PasswordInfo)value;
                Registrations obj = new Registrations();
                if (val.vnetcode != "15963")
                {
                    val.Success = false;
                    val.Message = "Unauthorised access";
                }
                else
                {
                    var result = obj.ForgetPassword(value.EmailId);
                    if (result)
                    {
                        val.Success = true;
                        val.Message = "Password has been successfully sent to registered email id";
                    }
                    else
                    {
                        val.Success = false;
                        val.Message = "Invalid email id";
                    }
                }
                return JsonConvert.SerializeObject(val);
            }
            catch { return "error"; }
        }

        [HttpPost]
        public string GetIridDetail(Irid IRID)
        {
            try
            {
                IridClass objirid = new IridClass();
                

                Irid val = IRID;
                
                
                if (val.vnetcode != "15963")
                {
                    val.Success = false;
                    val.Message = "Unauthorised access";
                }
                else
                {
                    DataSet objdetails = objirid.GetUserDetails(val.Ir_id);
                    if (objdetails.Tables.Count>0 && objdetails.Tables[0].Rows.Count>0)
                    {
                        val.FullName = objdetails.Tables[0].Rows[0]["IRName"].ToString();
                        val.Emailid = objdetails.Tables[0].Rows[0]["Email"].ToString();
                        val.ContactNo = objdetails.Tables[0].Rows[0]["ContactNoMobile"].ToString();
                        val.Success = true;
                        val.Message = "Irid details found successfully";
                    }
                    else
                    {
                        val.Success = false;
                        val.Message = "Invalid Irid";
                    }
                }
                return JsonConvert.SerializeObject(val);
            }
            catch { return "error"; }
        }

        public class Result
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public string UserId { get; set; }
        }

        public class BookingResult
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public string BookingId { get; set; }
        }

        public class Register : Base
        {
            //public string vnetcode { get; set; }
            public string name { get; set; }
            public string country { get; set; }
            public string emailid { get; set; }
            public string password { get; set; }
        }

        public class Login : Base
        {
            public string emailid { get; set; }
            public string password { get; set; }
            public string usertypeid { get; set; }
        }

        public class Verify : Base
        {
            //public string vnetcode { get; set; }
            public string emailid { get; set; }
            public string code { get; set; }
            public string uname { get; set; }
            public string password { get; set; }
        }

        public class EventList : Base
        {
            public List<EventClass.EventsServices> Events { get; set; }

            public int NoOfEvents { get; set; }
        }

        public class Event : Base
        {
            public string id { get; set; }
        }

        public class UserDetails : Base
        {
            public string userid { get; set; }
        }

        public class Base
        {
            public int id { get; set; }
            public string vnetcode { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }
        public class ProfileDetails : Base
        {
            public string FullName { get; set; }
            public string EmailId { get; set; }
            public string Country { get; set; }
        }

        public class PasswordInfo : Base
        {
            public string EmailId { get; set; }
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
        }
        
        public class PasswordResetInfo : Base
        {
            public string EmailId { get; set; }
        }

        public class Booking : Base
        {
            public long BookingId { get; set; }
            public int EventId { get; set; }
            public string EventName { get; set; }
            public string Venue { get; set; }
            public DateTime EventDateTime { get; set; }
            public int? TotalAmount { get; set; }
            public int? NetAmount { get; set; }
            public string PaymentMode { get; set; }
            public string Status { get; set; }
            public string TicketType { get; set; }
            public int? Price { get; set; }
            public int CurrencyMode { get; set; }
            public int GroupId { get; set; }
            public int NoTicket { get; set; }
            public int UserId { get; set; }
            public string IRID { get; set; }
            public string UName { get; set; }
            public string EmailId { get; set; }
            public string Country { get; set; }
            public string TicketId { get; set; }
            public string ValidationStatus { get; set; }
            public string PromoCode { get; set; }
            public int Discount { get; set; }
        }

        public class GetBook
        {
            public string UserName { get; set; }
        }

        public class Irid : Base
        {
            public string Ir_id { get; set; }
            public string FullName { get; set; }
            public string Emailid { get; set; }
            public string ContactNo { get; set; }
        }

        [HttpPost]
        public string GetBookingList(GetBook book)
        {
            NewBookingClass obj = new NewBookingClass();
            List<NewBookingClass.NewBookingDetails> bookingList = obj.GetBookingDetailsToServices(book.UserName);

            string server = ConfigurationManager.AppSettings["server"];
            foreach (var item in bookingList)
            {
                item.ImagePath = item.ImagePath.Replace("~", server);
            }
            return JsonConvert.SerializeObject(bookingList);
        }

        [HttpPost]
        public string testmethodBooking()
        {
            List<NewBookingClass.NewBookingDetails> bookingList = new List<NewBookingClass.NewBookingDetails>();
            for (int i = 0; i < 2; i++)
            {
                NewBookingClass.NewBookingDetails obj = new NewBookingClass.NewBookingDetails();
                obj.UserId = 1;
                obj.EventName = "test event1";
                obj.TicketType = "test1";
                bookingList.Add(obj);
            }
            return JsonConvert.SerializeObject(bookingList);
        }

        [HttpPost]
        public string AddBooking(List<Booking> book)
        {
            try
            {
                if (book == null || book.Count == 0)
                {
                    return "error";
                }
                BookingResult result = new BookingResult();
                if (book[0].vnetcode != "15963")
                {
                    result.Success = false;
                    result.Message = "Unauthorised access";
                }
                else
                {
                    NewBookingClass obj = new NewBookingClass();

                    List<NewBookingClass.NewBookingDetails> bookingList = new List<NewBookingClass.NewBookingDetails>();

                    foreach (var item in book)
                    {
                        NewBookingClass.NewBookingDetails objtemp = new NewBookingClass.NewBookingDetails();
                        objtemp.IRID = item.IRID;
                        objtemp.UName = item.UName;
                        objtemp.EventId = item.EventId;
                        objtemp.GroupId = item.GroupId;
                        objtemp.NoTicket = item.NoTicket;
                        objtemp.EmailId = item.EmailId;
                        objtemp.Reference = "";
                        objtemp.Country = item.Country;
                        objtemp.CashPaid = 0;
                        objtemp.Price = item.Price;
                        objtemp.TicketType = item.TicketType;
                        objtemp.UserId = item.UserId;
                        objtemp.NetAmount = item.NetAmount;
                        objtemp.CurrencyMode = item.CurrencyMode;
                        bookingList.Add(objtemp);
                    }
                    long bookingId = obj.AddUserTransaction(bookingList, book[0].UserId, "User");
                    result.BookingId = obj.UpdateBookingDetails(bookingId, book[0].PaymentMode, "Confirmed", book[0].PromoCode, book[0].Discount, book[0].NetAmount.Value, 0, 0);
                    result.Success = true;
                    result.Message = "Booking details has been saved successfully";
                }
                return JsonConvert.SerializeObject(result);
            }
            catch
            {
                return "error";
            }
        }
    }
}