﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="Collections.aspx.cs" Inherits="VNETTicketing.Registrar.Collections" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Assets/css/collections.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
            <div class="col-lg-12">
        <h1 class="page-header">Collection</h1>
    </div>
    <div class="row">
        <div class="field">
            <div class="field">
                <div class="col-md-4 col-xs-8">
                    <asp:TextBox ID="txtBarcode" runat="server" placeholder="Barcode" CssClass="login username-field form-control"></asp:TextBox>
                </div>
                <div class="col-md-8 col-xs-4">
                    <asp:Button ID="btnScan" CssClass="btn btn-primary" runat="server" Text="Scan" OnClick="btnScan_Click" />
                </div>
            </div>
        </div>
    </div>
    <div class="row pdetails">
        <p id="pInfo" runat="server" class="lead text-info"></p>
        <asp:Panel ID="Panel1" runat="server" Visible ="false">
            <div class="row">
                <div class="container-fluid padding_null page_content" id="DivIdToPrint">
                    <h2 class="page-header">Booking Details</h2>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Booking ID</td>
                                <td>
                                    <asp:Label Text="" ID="lblBookingId" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Event</td>
                                <td>
                                    <asp:Label Text="" ID="lblEvent" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Venue</td>
                                <td>
                                    <asp:Label Text="" ID="lblVenue" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Event Datetime</td>
                                <td>
                                    <asp:Label Text="" ID="lblEventDateTime" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Number of ticket</td>
                                <td>
                                    <asp:Label Text="" ID="lblNoOfTicket" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td>
                                    <asp:Label Text="" ID="lblTotalAmount" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Discount</td>
                                <td>
                                    <asp:Label Text="" ID="lblDiscount" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Net Amount</td>
                                <td>
                                    <asp:Label Text="" ID="lblNetAmount" runat="server" /></td>
                            </tr>

                            <tr>
                                <td>Mode of payment</td>
                                <td>
                                    <asp:Label Text="" ID="lblModeOfPayment" runat="server" /></td>
                            </tr>

                            <tr>
                                <td>Status</td>
                                <td>
                                    <asp:Label Text="" ID="lblStatus" runat="server" /></td>
                            </tr>
                        </tbody>
                    </table>

                    <h2 class="page-header">User Details</h2>
                    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
                        <Columns>
                            <asp:BoundField DataField="TicketId" HeaderText="Ticket Id" />
                            <asp:BoundField DataField="IrId" HeaderText="Ir Id" />
                            <asp:BoundField DataField="UName" HeaderText="Name" />
                            <asp:BoundField DataField="TicketType" HeaderText="Ticket Type" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="ValidationStatus" HeaderText="Validation Status" />

                        </Columns>
                    </asp:GridView>

                </div>
            </div>
            <div class="row">
                <asp:Button ID="btnValidate"  CssClass="btn btn-primary"  runat="server" Text="Validate" OnClick="btnValidate_Click" />
                <asp:Button ID="btnInvalidate"  CssClass="btn btn-primary"  runat="server" Text="Invalidate" OnClick="btnInvalidate_Click" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
