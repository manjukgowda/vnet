﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="ConfirmationLetter.aspx.cs" Inherits="VNETTicketing.Registrar.ConfirmationLetter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="col-lg-12">
        <h1 class="page-header">Confirmation Letter</h1>
    </div>
    <div class="container-fluid padding_null page_content" id="DivIdToPrint">
        <h2 class="page-header">Booking Details</h2>  
          <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>Booking ID</td>
                    <td>
                        <asp:Label Text="" ID="lblBookingId" runat="server" /></td>
                </tr>
                <tr>
                    <td>Event</td>
                    <td>
                        <asp:Label Text="" ID="lblEvent" runat="server" /></td>
                </tr>
                <tr>
                    <td>Venue</td>
                    <td>
                        <asp:Label Text="" ID="lblVenue" runat="server" /></td>
                </tr>
                <tr>
                    <td>Event Datetime</td>
                    <td>
                        <asp:Label Text="" ID="lblEventDateTime" runat="server" /></td>
                </tr>
                <tr>
                    <td>Number of ticket</td>
                    <td>
                        <asp:Label Text="" ID="lblNoOfTicket" runat="server" /></td>
                </tr>
                <%--<tr>
                    <td>Ticket Type</td>
                    <td>
                        <asp:Label Text="" ID="lblTicketType" runat="server" /></td>
                </tr>--%>
                <tr>
                    <td>Total Amount</td>
                    <td>
                        <asp:Label Text="" ID="lblTotalAmount" runat="server" /></td>
                </tr>
                <tr>
                    <td>Discount</td>
                    <td>
                        <asp:Label Text="" ID="lblDiscount" runat="server" /></td>
                </tr>
                <tr>
                    <td>Net Amount</td>
                    <td>
                        <asp:Label Text="" ID="lblNetAmount" runat="server" /></td>
                </tr>

                <tr>
                    <td>Mode of payment</td>
                    <td>
                        <asp:Label Text="" ID="lblModeOfPayment" runat="server" /></td>
                </tr>

                <tr>
                    <td>Status</td>
                    <td>
                        <asp:Label Text="" ID="lblStatus" runat="server" /></td>
                </tr>
            </tbody>
        </table>
            
<h2 class="page-header">User Details</h2>
        <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
            <Columns>
                <asp:BoundField DataField="TicketId" HeaderText="Ticket Id" />                
                <asp:BoundField DataField="IrId" HeaderText="Ir Id" />
                <asp:BoundField DataField="UName" HeaderText="Name" />                
                <asp:BoundField DataField="TicketType" HeaderText="Ticket Type" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                </Columns>
        </asp:GridView>

        <asp:Image ID="imgBarCode" runat="server" />
    </div>
    <div class="pull-right">
        <input type="button" name="" value="Print" class="btn btn-primary"  onclick="printDiv()" />
    </div>
    <br />
   
    <script>
        function printDiv() {

            var divToPrint = document.getElementById('DivIdToPrint');

            var newWin = window.open('', 'Print-Window');

            newWin.document.open();

            //newWin.document.write('<html><head><title>DIV Contents</title>');
            //newWin.document.write('<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet" type="text/css" />');
            //newWin.document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js" type="text/javascript" />');
            //newWin.document.write('</head><body  onload="window.print()">');
            //newWin.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            //newWin.document.write(divToPrint.innerHTML);
            //newWin.document.write('</body></html>');
            newWin.document.write('<html><head></head><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

            newWin.document.close();

            setTimeout(function () { newWin.close(); }, 10);

        }
    </script>
</asp:Content>
