﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="VNETTicketing.Registrar.Transfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row dashboard">
                <div class="col-lg-12">
                    <h1 class="page-header">Transfer tickets</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="" id="conf">Confirmation letter</label>
                </div>
                <div class="row">
                    <div class="field">
                        <div class="field">
                            <div class="col-md-8 col-xs-8">
                                <input type="text" id="barcode" name="barcode" value="" placeholder="Barcode" disabled class="login username-field form-control" />
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <button class="btn btn-primary" id="scan">Scan</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pdetails">
                    <p class="lead text-success">Successfully verified</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Participant details</div>
                                <div class="panel-body">
                                    <p><b>Full name</b> : John Alex</p>
                                    <p><b>IR ID</b> : XXXXXXXX23456</p>
                                    <p><b>Ticket number</b> : XXXXXXXX57823</p>
                                    <p><b>Group name</b> : Group 1</p>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Participant details</div>
                                <div class="panel-body">
                                    <p><b>Full name</b> : Mister X</p>
                                    <p><b>IR ID</b> : XXXXXXXX123456</p>
                                    <p><b>Ticket number</b> : XXXXXXXX098765</p>
                                    <p><b>Group name</b> : Group 2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row submit_div">
                <center>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button>
                    <button class="btn btn-danger">Cancel</button>
                </center>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Transferee details</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="field">
                                    <div class="col-md-8 col-xs-8">
                                        <input type="text" id="Text1" name="barcode" value="" placeholder="IR ID" class="login username-field form-control" />
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <button class="btn btn-primary" type="button" name="verify">Verify</button>
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Full name</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" id="Text2" name="barcode" value="" placeholder="Full name" disabled class="login username-field form-control" />
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Email Address</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" id="Text3" name="barcode" value="" placeholder="Email Address" disabled class="login username-field form-control" />
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Transfer fee</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <p>$ 60</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="window.location.href='congratulations-transfer.html'">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- jQuery -->
    <script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../Assets/dist/js/sb-admin-2.js"></script>
    <script>
        $(document).ready(function () {
            $('.pdetails').hide();
            $("#scan").click(function () {
                //alert("skdhzhlk,.mn");
                $(".pdetails").show();
            });
        });
        document.getElementById('conf').onchange = function () {
            document.getElementById('barcode').disabled = !this.checked;
        };
    </script>
</asp:Content>
