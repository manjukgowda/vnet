﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Registrar
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DashboardHelper obj = new DashboardHelper();
                var usr = SessionUsers.GetCurrentUser(3);
                int eventId = obj.GetEventIdByRegistrar(usr.UserId);
                DashboardStats c = obj.GetRegistrarDashboardData(eventId);
                lblAmount.Text = Convert.ToString(c.TotalAmount);
                lblInvalidatedTickets.Text = Convert.ToString(c.InvalidatedTickets);
                lblTotalTickets.Text = Convert.ToString(c.TotalTickets);
                lblValidatedTickets.Text = Convert.ToString(c.ValidatedTickets);
            }
        }
    }
}