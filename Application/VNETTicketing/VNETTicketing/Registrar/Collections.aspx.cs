﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;

namespace VNETTicketing.Registrar
{
    public partial class Collections : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Panel1.Visible = false;
            }
        }

        protected void btnScan_Click(object sender, EventArgs e)
        {
            try
            {
                long BookingId = Convert.ToInt64(txtBarcode.Text);
                NewBookingClass obj = new NewBookingClass();

                NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(BookingId.ToString());
                if (objdetails != null)
                {
                    lblBookingId.Text = objdetails.BookingId.ToString();
                    lblEvent.Text = objdetails.EventName;
                    lblVenue.Text = objdetails.Venue;
                    lblEventDateTime.Text = objdetails.EventDateTime.ToString();
                    lblNoOfTicket.Text = objdetails.NoTicket.ToString();
                    lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
                    lblDiscount.Text = Convert.ToString(objdetails.Discount);
                    lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
                    lblModeOfPayment.Text = objdetails.PaymentMode;
                    lblStatus.Text = objdetails.Status;
                    gvUsers.DataSource = objdetails.BookingUserDetail;
                    gvUsers.DataBind();
                    pInfo.InnerHtml = "Successfully verified";
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                    pInfo.InnerHtml = "You have entered an invalid barcode!";
                }
            }
            catch (Exception ex)
            {
                Panel1.Visible = false;
                pInfo.InnerHtml = "Error while verification: " + ex.Message;
            }

        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                string BookingId = txtBarcode.Text;
                NewBookingClass obj = new NewBookingClass();

                List<VNETTicketing.Models.Admin.NewBookingClass.NewBookingDetails> objdetails = obj.GetUsersDetailsByBookingId(BookingId);
                if (objdetails.Count > 0)
                {
                    obj.ValidateTicketsStatus(objdetails[0].BookingId, "Validated");

                    NewBookingClass.NewBookingDetails usrDetails = obj.GetBookingDetailsToPrint(BookingId);

                    gvUsers.DataSource = usrDetails.BookingUserDetail;
                    gvUsers.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Validated successfully!')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('You have entered an invalid barcode!')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Error while verification: " + ex.Message + "')", true);
            }

        }

        protected void btnInvalidate_Click(object sender, EventArgs e)
        {
            try
            {
                string BookingId = txtBarcode.Text;
                NewBookingClass obj = new NewBookingClass();

                List<VNETTicketing.Models.Admin.NewBookingClass.NewBookingDetails> objdetails = obj.GetUsersDetailsByBookingId(BookingId);
                if (objdetails.Count > 0)
                {
                    obj.ValidateTicketsStatus(objdetails[0]. BookingId, "Invalidated");
                    NewBookingClass.NewBookingDetails usrDetails = obj.GetBookingDetailsToPrint(BookingId);

                    gvUsers.DataSource = usrDetails.BookingUserDetail;
                    gvUsers.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Invalidated successfully!')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('You have entered an invalid barcode!')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Error while verification: " + ex.Message + "')", true);
            }
        }
    }
}