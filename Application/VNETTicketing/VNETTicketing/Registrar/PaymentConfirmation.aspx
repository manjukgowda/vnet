﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="PaymentConfirmation.aspx.cs" Inherits="VNETTicketing.Registrar.PaymentConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="col-lg-12">
        <h1 class="page-header">Payment confirmation</h1>
    </div>
    <div class="container-fluid padding_null page_content">
        <div class="row partdetails">
            <%-- <div class="col-md-8 col-xs-12" id="participantpanel">
        </div>--%>
            <div class="col-md-1"></div>
            <div class="col-md-8 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Tickets Details</div>
                    <div class="panel-body">
                        <p>
                            <b>Tickets</b> :
                            <asp:Label Text="0" ID="lblTickets" runat="server" />
                        </p>
                        <p>
                            <b>Total price</b> :
                            <asp:Label Text="0" ID="lblPrice" runat="server" />
                        </p>
                        <hr />
                
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Apply Promocode if any</div>
                    <div class="panel-body"><p>
                        <b>Promo Code</b> :  
                            <asp:TextBox ID="txtPromoCode" CssClass="login irid-field form-control" runat="server"></asp:TextBox>
                        
                        <asp:Button ID="btnValidatePromo" CssClass="btn btn-primary" runat="server" Text="Apply" OnClick="btnValidatePromo_Click" /></p>
                    </div>
                    <asp:Panel ID="pnlDiscount" runat="server" Visible="true">
                        <div class="panel-body">
                        <p>
                            <b>Discount</b> :
                            <asp:Label Text="0" ID="lblDiscount" runat="server" />
                        </p>
                        <p>
                            <b>Net Amount (RM MYR)</b> :
                            <asp:Label Text="0" ID="lblNetAmount" runat="server" />
                        </p></div>
                    </asp:Panel>
                </div>
                 <div class="panel panel-default">
                    <div class="panel-heading">Payment</div>
                    <div class="panel-body">
                                        
                    <div class="radio">
                        <p style="margin-left:20px;">
                            <asp:RadioButton Text="Cash" ID="rbtnCash" runat="server" GroupName="paymentmode" />
                        </p>
                        <p style="margin-left:20px;">
                            <asp:RadioButton Text="Credit/Debit card" runat="server" ID="rbtncard" GroupName="paymentmode" />
                        </p>
                        
                    </div>
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <center>
            <asp:button text="Submit" CssClass="btn btn-primary" runat="server" ID="btnSubmitPayment" OnClick="btnSubmitPayment_Click" OnCommand="btnSubmitPayment_Command" />
                                        <%--<button type="submit" class="btn btn-primary" onclick="window.location.href='congratulations-booking.html'">Submit</button>--%>
                                        <button type="submit" class="btn btn-danger">Cancel</button>
                                    </center>
        </div>
    </div>
     <div id="modalError" class="modal fade" role="dialog">
           <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="lblTitle" class="modal-title" runat="server">Error</h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
      <script src="../Assets/js/jquery-2.1.2.min.js"></script>
        <script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script type="text/javascript">
            function openModalerror() {
                $('#modalError').modal('show');
            }
    </script>

</asp:Content>
