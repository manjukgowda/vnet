﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="NewBooking.aspx.cs" Inherits="VNETTicketing.Registrar.NewBooking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/collections.css">
    <style>
        /*#barcode {
            width: 40%;
        }*/

        .submitarea
        {
            padding-bottom: 2%;
        }

        .modal-header
        {
            background-color: #f7f7f7;
        }
    </style>
    <div class="col-lg-12">
        <h1 class="page-header">New Booking</h1>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Event:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlevents" InitialValue="0"  runat="server" ErrorMessage="Please select events"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Ticket Type:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTicketTypes" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketTypes_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTicketTypes" InitialValue="0" ErrorMessage="Please select ticket type"></asp:RequiredFieldValidator>
                <asp:Label Text="" ID="lbltickettypestatus" runat="server" />
                <p class="small">Ticket price is based on ticket types</p>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Price:</label>
                <asp:TextBox ID="txtPrice" runat="server"  CssClass="form-control" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField id="hdncurrencytypeid" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPrice" ErrorMessage="Price is required!"></asp:RequiredFieldValidator>
            </div>
        </div>
    
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Group:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlGroup" ErrorMessage="Please select group" InitialValue="0"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">No. of Tickets:</label>

                 <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTicketQuantity">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlTicketQuantity" InitialValue="0" ErrorMessage="Please select no of ticket"></asp:RequiredFieldValidator>
            </div>
        </div>
         <div class="modal-footer">
            <asp:Button ID="btnContinue" runat="server" CssClass="btn btn-primary" Text="Continue" OnClick="btnContinue_Click" />

        </div>
    </div>

</asp:Content>
