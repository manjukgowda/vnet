﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Registrar
{
    /// <summary>
    /// Summary description for RegistrarDashboard
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RegistrarDashboard : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(EnableSession=true)]
        public DashboardStats GetAdminDashboardCounters()
        {
            DashboardHelper obj = new DashboardHelper();
            var usr = SessionUsers.GetCurrentUser(3);
            int eventId = obj.GetEventIdByRegistrar(usr.UserId);
            DashboardStats c = obj.GetRegistrarDashboardData(eventId);
            return c;
        }
    }
}
