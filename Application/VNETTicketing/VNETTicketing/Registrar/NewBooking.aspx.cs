﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.Registrar
{
    public partial class NewBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeEventPage();
            }
        }

        private void InitializeEventPage()
        {
            BindEvents();

            for (int i = 1; i <= 10; i++)
            {
                ddlTicketQuantity.Items.Add(new ListItem(i.ToString()));
            }
            ddlTicketQuantity.Items.Insert(0, new ListItem("-Select-", "0"));
        }

        private void BindEvents()
        {
            lbltickettypestatus.Text = "";
            NewBookingClass bookingObj = new NewBookingClass();
            ddlevents.Items.Clear();
            EventsClass obj = new EventsClass();
            var events = obj.GetAllEvents();
            var usr = SessionUsers.GetCurrentUser(3);
            int eventid = bookingObj.GetEventIdByUserId(Convert.ToInt32( usr.UserId));
            events = events.Where(x => x.EventId == eventid).ToList();
            ddlevents.DataSource = events;
            ddlevents.DataTextField = "EventName";
            ddlevents.DataValueField = "EventId";
            ddlevents.DataBind();
            if (events.Count == 0)
            {
                ddlevents.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlGroup.Items.Insert(0, new ListItem("-Select-", "0"));
            }
            else
            {
                LoadTicketPrice(Convert.ToInt32(ddlevents.SelectedValue));
                LoadGroups(Convert.ToInt32(ddlevents.SelectedValue));
            }
        }

        protected void ddlevents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = Convert.ToInt32(ddlevents.SelectedValue);
            LoadTicketPrice(eventid);
            LoadGroups(eventid);
        }

        public void LoadGroups(int eventid)
        {
            NewBookingClass obj = new NewBookingClass();
            ddlGroup.DataSource = obj.GetGroups(eventid);
            ddlGroup.Items.Insert(0, new ListItem("-Select-", "0"));
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "GroupId";
            ddlGroup.DataBind();
        }

        private void LoadTicketPrice(int eventId)
        {
            NewBookingClass obj = new NewBookingClass();
            var tickets = obj.GetTicketTypes(eventId);
            ddlTicketTypes.DataSource = tickets;
            ddlTicketTypes.DataTextField = "TypeOfTicket";
            ddlTicketTypes.DataValueField = "PriceId";
            ddlTicketTypes.DataBind();

            if (tickets.Count == 0)
            {
                lbltickettypestatus.Text = "Error: Ticket type hasn't been set by super administrator";
                ddlTicketTypes.Items.Insert(0, new ListItem("-Select-", "0"));
                txtPrice.Text = "";
            }
            else
            {
                lbltickettypestatus.Text = "";
                GetTicketPrice(eventId, Convert.ToInt32(ddlTicketTypes.SelectedValue));
            }

        }

        private void GetTicketPrice(int eventId, int priceId)
        {
            NewBookingClass obj = new NewBookingClass();

            var prices = obj.GetTicketTypes(eventId, priceId);
            if (prices.Count == 1)
            {
                txtPrice.Text = prices[0].PriceWithCurrency; hdncurrencytypeid.Value = prices[0].CurrencyTypeId.ToString();
            }
            else
            {
                txtPrice.Text = ""; hdncurrencytypeid.Value = "1";
            }
        }

        protected void ddlTicketTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = Convert.ToInt32(ddlevents.SelectedValue);
            int priceId = Convert.ToInt32(ddlTicketTypes.SelectedValue);
            GetTicketPrice(eventid, priceId);
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            VNETTicketEntities obj = new VNETTicketEntities();
            int tickets = Convert.ToInt32(ddlTicketQuantity.SelectedValue);
            NewBookingClass.NewBookingDetails objdetails = new NewBookingClass.NewBookingDetails();
            objdetails.EventId = int.Parse(ddlevents.SelectedValue);
            objdetails.GroupId = int.Parse(ddlGroup.SelectedValue);
            int ticketTypeId = int.Parse(ddlTicketTypes.SelectedValue);
            objdetails.NoTicket = tickets;
            var ticketPrice = obj.tblEventTicketTypePrices.FirstOrDefault(x => x.PriceId == ticketTypeId);
            objdetails.Price = Convert.ToInt32(ticketPrice.Price);
            objdetails.TicketType = ticketPrice.TypeOfTicket;
            objdetails.CurrencyMode = int.Parse(hdncurrencytypeid.Value);
            Session["Regnewbookingdetails"] = objdetails;

            string url = "UsersDetails.aspx?Tickets=" + tickets;
            Response.Redirect(url);
        }
    }
}