﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnBarcode.Barcode;
using VNETTicketing.Models.Admin;

namespace VNETTicketing.Registrar
{
    public partial class ConfirmationLetter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["BookingId"] != null)
            {
                NewBookingClass obj = new NewBookingClass();
                string BookingId = Convert.ToString(Session["BookingId"]);

                NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(BookingId);

                lblBookingId.Text = objdetails.BookingId.ToString();
                lblEvent.Text = objdetails.EventName;
                lblVenue.Text = objdetails.Venue;
                lblEventDateTime.Text = objdetails.EventDateTime.ToString();
                lblNoOfTicket.Text = objdetails.NoTicket.ToString();
                lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
                lblDiscount.Text = Convert.ToString(objdetails.Discount);
                lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
                lblModeOfPayment.Text = objdetails.PaymentMode;
                lblStatus.Text = objdetails.Status;
                gvUsers.DataSource = objdetails.BookingUserDetail;
                gvUsers.DataBind();
                GenerateBarcode(BookingId.ToString());
            }
        }

        public void GenerateBarcode(string bookingId)
        {
            // Create linear barcode object
            Linear barcode = new Linear();
            // Set barcode symbology type to Code-39
            barcode.Type = BarcodeType.CODE39;
            // Set barcode data to encode
            barcode.Data = bookingId;
            // Set barcode bar width (X dimension) in pixel
            barcode.X = 1;
            // Set barcode bar height (Y dimension) in pixel
            barcode.Y = 60;
            // Draw & print generated barcode to png image file
            string vpath = "~\\Images";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            vpath = "~\\Images\\Barcodes";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            vpath = "~\\Images\\Barcodes\\" + Guid.NewGuid().ToString().Substring(1, 10) + ".png";
            barcode.drawBarcode(Server.MapPath(vpath));
            imgBarCode.ImageUrl = vpath;
        }
    }
}