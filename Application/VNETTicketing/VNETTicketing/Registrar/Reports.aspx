﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Registrar.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="VNETTicketing.Registrar.Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <!-- table export css -->
    <link href="../Assets/css/tableexport.min.css" rel="stylesheet">
    <link href="../Assets/css/collections.css" rel="stylesheet" />
        <style>
    #table {
        display: none;
    }
    .modal-header {
        background: #f9f9f9;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row dashboard">
        <div class="col-lg-12">
            <h1 class="page-header">Reports</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <center>
                    <form class="form-inline" role="form" style="position: relative;">
                        <div class="form-group">
                            <label for="pwd">Events:</label>
                            <select class="form-control" id="sel1">
                                <option>Event 1</option>
                                <option>Event 2</option>
                                <option>Event 3</option>
                                <option>Event 4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="from">From:</label>
                            <input type="text" class="form-control" id="from">
                        </div>
                        <div class="form-group">
                            <label for="to">To:</label>
                            <input type="text" class="form-control" id="to">
                        </div>
                        <br>
                        <br>
                    </form>
                    <button class="btn btn-primary" id="submit">Get reports</button>
                </center>
    </div>
    <br>
    <div class="row" id="table">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td><b>Event name</b></td>
                    <td>Event 1</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Registrar name</b></td>
                    <td>Moe</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>From</b></td>
                    <td>08/18/2016 9:00 AM</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>To</b></td>
                    <td>08/19/2016 12:00 AM</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Amount collected</b></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Cash</td>
                    <td>USD 2,200.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Credit</td>
                    <td>USD 4,050.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>AR</td>
                    <td>USD 4,400.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>FOC</td>
                    <td>USD 0.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Transfer</td>
                    <td>USD 0.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Total</b></td>
                    <td>USD 10,650.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Ticket issued</b></td>
                    <td></td>
                    <td><b>Total</b></td>
                </tr>
                <tr>
                    <td>Cash - 550 USD</td>
                    <td>4</td>
                    <td>USD 2,200.00</td>
                </tr>
                <tr>
                    <td>Credit card - 450 USD</td>
                    <td>9</td>
                    <td>USD 4,050.00</td>
                </tr>
                <tr>
                    <td>AR - 550 USD</td>
                    <td>8</td>
                    <td>USD 4,400.00</td>
                </tr>
                <tr>
                    <td>FOC</td>
                    <td>0</td>
                    <td>USD 0.00</td>
                </tr>
                <tr>
                    <td>Transfer</td>
                    <td>0</td>
                    <td>USD 0.00</td>
                </tr>
                <tr>
                    <td><b>Total</b></td>
                    <td><b>0</b></td>
                    <td><b>USD 10,650.00</b></td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- jQuery -->
    <script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- moment js -->
    <script src="../Assets/js/moment.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- bootstrap datetime picker -->
    <script src="../Assets/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- xlxs core js -->
    <script src="../Assets/js/xlsx.core.min.js"></script>
    <!-- Blob js -->
    <script src="../Assets/js/Blob.js"></script>
    <!-- file saver js -->
    <script src="../Assets/js/FileSaver.js"></script>
    <!-- table export js -->
    <script src="../Assets/js/tableexport.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../Assets/dist/js/sb-admin-2.js"></script>
    <script>
        $(function () {
            $('#from').datetimepicker();
            $('#to').datetimepicker();
        });
        $(document).ready(function (e) {
            $('#submit').click(function () {
                $('#table').show();
                e.preventDefault();
            })
            console.log(e);
        });
        $('#export').click(function (e) {
            window.open('data:application/vnd.ms-excel,' + $('#table').html());
            e.preventDefault();
        });
        $("table").tableExport({
            bootstrap: true,
            formats: ["xls"]
        });
    </script>
</asp:Content>
