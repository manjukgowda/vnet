﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VNETTicketing
{
    public partial class Message : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["message"] != null)
            {
                string msg = Convert.ToString(Session["message"]);
                if (msg == "1")
                {
                    lblMessage.Text = "Your Email-ID has been verified successfully!";
                }
                else if (msg == "2")
                {
                    lblMessage.Text = "";
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
    }
}