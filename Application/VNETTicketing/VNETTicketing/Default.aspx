﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VNETTicketing.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>The V</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="Assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- <link href="../bower_components/bootstrap/dist/css/bootstrap-responsive.min.css" rel="stylesheet"> -->
    <link href="Assets/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="Assets/css/test.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="header">
            <div class="headerContent">
                <div class="logo-wrap">
                    <div class="col-md-6 pull-left">
                        <div class="site-header">
                            <a href="#">
                                <img src="Assets/images/V-Logo.png" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-md-6 pull-right">
                        <div class="contents pull-right">
                            <i class="glyphicon glyphicon-log-in"></i><a href="Login.aspx">Login</a>
                        </div>
                        <!-- <div class="contents1 pull-right">
        				<div class="col-md-5">
        					<label class="currency">Currency</label>	
        				</div>
        				<div class="col-md-7">
	        				<select class="form-control">
	        					<option>MYR</option>
	        					<option>USD</option>
	        				</select>
        				</div>
        			</div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="row">
                <div class="container">
                    <p class="text-center lead">V Global Management Sdn. Bhd</p>
                    <p class="text-center lead">(472571-M)</p>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="container">
                    <div class="content">
                        <p class="text-center"><b>Contact:</b></p>
                        <p class="text-center"><b>Victor David</b></p>
                        <p class="text-center">Tel: (+603) 7965 8299 ext. 8208</p>
                        <p class="text-center">Mobile: (+6012) 550 2132</p>
                        <p class="text-center">Email: victor.d@the-v.net</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
