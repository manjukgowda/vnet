﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="VNETTicketing.User.Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Reports</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Booking ID</th>
                            <th>Ticket No</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>
    <!-- data tables css -->
    <link rel="stylesheet" type="text/css" href="../Assets/css/modify-events.css" />
    <link rel="stylesheet" type="text/css" href="../Assets/css/jquery.dataTables.css" /><link rel="stylesheet" type="text/css" href="../Assets/css/responsive.bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="../Assets/css/responsive.dataTables.min.css" />

    <!-- bootstrap switch css -->
    <link rel="stylesheet" type="text/css" href="../Assets/css/bootstrap-switch.min.css" />
    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <link rel="stylesheet" type="text/css" href="../Assets/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/css/buttons.dataTables.min.css">
    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="../Assets/js/jszip.min.js"></script>
    <script type="text/javascript" src="../Assets/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vfs_fonts.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.print.min.js"></script>
    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $('#empTable').DataTable({
            columns: [
                { 'data': 'EventName' },
                { 'data': 'strBookingDate' },
                { 'data': 'BookingId' },
                { 'data': 'TicketNo' },
                { 'data': 'Price' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            dom: 'Bfrtip',
            sAjaxSource: 'UserReports.asmx/GetTicketsReports',
            sServerMethod: 'Post',
            aoColumnDefs: [
                { "data": 'EventName', "bSortable": true },
                { "data": 'strBookingDate', "bSortable": true },
                { "data": 'BookingId', "bSortable": false },
                { "data": 'TicketNo', "bSortable": true },
                { "data": 'Price', "bSortable": true }
            ],  
            select: false,
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
            }]
        });
    </script>
</asp:Content>
