﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    public partial class EventBookingDetail : System.Web.UI.Page
    {
        int count;
        NewBookingClass obj;
        IridClass objirid;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Eventproceed"] == null)
                Response.Redirect("Events.aspx");
            if (!IsPostBack)
            {
                if (Session["Eventproceed"] != null)
                {
                    EventClass.EventViewModel objpro = new EventClass.EventViewModel();
                    objpro = (EventClass.EventViewModel)Session["Eventproceed"];
                    txtEventName.Text = objpro.EventName;


                    int eventId = objpro.EventId;
                    int ticketTypeID = objpro.EventTicketTypeSelectedValue;
                    var evt = new EventClass();
                    EventClass.TicketTypeViewModel objTicket = evt.GetEventPrice(eventId, ticketTypeID);
                    decimal price = Convert.ToDecimal(objTicket.EventPrice);
                    decimal BasePrice = CurrencyHelper.ConvertBaseCurrency(objTicket.CurrencyModeId, price);
                    decimal net = BasePrice * Convert.ToDecimal(objpro.TicketCountSelected);

                    objpro.EventPrice = price;
                    objpro.NetAmount = net;
                    objpro.PromoCodeApp = "";
                    objpro.Discount = 0;
                    Session.Remove("Eventproceed");
                    Session["Eventproceed"] = objpro;

                    txtTotalPrice.Text = net.ToString();
                    txtNetPrice.Text = net.ToString();
                    txtEventPrice.Text = BasePrice.ToString();
                    txtPromoCode.ReadOnly = false;
                    txtPromoCode.Text = "";
                    txtDiscount.Text = "0";

                    txtTicketType.Text = objpro.EventTicketTypeSelected;
                    txtTicketNo.Text = objpro.TicketCountSelected;
                    txtGroup.Text = objpro.GroupSelected == "Select group" ? "NA" : objpro.GroupSelected;
                    List<Participants> participants = new List<Participants>();
                    count = Convert.ToInt32(objpro.TicketCountSelected);
                    for (int i = 0; i < count; i++)
                    {
                        Participants p = new Participants();
                        p.Id = i + 1;
                        participants.Add(p);
                    }
                    rpt_participants.DataSource = participants;
                    rpt_participants.DataBind();

                }
                else
                    Response.Redirect("Events.aspx");
            }
        }

        public class Participants
        {
            public int Id { get; set; }
        }



        protected void btnApplyPromo_Click(object sender, EventArgs e)
        {
            EventClass.EventViewModel objpro = new EventClass.EventViewModel();
            objpro = (EventClass.EventViewModel)Session["Eventproceed"];

            NewBookingClass obj = new NewBookingClass();
            //long BookingId = Convert.ToInt64(Session["BookingId"]);
            //var book = obj.GetBookingDetails(BookingId);
            decimal totalAmount = Convert.ToDecimal(txtTotalPrice.Text);
            int eventId = objpro.EventId;
            string promocode = txtPromoCode.Text.Trim();

            if (obj.IsValidPromoCode(eventId, promocode))
            {
                var promo = obj.GetPromoCode(eventId, promocode);
                decimal baseDiscount = CurrencyHelper.ConvertBaseCurrency(promo.CouponCurrencyMode.Value, Convert.ToDecimal(promo.CouponValue));
                decimal discount = baseDiscount * Convert.ToInt32(txtTicketNo.Text);
                txtDiscount.Text = discount.ToString();
                //int minimumAmount = Convert.ToInt32(promo.MinimumAmount);

                //if (totalAmount > minimumAmount)
                //{
                decimal netAmount = Convert.ToDecimal(totalAmount) - discount;
                txtNetPrice.Text = netAmount.ToString();
                objpro.PromoCodeApp = txtPromoCode.Text;

                //EventClass.EventViewModel objProceed = new EventClass.EventViewModel();
                //objProceed.PromoCodeApp = promocode;
                objpro.NetAmount = netAmount;
                objpro.Discount = discount;
                Session.Remove("Eventproceed");

                Session["Eventproceed"] = objpro;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('Promo Code has been applied succuessfully');", true);

                //               }
                //               else
                //               {
                //                               ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('');", true);
                //"This promocode is applied only for above " + minimumAmount + " " + promo.tblCurrencyMode.CurrencyName;
                //               }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('This is not a valid promo code for this event!/Sorry, limit for promo code is reached');", true);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('');", true);
        }


        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                Button btn = sender as Button;
                RepeaterItem item = btn.Parent as RepeaterItem;
                TextBox txtirid = item.FindControl("txtIrId") as TextBox;

                objirid = new IridClass();
                TextBox txtphonenumber = item.FindControl("txtPhoneNumber") as TextBox;
                TextBox txtEmailId = item.FindControl("txtEmailId") as TextBox;
                TextBox txtFullName = item.FindControl("txtFullName") as TextBox;
                if (txtirid.Text.Count() > 0)
                {
                    ds = objirid.GetUserDetails(txtirid.Text);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        txtphonenumber.Text = ds.Tables[0].Rows[0]["ContactNoMobile"].ToString();
                        txtEmailId.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                        txtFullName.Text = ds.Tables[0].Rows[0]["IRName"].ToString();
                    }
                    else
                    {
                        txtphonenumber.Text = "";
                        txtEmailId.Text = "";
                        txtFullName.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "InvalidIRID();", true);
                    }

                }

                else
                {

                    txtphonenumber.Text = "";
                    txtEmailId.Text = "";
                    txtFullName.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "InvalidIRID();", true);
                }


                //txtphonenumber.Text = txtirid.Text + "Phone Number";
                //txtEmailId.Text = txtirid.Text + "Email Id";
                //txtFullName.Text = txtirid.Text + " Name";
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('Please try again');", true);
            }
        }

        protected void btnProceedPayment_Click(object sender, EventArgs e)
        {
            var usr = SessionUsers.GetCurrentUser(4);
            EventClass.EventViewModel objpro = new EventClass.EventViewModel();
            if (Session["Eventproceed"] == null)
            {
                Response.Redirect("Events.aspx");
            }
            objpro = (EventClass.EventViewModel)Session["Eventproceed"];
            count = Convert.ToInt32(objpro.TicketCountSelected);
            int eventId = objpro.EventId;
            if (ValidationOfIRIds(eventId) == false)
                return;
            EventClass objEvent = new EventClass();
            int available = objEvent.GetAvailableTicketsCount(eventId);
            if (count <= available)
            {
                List<NewBookingClass.NewBookingDetails> bookingList = new List<NewBookingClass.NewBookingDetails>();
                obj = new NewBookingClass();
                foreach (RepeaterItem item in rpt_participants.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        TextBox txtIR = item.FindControl("txtIRId") as TextBox;
                        TextBox txtFName = item.FindControl("txtFullName") as TextBox;
                        TextBox txtEmailId = item.FindControl("txtEmailId") as TextBox;
                        NewBookingClass.NewBookingDetails objtemp = new NewBookingClass.NewBookingDetails();
                        objtemp.IRID = txtIR.Text;
                        objtemp.UName = txtFName.Text;
                        //objtemp.CurrencyMode = objpro.EventCurrencyType;
                        objtemp.EmailId = txtEmailId.Text;
                        objtemp.EventId = objpro.EventId;
                        objtemp.GroupId = Convert.ToInt32(objpro.GroupSelectedValue);
                        objtemp.NoTicket = Convert.ToInt32(objpro.TicketCountSelected);
                        objtemp.Price = objpro.EventPrice;// sessdetails.Price;
                        objtemp.Discount = objpro.Discount;
                        objtemp.NetAmount = objpro.NetAmount;
                        objtemp.TicketType = txtTicketType.Text;
                        objtemp.UserId = Convert.ToInt32(usr.UserId);
                        objtemp.AR = 0;
                        objtemp.CashPaid = 0;
                        objtemp.PaymentMode = "Online";
                        //objtemp.UserIds = sessdetails.UserIds;
                        objtemp.PromoCodeUsed = objpro.PromoCodeApp;
                        bookingList.Add(objtemp);
                    }
                }
                long bookingId = obj.AddUserTransaction(bookingList, Convert.ToInt32(usr.UserId), "User");
                string orderid = obj.GetBookingDetails(bookingId).EventBookingID;
                Session.Remove("Eventproceed");
                string email = bookingList[0].EmailId;

                //for (int i = 0; i < bookingList.Count; i++)
                //{
                //    MailSender.SendTransactionIntiationMsg(orderid, bookingList[i].EventName);
                //}
                string amount = objpro.NetAmount.ToString();//"02.99";
                string desc = objpro.EventName; //"DIGIReloadCouponRM30";
                string country = PaymentClass.PaymentHelperMethods.GetCountry();//"MY";
                string verifykey = PaymentClass.PaymentHelperMethods.GetVerifyKey();// "373b540e86a08e9f83b690c99ac11857";
                string merchantid = PaymentClass.PaymentHelperMethods.GetMerchantId();//"thevnet_Dev";
                //var vcode = md5(amount & merchantID & orderid & xxxxxxxxxxxx );
                // REPLACE xxxxxxxxxxxx with MOLPay Verify Key
                var vcode = MD5Alg.md5(String.Concat(amount, merchantid, orderid, verifykey));
                // REPLACE xxxxxxxxxxxx with MOLPay Verify Key
                //StringBuilder molipayurl = new StringBuilder("https://www.onlinepayment.com.my/MOLPay/pay/thevnet_Dev/?");
                StringBuilder molipayurl = new StringBuilder(PaymentClass.PaymentHelperMethods.GetMolipayURL());
                molipayurl.Append("amount=" + amount);
                molipayurl.Append("&orderid=" + orderid);
                //molipayurl.Append("&bill_name=" + name);
                molipayurl.Append("&bill_email=" + email);
                molipayurl.Append("&bill_desc=" + desc);
                molipayurl.Append("&country=" + country);
                molipayurl.Append("&vcode=" + vcode);
                Response.Redirect(molipayurl.ToString());
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "TicketsNotAvailable();", true);
            }

        }

        private bool ValidationOfIRIds(int eventid)
        {
            List<string> IrIds = new List<string>();
            foreach (RepeaterItem item in rpt_participants.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var objirid = new IridClass();
                    TextBox txtirid = item.FindControl("txtIrId") as TextBox;

                    var ds = objirid.GetUserDetails(txtirid.Text);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        if (IrIds.Contains(txtirid.Text) == false)
                        {
                            IrIds.Add(txtirid.Text);
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('You are trying to buy more than one ticket for " + txtirid.Text + "!')", true);
                            return false;
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('Invalid IR Id " + txtirid.Text + "!')", true);
                        return false; ;
                    }

                }
            }
            EventClass obj = new EventClass();
            foreach (var irid in IrIds)
            {
                if (obj.IsIRIdDuplicated(eventid, irid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('Ticket is already booked for " + irid + "!')", true);
                    return false;
                }
            }

            return true;
        }

    }

}