﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    public partial class Events : System.Web.UI.Page
    {
        EventClass obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadActiveEvents();
            }
        }

        private void LoadActiveEvents()
        {
            obj = new EventClass();
            rpt_events.DataSource = obj.GetAllEventsToServices();
            rpt_events.DataBind();
        }
    }
}