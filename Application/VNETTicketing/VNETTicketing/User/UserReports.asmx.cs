﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    /// <summary>
    /// Summary description for UserReports
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    
    public class UserReports : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(EnableSession=true)]
        public void GetTicketsReports()
        {
            EventClass obj = new EventClass();
            var usr  = SessionUsers.GetCurrentUser(4);
           
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            sortingModal.SortColumnName = iSortCol_0 == 0 ? "EventName" : iSortCol_0 == 1 ? "EventDate" : iSortCol_0 == 2 ? "BookingId" : iSortCol_0 == 3 ? "TicketNo" : "Price" ;
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            var allTickets = obj.GetUserTicketsReports().Where(x => x.BookedUserId == usr.UserId); 
            if (sortingModal.SearchString != "")
            {
                allTickets = allTickets.Where(x => x.BookingId.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.EventName.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.Price.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.TicketNo.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.strBookingDate.ToLower().Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredTickets = SortingAndPagingHelper.SortingAndPaging<EventClass.TicketsReport>(allTickets, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = allTickets.Count(),
                iTotalRecords = filteredTickets.Count(),
                data = filteredTickets
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }
    }
}
