﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="EventDetail.aspx.cs" Inherits="VNETTicketing.User.EventDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Assets/bower_components/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid padding_null page_content">
        <div class="row">
            <div class="col-md-11">
                <div class="thumbnail">
                    <asp:Image ID="imgEvent" CssClass="img-responsive" Width="800px" Height="300px" runat="server" AlternateText="" />
                    <div class="caption-full">
                        <h4>
                            <asp:Label Text="" runat="server" ID="lblEventName" CssClass="text-primary" /></h4>
                        <p>
                            <asp:Label Text="" runat="server" ID="lblEventDesc" CssClass="dl-horizontal" />
                        </p>
                    </div>
                    <div class="row margin_null">
                        <div class="col-md-4">
                            <!-- <div class="form-group">
                                        <label>Ticket Type</label>
                                        <input type="text" name="tickettype" value="" class="form-control" disabled>
                                        <p>*Ticket price based on ticket type</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Promo code</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <input type="text" name="" class="form-control" placeholder="Enter promo code here">    
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-primary">Confirm</button>
                                            </div>    
                                        </div>
                                    </div> -->
                            <div class="form-group">
                                <label for="eventfee">
                                    <h3>Ticket type</h3>
                                </label>
                                <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlTicketType" CssClass="form-control" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--<select class="form-control">
                                    <option>Type 1</option>
                                    <option>Type 2</option>
                                    <option>Type 3</option>
                                    <option>Type 4</option>
                                </select>--%>
                            </div>
                           
                            <div class="form-group">
                                <asp:Label Text="" ID="lblPromoStatus" runat="server" />
                            </div>
                            <div class="form-group">
                                <label for="eventfee">
                                    <h3>Event fee</h3>
                                </label>
                                <p class="lead" id="eventfee">
                                    <asp:Label Text="" runat="server" ID="lblEventCurrency" />
                                    <asp:Label Text="" runat="server" ID="lblEventPrice" />
                                </p>
                            </div>
                            <div class="form-group">
                                <label for="eventfee">
                                    <h3>Group</h3>
                                </label>
                                <asp:DropDownList runat="server" ID="ddlGroup" CssClass="form-control">
                                </asp:DropDownList>
                                <%--                    <select class="form-control">
                                    <option>Group 1</option>
                                    <option>Group 2</option>
                                    <option>Group 3</option>
                                    <option>Group 4</option>
                                </select>--%>
                            </div>
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-5">
                            
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Book Now</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div>
                                            <div>
                                                <asp:Panel runat="server" ID="pnlBook">
                                                <div class="form-group">
                                                    <label class="col-md-7 padding_null ticket_qty">Tickets Quantity</label>
                                                    <%--<input class="col-md-5 padding_null text-center" readonly="true" id="ticketCount" type="text" value="1" name="ticketCount">--%>
                                                    <asp:TextBox runat="server" class="col-md-5 padding_null text-center" ID="ticketCount" Text="1" name="ticketCount" />
                                                </div>
                                                <div class="form-group">
                                                    <div class="row margin_null">
                                                        <label class="col-md-7 padding_null ticket_qty">Total Cost</label>
                                                        <label class="col-md-2 padding_null ticket_qty text-right" id="totalcurr"></label>
                                                        <label class="col-md-3 padding_null ticket_qty text-right" id="total"></label>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                            </div>
                                            <div class="form-group">
                                                <%--<button type="button" class="btn btn-primary center-block proceed_btn">Proceed</button>--%>
                                                <asp:Button Text="Proceed" CssClass="btn btn-primary center-block proceed_btn" ID="btnProceed" runat="server" OnClick="btnProceed_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row margin_null">
                    <div class="col-md-12 padding_null">
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs event_order_tabs">
                                    <li class="active"><a href="#tab1primary" data-toggle="tab">About Event</a></li>
                                    <li><a href="#tab2primary" data-toggle="tab">Venue</a></li>
                                    <li><a href="#tab3primary" data-toggle="tab">Terms & Conditions</a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1primary">
                                        <asp:Label Text="" ID="lblDesc1" runat="server" />
                                    </div>
                                    <div class="tab-pane fade" id="tab2primary">
                                        <ul type="disc">
                                            <li>
                                                <asp:Label Text="" ID="lblPlace" runat="server" /></li>
                                            <li>
                                                <asp:Label Text="" ID="lblAddress1" runat="server" /></li>
                                            <li>
                                                <asp:Label Text="" ID="lblAddress2" runat="server" /></li>
                                            <li>
                                                <asp:Label Text="" ID="lblCity" runat="server" /></li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="tab3primary">
                                        By purchasing this ticket you ("ticket holder") agree to be bound by the terms and conditions specified herein and any other provision as may be specified from time to time by the Event/Show Organizer ("Promoter") and/or Venue Management/Owner ("Venue Owner").
                                        <br />
                                        <ol type="disc">
                                            <li>All ticket sales are final. Tickets may not be cancelled, refunded, redeemed for cash, nor exchanged under any circumstances. </li>
                                            <li>Failure to comply with any of the terms stated herein may result in non- admittance or expulsion from the event.</li>
                                            <li>Each ticket permits admission of one individual only. This ticket must be kept safely throughout the duration of the event and if requested, shall be required to be produced for verification.</li>
                                            <li>Any complaints regarding the event will be directed to and dealt with by the Promoter.</li>
                                            <li>The ticket holder voluntarily assumes all risks and danger incidental to the event for which the ticket is issued, whether occurring prior to, during, or after the event, including, but not limited to any injury, loss, damages or expenses incurred, untoward incidents during the event or by other spectators. In no event shall the Promoter be liable for any consequential or other losses or damages howsoever arising.

The ticket holder will further undertake all risks for loss of personal articles and     properties.
                                            </li>
                                            <li>The ticket holder voluntarily agrees that the Promoter, participants, and all their respective agents, officers, directors, owners and employees 
(“the Management”) shall be expressly released from any liability, regardless of whether injury or loss occurs before, during or after the event.
                                            </li>
                                            <li>Further and/or alternatively, if the Promoter is in any respect responsible for the occurrence for loss, damages or expenses, the same shall be limited to the original purchase price of the ticket.</li>
                                            <li>The Promoter reserves the right to refuse admission or expel any individual(s) whose conduct is objectionable or offensive or for any other reasonable cause or reason whatsoever. This shall remain at the sole and exclusive discretion of the Promoter.  Expulsion shall result in the forfeiture of any and all claims to refund or otherwise.</li>
                                            <li>The ticket holder shall be responsible for all ticket(s) purchased. The Promoter shall not be obliged to re-issue exchange or compensate for any damaged, lost, destroyed or stolen tickets.</li>
                                            <li>Any counterfeit and/or tampered tickets will not be honored and refused entry. Admission is subject to rules, laws, and bylaws of the venue.</li>
                                            <li>The ticket holder shall be responsible to take note of any entry conditions or restrictions imposed on infants in arms, children without tickets or any minimum children admission age.</li>
                                            <li>In accordance with the Venue Owner’s rules of premise usage; alcohol, illegal drugs, firearms and explosives shall be excluded from the premises.</li>
                                            <li>Glass bottles, plastic containers, aluminum cans and beverages are strictly prohibited from the event hall.</li>
                                            <li>Unauthorized photography or recording is strictly prohibited. No audio visual or cinematographic devices shall be brought in to the venue without prior written consent from the Promoter. Failure to comply shall result in expulsion from the event.</li>
                                            <li>Ticket holder consents to a reasonable inspection of his/her belongings prior to entry and/or at any point throughout the duration of the event to ensure adherence to the Venue Owner’s rules of premise usage.</li>
                                            <li>Ticket(s) shall not be resold or offered for any commercial purpose without the express written consent of the producers or Promoters.</li>
                                            <li>The Promoter may use the ticket holder’s image or likeness in any live or recorded video display, photograph or picture.</li>
                                            <li>The Promoter may amend the event programme, event times, seating arrangements and audience capacity without prior notice.</li>
                                            <li>The Promoter may postpone, cancel, interrupt or stop the event due to adverse weather, dangerous situations or any other force majeure circumstances beyond its reasonable control.</li>
                                        </ol>
                                        <%--<ul type="disc">
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                                        </ul>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Upcoming Events -->
            <div class="col-md-1"></div>
            <%--<div class="col-md-3">
                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Upcoming Events</h4>
                    <div class="thumbnail">
                        <img src="http://placehold.it/320x150" alt="">
                        <div class="caption">
                            <h4 class="pull-right">$24.99</h4>
                            <h4><a href="#">First Event</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                        </div>
                        <div class="row margin_null center-block text-center buy_button">
                            <a class="btn btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div id="noeventModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Event details page</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Sorry, we didn't find any event details.</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="redirectevent();" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="promoalertModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Promo code alert</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Ticket count cannot be changed after applying promo code. Are you sure to proceed with promo code ?</label>
                </div>
                <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelredirect();">Close</button>
                            <button type="button" class="btn btn-primary">Proceed with promo</button>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function () {
            
            //var efee = $('#eventfee').text().trim();
            var efee = $("#ContentPlaceHolder1_lblEventPrice").text();
            var ecurr = $("#ContentPlaceHolder1_lblEventCurrency").text();
            if ($("#ContentPlaceHolder1_ContentPlaceHolder1_lblPromoStatus") == "") {
                $("#ContentPlaceHolder1_ticketCount").val('1');
                $("#totalcurr").text(ecurr);
            }
            else {
                var c = $("#ContentPlaceHolder1_ticketCount").val() * efee;
                $("#total").text(c);
            }
            //var total = $('#total').text(efee);
            $("#ContentPlaceHolder1_ticketCount").TouchSpin({
                min: 1,
                max: 10
            });


            $("#ContentPlaceHolder1_ticketCount").TouchSpin().on('touchspin.on.startupspin', function () {
                
                
                    var a = this.value * $('#ContentPlaceHolder1_lblEventPrice').text();//total;
                    //var c = $("#ContentPlaceHolder1_lblEventCurrency").text() + " " + a;
                    console.log(this.value);

                    $("#ContentPlaceHolder1_ticketCount").val(this.value);
                    $('#total').text(a);
                
            });

            $("#ContentPlaceHolder1_ticketCount").TouchSpin().on('touchspin.on.startdownspin', function () {
                //
                var lblprice = parseInt($("#ContentPlaceHolder1_lblEventPrice").text());
                var total = parseInt($("#total").text());
                //if($("#ContentPlaceHolder1_lblEventPrice").text() != $("#total").text())
                if (total > lblprice) {
                    var a = $("#total").text() - $('#ContentPlaceHolder1_lblEventPrice').text();//total;
                    $('#total').text(a);
                }
                $("#ContentPlaceHolder1_ticketCount").val(this.value);
            });


            $("#ContentPlaceHolder1_ticketCount").blur(function () {
                $("#ContentPlaceHolder1_ticketCount").val('1');
                console.log($("#ContentPlaceHolder1_lblEventPrice").val());
                $("#total").text($("#ContentPlaceHolder1_lblEventPrice").text());
                alert('Please use the + or - button');
            });
            //$("#ticketCount").TouchSpin({
            //    min: 0,
            //    max: 1,
            //    stepinterval: 50
            //});
            //$("#ticketCount").TouchSpin().on('touchspin.on.startspin', function () {
            //    $('#total').text(this.value * total);
            //});
            /*$(".selectpicker").change(function() {
                var selectTicket = $(".selectpicker option:selected").val();
                $('#total').text('$' + selectTicket);
                $("#ticketCount").val(1);
                $("#ticketCount").TouchSpin().on('touchspin.on.startspin', function() {
                    $('#total').text('$' + (this.value * selectTicket));
                });
            });*/
            //$('.proceed_btn').click(function (e) {
            //    var efee = $('#eventfee').text().trim();
            //    alert(efee);
            //});
        });

        function noeventModal() {
            $('#noeventModal').modal('show');
        }

        function redirectevent() {
            window.location.href = "http://" + window.location.host + "/User/Events.aspx";
        }
        function showticketlimitalert()
        {
            $('#promoalertModal').modal('show');
        }

        function cancelredirect() {
            return false;
        }
    </script>
</asp:Content>
