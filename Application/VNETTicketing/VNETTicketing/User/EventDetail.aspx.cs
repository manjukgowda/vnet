﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    public partial class EventDetail : System.Web.UI.Page
    {
        EventClass obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Request.QueryString["EventId"] != null)
                {
                    LoadEventDetails(Request.QueryString["EventId"].ToString());
                }
            }
        }

        private void LoadEventDetails(string eid)
        {
            obj = new EventClass();
            var objEventDetail = obj.GetEventsDetailsByEventId(Convert.ToInt32(eid));
            if (objEventDetail != null)
            {
                imgEvent.ImageUrl = objEventDetail.ImagePath;

                ddlGroup.DataSource = objEventDetail.Groups;
                ddlGroup.DataTextField = "GroupName";
                ddlGroup.DataValueField = "GroupId";
                ddlGroup.DataBind();
                ddlGroup.Items.Insert(0, new ListItem { Selected = true, Text = "Select group", Value = "0" });

                List<EventClass.TicketsPrices> o = new List<EventClass.TicketsPrices>();
                o.Add(objEventDetail.TicketPrice[0]);

                ddlTicketType.DataSource = o;
                ddlTicketType.DataTextField = "TicketType";
                ddlTicketType.DataValueField = "TicketTypeId";
                ddlTicketType.DataBind();

                lblEventName.Text = objEventDetail.EventName;
                lblDesc1.Text = lblEventDesc.Text = objEventDetail.EventDescription;
                lblAddress1.Text = objEventDetail.Address1;
                lblAddress2.Text = objEventDetail.Address2;

                lblPlace.Text = objEventDetail.Venue;
                lblCity.Text = objEventDetail.City + " - " + objEventDetail.PostalCode;
                ddlTicketType_SelectedIndexChanged(null, null);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "noeventModal();", true);
            }
        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int eventId = Convert.ToInt32(Request.QueryString["EventId"]);
                int ticketTypeID = Convert.ToInt32(ddlTicketType.SelectedValue);
                obj = new EventClass();
                EventClass.TicketTypeViewModel objTicket = obj.GetEventPrice(eventId, ticketTypeID);
                lblEventPrice.Text = objTicket.EventPrice;
                lblEventCurrency.Text = objTicket.TicketTypeCurrency;
            }
            catch
            {
                lblEventPrice.Text = "--";
                lblEventCurrency.Text = "--";
            }
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            EventClass.EventViewModel objProceed = new EventClass.EventViewModel();
            if (Session["Eventproceed"] != null)
            {
                objProceed = (EventClass.EventViewModel)Session["Eventproceed"];
            }
            
           // objProceed.EventCurrencyType = lblEventCurrency.Text;
            objProceed.EventId = Convert.ToInt32(Request.QueryString["EventId"]);
            objProceed.EventName =  lblEventName.Text;
            //objProceed.EventPrice = lblEventPrice.Text;
            objProceed.EventTicketTypeSelected = ddlTicketType.SelectedItem.Text;
            objProceed.EventTicketTypeSelectedValue = Convert.ToInt32(ddlTicketType.SelectedValue);
            objProceed.TicketCountSelected = ticketCount.Text;
            objProceed.GroupSelected = ddlGroup.SelectedItem.Text;
            objProceed.GroupSelectedValue = ddlGroup.SelectedItem.Value;
            Session["Eventproceed"] = objProceed;
            Response.Redirect("EventBookingDetail.aspx");
            //Request.QueryString["EventId"].ToString();
        }
    }
}