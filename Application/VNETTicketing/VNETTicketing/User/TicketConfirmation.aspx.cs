﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    public partial class TicketConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowDetails();
        }

        public void ShowDetails()
        {
            if (Session["BookingId"] != null)
            {
                NewBookingClass obj = new NewBookingClass();
                long BookingId = Convert.ToInt64(Session["BookingId"]);
                string bookId = obj.GetBookingDetails(BookingId).EventBookingID;
                NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);

                lblBookingId.Text = objdetails.BookingId.ToString();
                lblEvent.Text = objdetails.EventName;
                lblVenue.Text = objdetails.Venue;
                lblEventDateTime.Text = objdetails.EventDateTime.ToString();
                lblNoOfTicket.Text = objdetails.NoTicket.ToString();
                lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
                lblDiscount.Text = Convert.ToString(objdetails.Discount);
                lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
                lblModeOfPayment.Text = objdetails.PaymentMode;
                lblStatus.Text = objdetails.Status;
                gvUsers.DataSource = objdetails.BookingUserDetail;
                gvUsers.DataBind();
                string CustomerName = "Customer";
                if (objdetails.BookingUserDetail.Count > 0)
                    CustomerName = objdetails.BookingUserDetail[0].UName;

                string EmailID = "manjumysore.k@gmail.com";
                if (objdetails.BookingUserDetail.Count > 0)
                    EmailID = objdetails.BookingUserDetail[0].EmailId;

                //GenerateBarcode(BookingId.ToString(), objdetails.EventName, CustomerName,EmailID);
                SessionUser usr = (SessionUser) Session["usr"] ;
                EventClass.EventViewModel objpro = (EventClass.EventViewModel)Session["objpro"];
                ProcessMoliPay(usr, objpro);
            }
        }

        public void GenerateBarcode(string bookingId, string eventname, string CustomerName,string EmailID)
        {

            string vpath = "~\\Images";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            vpath = "~\\Images\\Barcodes";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            string guid = Guid.NewGuid().ToString().Substring(1, 10);
            vpath = "~\\Images\\Barcodes\\" + guid + ".png";
            string server = ConfigurationManager.AppSettings["server"].ToString();
            string vpdf = "~\\Images\\Barcodespdf\\" + guid + ".pdf";

            string fullpath = server + "/Images/Barcodes/" + guid + ".png";
            string fullpathpdf = server + "/Images/Barcodespdf/" + guid + ".pdf";

            BarCodeHelper obj = new BarCodeHelper();
            obj.GenerateBarCode(bookingId, Server.MapPath(vpath));

            imgBarCode.ImageUrl = fullpath;
            //Export(vpdf);
            //MailSender.SendTicketConfirmationEmail(CustomerName, eventname, fullpathpdf,EmailID);
        }

        public void Export(string vpdf)
        {
            
                StringWriter stringWriter = new StringWriter();

                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
                maindiv.RenderControl(htmlTextWriter);

                StringReader stringReader = new StringReader(stringWriter.ToString());

                Document Doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

                HTMLWorker htmlparser = new HTMLWorker(Doc);

                PdfWriter.GetInstance(Doc, new FileStream(Server.MapPath(vpdf), FileMode.Create));

                Doc.Open();

                htmlparser.Parse(stringReader);

                Doc.Close();
            
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


        private void ProcessMoliPay(SessionUser usr, EventClass.EventViewModel objpro)
        {
            //string amount = objpro.NetAmount.ToString();//"02.99";
            //string orderid = "ORD" + usr.UserId + objpro.EventId + DateTime.Now.ToString("ddMMyy") + DateTime.Now.ToString("HHmmss");
            //string mobile = "016-2341234";
            //string desc = objpro.EventName; //"DIGIReloadCouponRM30";
            //string country = PaymentClass.PaymentHelperMethods.GetCountry();//"MY";
            //string verifykey = PaymentClass.PaymentHelperMethods.GetVerifyKey();// "373b540e86a08e9f83b690c99ac11857";
            //string merchantid = PaymentClass.PaymentHelperMethods.GetMerchantId();//"thevnet_Dev";
            ////var vcode = md5(amount & merchantID & orderid & xxxxxxxxxxxx );
            //// REPLACE xxxxxxxxxxxx with MOLPay Verify Key
            //var vcode = md5(String.Concat(amount, merchantid, orderid, verifykey));
            //// REPLACE xxxxxxxxxxxx with MOLPay Verify Key
            ////StringBuilder molipayurl = new StringBuilder("https://www.onlinepayment.com.my/MOLPay/pay/thevnet_Dev/?");
            //StringBuilder molipayurl = new StringBuilder(PaymentClass.PaymentHelperMethods.GetMolipayURL());
            //molipayurl.Append("amount=" + amount);
            //molipayurl.Append("&orderid=" + orderid);
            ////molipayurl.Append("&bill_name=" + name);
            ////molipayurl.Append("&bill_email=" + email);
            //molipayurl.Append("&bill_mobile=" + mobile);
            //molipayurl.Append("&bill_desc=" + desc);
            //molipayurl.Append("&country=" + country);
            //molipayurl.Append("&vcode=" + vcode);
            //Response.Redirect(molipayurl.ToString());
        }

        
    }
}