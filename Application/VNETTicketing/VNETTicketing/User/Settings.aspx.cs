﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing.User
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var usr = SessionUsers.GetCurrentUser(4);
                SettingsClass obj = new SettingsClass();
                VNETTicketing.Models.User.SettingsClass.Profiles p = new SettingsClass.Profiles();
                p = obj.GetProfile(usr.UserId);
                txtFullName.Text = p.FullName;
                ddlCountry.SelectedValue = p.Country;
                txtEmail.Text = p.EmailId;
                txtEmail.ReadOnly = true;
                //txtFullName.ReadOnly = true;
                //ddlCountry.Enabled = false;
            }
        }

        protected void btnSavePwd_Click(object sender, EventArgs e)
        {
            SettingsClass obj = new SettingsClass();
            var usr = SessionUsers.GetCurrentUser(4);
            if (obj.ChangePassword(usr.UserId, txtOldPassword.Text, txtNewPassword.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalerror();", true);
            }
        }

        protected void btnSaveProfile_Click(object sender, EventArgs e)
        {
            SettingsClass obj = new SettingsClass();
            var usr = SessionUsers.GetCurrentUser(4);
            if (obj.UpdateProfile(txtEmail.Text,txtFullName.Text,ddlCountry.SelectedItem.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openProfileModal();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openProfileModalerror();", true);
            }
        }
    }
}