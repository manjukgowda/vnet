﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="EventBookingDetail.aspx.cs" Inherits="VNETTicketing.User.EventBookingDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .uppercase {
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid padding_null page_content">
        <h4 class="modal-title">Booking details</h4>
        <div class="row">
            <div class="col-md-11">
                <section>
                    <div class="wizard">
                        <%--<div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                            <span class="badge">1</span> <b>Check ticket details</b>
                                        </a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                            <span class="badge">2</span> <b>Attendee details</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>--%>
                        <%--<form role="form"></form>--%>
                        <div class="tab-content">
                            <%--<div class="tab-pane active" role="tabpanel" id="step1">--%>
                            <div class="row" id="check">
                                <div class="container-fluid">
                                    <div class="field">
                                        <div class="col-md-5">
                                            <p class="text-center">Event name</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="ename" disabled class="form-control">--%>
                                            <asp:TextBox runat="server" ReadOnly="true" CssClass="form-control" ID="txtEventName" />
                                        </div>
                                    </div>
                                    <div class="field2">
                                        <div class="col-md-5">
                                            <p class="text-center">Tiket type</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="ttype" disabled class="form-control">--%>
                                            <asp:TextBox ID="txtTicketType" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="field1">
                                        <div class="col-md-5">
                                            <p class="text-center">Event Price Per Person (RM MYR)</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="price" disabled class="form-control">--%>
                                            <asp:TextBox ID="txtEventPrice" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="field3">
                                        <div class="col-md-5">
                                            <p class="text-center">No. of Ticket</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="tquantity" disabled class="form-control">--%>
                                            <asp:TextBox ID="txtTicketNo" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="field7">
                                        <div class="col-md-5">
                                            <p class="text-center">Total Price (RM MYR)</p>
                                        </div>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTotalPrice" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="field4">
                                        <div class="col-md-5">
                                            <p class="text-center">Promo code</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="pcode" disabled class="form-control">--%>
                                            <asp:TextBox ID="txtPromoCode" runat="server" CssClass="form-control uppercase" />
                                        </div>
                                    </div>
                                    <div class="field4">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                            <asp:Button Text="Apply Promo Code" ID="btnVerify" CssClass="btn btn-primary" runat="server" OnClick="btnApplyPromo_Click" />
                                        </div>
                                    </div>
                                    <div class="field7">
                                        <div class="col-md-5">
                                            <p class="text-center">Discount (RM MYR)</p>
                                        </div>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDiscount" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="field7">
                                        <div class="col-md-5">
                                            <p class="text-center">Net Price (RM MYR)</p>
                                        </div>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtNetPrice" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>


                                    <div class="field5">
                                        <div class="col-md-5">
                                            <p class="text-center">Group</p>
                                        </div>
                                        <div class="col-md-7">
                                            <%--<input type="text" name="group" disabled class="form-control">--%>
                                            <asp:TextBox ID="txtGroup" runat="server" ReadOnly="true" CssClass="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--<ul class="list-inline pull-right">
                                            <li>
                                                <button type="button" class="btn btn-primary next-step">Proceed</button>
                                            </li>
                                        </ul>--%>
                            <%--</div>--%>
                            <%--<div class="tab-pane" role="tabpanel" id="step2">--%>
                            <br />
                            <div class="row center-block" id="Div1">
                                <div class="container-fluid">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="row center-block">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Attendee details</div>
                                                <div class="panel-body table">
                                                    <asp:Repeater runat="server" ID="rpt_participants">
                                                        <HeaderTemplate>
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <label>IR ID</label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label>Full Name</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>Email ID</label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>Phone number</label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label></label>
                                                                </div>
                                                                <hr />
                                                            </div>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%--<div class="col-sm-12 col-lg-8 col-md-8">
                                                                <div class="login-fields">
                                                                    <div class="field detailspart">
                                                                        <div class="field detailspart">--%>
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <asp:TextBox runat="server" ID="txtIrId" CssClass="form-control clstxtirid" />
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <asp:TextBox runat="server" ID="txtFullName" ReadOnly="true" CssClass="form-control" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="AB" ErrorMessage="Full name can't be empty!" ControlToValidate="txtFullName"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:TextBox runat="server" ID="txtEmailId" CssClass="form-control" />
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:TextBox runat="server" ID="txtPhoneNumber" ReadOnly="true" CssClass="form-control" />
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <asp:Button Text="Verify" CssClass="btn btn-primary" ID="btnVerify" runat="server" OnClick="btnVerify_Click" />
                                                                </div>
                                                            </div>
                                                            <hr />
                                                            <%--</div>
                                                                    </div>
                                                                </div>
                                                            </div>--%>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <%--<li>
                                                <button type="button" class="btn btn-danger prev-step">Previous</button>
                                            </li>--%>

                                <%--<div class="checkbox">--%>
                                <li class="">
                                    <label>
                                        <input type="checkbox" id="chkTermsCond" value=""></label>
                                </li>
                                <li><a data-toggle="modal" data-target="#termsModal" title="Click to read terms and condition">I accept terms and condtions</a></li>
                                <%--</div>--%>

                                <li>
                                    <asp:Button Text="Proceed to payment" ID="btnProceedPayment" ValidationGroup="AB" disabled="disabled" CssClass="btn btn-primary next-step" OnClick="btnProceedPayment_Click" runat="server" />
                                    <%--<button id="btnProceedPayment" type="button" class="btn btn-primary next-step">Proceed to payment</button>--%>
                                </li>
                            </ul>
                            <%--</div>--%>
                            <div class="clearfix"></div>
                            <%--</div>--%>
                        </div>
                </section>
            </div>
            <!-- Upcoming Events -->
            <div class="col-md-1"></div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div id="noeventModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Event details page</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Sorry, we didn't find any event details.</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="redirectevent();" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="termsModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Terms and Condition</h4>
                </div>
                <div class="modal-body">
                    <p>
                        By purchasing this ticket you ("ticket holder") agree to be bound by the terms and conditions specified herein and any other provision as may be specified from time to time by the Event/Show Organizer ("Promoter") and/or Venue Management/Owner ("Venue Owner").
                                        <br />
                        <ol type="disc">
                            <li>All ticket sales are final. Tickets may not be cancelled, refunded, redeemed for cash, nor exchanged under any circumstances. </li>
                            <li>Failure to comply with any of the terms stated herein may result in non- admittance or expulsion from the event.</li>
                            <li>Each ticket permits admission of one individual only. This ticket must be kept safely throughout the duration of the event and if requested, shall be required to be produced for verification.</li>
                            <li>Any complaints regarding the event will be directed to and dealt with by the Promoter.</li>
                            <li>The ticket holder voluntarily assumes all risks and danger incidental to the event for which the ticket is issued, whether occurring prior to, during, or after the event, including, but not limited to any injury, loss, damages or expenses incurred, untoward incidents during the event or by other spectators. In no event shall the Promoter be liable for any consequential or other losses or damages howsoever arising.

The ticket holder will further undertake all risks for loss of personal articles and     properties.
                            </li>
                            <li>The ticket holder voluntarily agrees that the Promoter, participants, and all their respective agents, officers, directors, owners and employees 
(“the Management”) shall be expressly released from any liability, regardless of whether injury or loss occurs before, during or after the event.
                            </li>
                            <li>Further and/or alternatively, if the Promoter is in any respect responsible for the occurrence for loss, damages or expenses, the same shall be limited to the original purchase price of the ticket.</li>
                            <li>The Promoter reserves the right to refuse admission or expel any individual(s) whose conduct is objectionable or offensive or for any other reasonable cause or reason whatsoever. This shall remain at the sole and exclusive discretion of the Promoter.  Expulsion shall result in the forfeiture of any and all claims to refund or otherwise.</li>
                            <li>The ticket holder shall be responsible for all ticket(s) purchased. The Promoter shall not be obliged to re-issue exchange or compensate for any damaged, lost, destroyed or stolen tickets.</li>
                            <li>Any counterfeit and/or tampered tickets will not be honored and refused entry. Admission is subject to rules, laws, and bylaws of the venue.</li>
                            <li>The ticket holder shall be responsible to take note of any entry conditions or restrictions imposed on infants in arms, children without tickets or any minimum children admission age.</li>
                            <li>In accordance with the Venue Owner’s rules of premise usage; alcohol, illegal drugs, firearms and explosives shall be excluded from the premises.</li>
                            <li>Glass bottles, plastic containers, aluminum cans and beverages are strictly prohibited from the event hall.</li>
                            <li>Unauthorized photography or recording is strictly prohibited. No audio visual or cinematographic devices shall be brought in to the venue without prior written consent from the Promoter. Failure to comply shall result in expulsion from the event.</li>
                            <li>Ticket holder consents to a reasonable inspection of his/her belongings prior to entry and/or at any point throughout the duration of the event to ensure adherence to the Venue Owner’s rules of premise usage.</li>
                            <li>Ticket(s) shall not be resold or offered for any commercial purpose without the express written consent of the producers or Promoters.</li>
                            <li>The Promoter may use the ticket holder’s image or likeness in any live or recorded video display, photograph or picture.</li>
                            <li>The Promoter may amend the event programme, event times, seating arrangements and audience capacity without prior notice.</li>
                            <li>The Promoter may postpone, cancel, interrupt or stop the event due to adverse weather, dangerous situations or any other force majeure circumstances beyond its reasonable control.</li>
                        </ol>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="TicketsNotAvailable" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Oops...</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Sorry, the required no. of tickets are not available at this moment!</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="InvalidIrid" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Oops...</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Sorry, you had entered invalid IRID!</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#chkTermsCond").change(function () {
                if ($(this).is(":checked")) {
                    $("#ContentPlaceHolder1_btnProceedPayment").removeAttr("disabled");
                }
                else {
                    $("#ContentPlaceHolder1_btnProceedPayment").attr("disabled", "disabled");
                }
            });
            
        });

        ////var efee = $('#eventfee').text().trim();
        //var efee = $("#ContentPlaceHolder1_lblEventPrice").text();
        //var ecurr = $("#ContentPlaceHolder1_lblEventCurrency").text();
        //$("#totalcurr").text(ecurr);
        //var total = $('#total').text(efee);
        //$("#ContentPlaceHolder1_ticketCount").TouchSpin({
        //    min: 1,
        //    max: 10
        //});


        //$("#ContentPlaceHolder1_ticketCount").TouchSpin().on('touchspin.on.startupspin', function () {
        //    var a = this.value * $('#ContentPlaceHolder1_lblEventPrice').text();//total;
        //    //var c = $("#ContentPlaceHolder1_lblEventCurrency").text() + " " + a;
        //    $('#total').text(a);
        //});

        //$("#ContentPlaceHolder1_ticketCount").TouchSpin().on('touchspin.on.startdownspin', function () {
        //    //
        //    var lblprice = parseInt($("#ContentPlaceHolder1_lblEventPrice").text());
        //    var total = parseInt($("#total").text());
        //    //if($("#ContentPlaceHolder1_lblEventPrice").text() != $("#total").text())
        //    if (total > lblprice) {
        //        var a = $("#total").text() - $('#ContentPlaceHolder1_lblEventPrice').text();//total;
        //        $('#total').text(a);
        //    }
        //});
        ////$("#ticketCount").TouchSpin({
        ////    min: 0,
        ////    max: 1,
        ////    stepinterval: 50
        ////});
        ////$("#ticketCount").TouchSpin().on('touchspin.on.startspin', function () {
        ////    $('#total').text(this.value * total);
        ////});
        ///*$(".selectpicker").change(function() {
        //    var selectTicket = $(".selectpicker option:selected").val();
        //    $('#total').text('$' + selectTicket);
        //    $("#ticketCount").val(1);
        //    $("#ticketCount").TouchSpin().on('touchspin.on.startspin', function() {
        //        $('#total').text('$' + (this.value * selectTicket));
        //    });
        //});*/
        //$('.proceed_btn').click(function (e) {
        //    var efee = $('#eventfee').text().trim();
        //    alert(efee);
        //});
        

        function noeventModal() {
            $('#noeventModal').modal('show');
        }

        function redirectevent() {
            window.location.href = "http://" + window.location.host + "/User/Events.aspx";
        }

        function TicketsNotAvailable() {
            $('#TicketsNotAvailable').modal('show');
        }

        function InvalidIRID() {
            $('#InvalidIrid').modal('show');
        }
    </script>

</asp:Content>
