﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="VNETTicketing.User.Events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid padding_null page_content">
        <div class="row">
            <div class="col-md-11">
                <div class="row carousel-holder">
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <asp:Repeater runat="server" ID="rpt_events">
                        <ItemTemplate>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="thumbnail">
                                    <div style="width:100%; height:200px;">
                                        <asp:Image ID="Image1" ImageUrl='<%# Bind("ImagePath", "{0}") %>' runat="server" />
                                    </div>
                                    <div class="caption">
                                        <h4 class="pull-right"><%#  DataBinder.Eval(Container, "DataItem.PriceToDisplay") %></h4>
                                        <h4 class="text-primary"><%# DataBinder.Eval(Container, "DataItem.EventName").ToString().Length > 12 ? DataBinder.Eval(Container, "DataItem.EventName").ToString().Substring(0,12)+"..." : DataBinder.Eval(Container, "DataItem.EventName").ToString()%>
                                        </h4>
                                        <p><%# DataBinder.Eval(Container, "DataItem.EventDescription")%></p>
                                    </div>
                                    <div class="row margin_null center-block text-center buy_button">
                                        <a class="btn btn-primary" href="../User/EventDetail.aspx?EventId=<%# DataBinder.Eval(Container,"DataItem.EventId") %>">Buy Ticket</a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$64.99</h4>
                                <h4><a href="#">Second Event</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="row margin_null center-block text-center buy_button">
                                <a class="btn btn-primary" href="#">Buy Ticket</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$74.99</h4>
                                <h4><a href="#">Third Event</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="row margin_null center-block text-center buy_button">
                                <a class="btn btn-primary" href="#">Buy Ticket</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$84.99</h4>
                                <h4><a href="#">Fourth Event</a></h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="row margin_null center-block text-center buy_button">
                                <a class="btn btn-primary" href="#">Buy Ticket</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="http://placehold.it/320x150" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$94.99</h4>
                                <h4><a href="#">Fifth Event</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore.</p>
                            </div>
                            <div class="row margin_null center-block text-center buy_button">
                                <a class="btn btn-primary" href="#">Buy Ticket</a>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
            <div class="col-md-1"></div>
            <!-- Upcoming Events -->
<%--            <div class="col-md-3">
                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Upcoming Events</h4>
                    <div class="thumbnail">
                        <img src="http://placehold.it/320x150" alt="">
                        <div class="caption">
                            <h4 class="pull-right">$24.99</h4>
                            <h4><a href="#">First Event</a>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                        </div>
                        <div class="row margin_null center-block text-center buy_button">
                            <a class="btn btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</asp:Content>
