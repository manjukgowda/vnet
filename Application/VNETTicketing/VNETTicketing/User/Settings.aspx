﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="VNETTicketing.User.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Assets/css/settings.css" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid padding_null page_content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Basic Information</h4>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#home" data-toggle="tab">Profile</a>
                            </li>
                            <li><a href="#profile" data-toggle="tab">Change Password</a>
                            </li>
                           <!-- <li><a href="#messages" data-toggle="tab">Notification</a>
                            </li> -->
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">

                                <div class="setting_fields">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Full Name</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="name" ControlToValidate="txtFullName" runat="server" ErrorMessage="Full name is required!"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Country</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCountry" CssClass="selectpicker" runat="server">
                                                <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                                                <asp:ListItem Text="India" Value="India"></asp:ListItem>
                                                <asp:ListItem Text="Dubai" Value="Dubai"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Email Id</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group text-center setting_btn">
                                        <asp:Button ID="btnSaveProfile" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="btnSaveProfile_Click" ValidationGroup="name" />
                                        <asp:Button ID="btnCancelProfile" CssClass="btn btn-primary" runat="server" Text="Cancel" />
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="profile">
                                <div class="setting_fields">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Old Password</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtOldPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">New Password</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ValidationGroup="pwd" ID="rfvpwd" ControlToValidate="txtNewPassword" runat="server" ErrorMessage="Its mandatory"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Confirm New Password</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtConfirmPwd" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvcpwd" ValidationGroup="pwd" runat="server" ControlToValidate="txtConfirmPwd" ErrorMessage="Confirm password must match!"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvpassword" ValidationGroup="pwd" ControlToValidate="txtConfirmPwd" ControlToCompare="txtNewPassword" runat="server" ErrorMessage="Password and Confirm Password are not matching!"></asp:CompareValidator>
                                        </div>
                                    </div>
                                    <div class="form-group text-center setting_btn">
                                        <asp:Button ID="btnSavePwd" CssClass="btn btn-primary" ValidationGroup="pwd" runat="server" Text="Save" OnClick="btnSavePwd_Click" />
                                        <asp:Button ID="btnCancelPwd" CssClass="btn btn-primary" runat="server" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="tab-pane fade" id="messages">
                                <h3>Notification Settings</h3>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="New messages alert" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Text="New notification alert" />
                                    </div>
                                </div>
                                <div class="form-group text-center setting_btn">
                                    <asp:Button ID="btnSaveNotification" CssClass="btn btn-primary" runat="server" Text="Save" />
                                    <asp:Button ID="btnCancelNotification" CssClass="btn btn-primary" runat="server" Text="Cancel" />
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->
    </div>
     <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Information</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Your password changed successfully!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
     <div id="modalError" class="modal fade" role="dialog">
           <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">You did not enter your correct old password!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
   
      <div id="myprofile" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Information</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Your profile updated successfully!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
     <div id="profileerror" class="modal fade" role="dialog">
           <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Something went wrong, please contact adminstrator</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
   
    <script>
        function openModal() {
            $('#myModal').modal('show');
        }
        function openModalerror() {
            $('#modalError').modal('show');
        }
        function openProfileModal() {
            $('#myprofile').modal('show');
        }
        function openProfileModalerror() {
            $('#profileerror').modal('show');
        }
    </script>
</asp:Content>
