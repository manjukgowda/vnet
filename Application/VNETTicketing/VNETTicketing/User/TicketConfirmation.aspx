﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserMaster.Master" AutoEventWireup="true" CodeBehind="TicketConfirmation.aspx.cs" Inherits="VNETTicketing.User.TicketConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div id="maindiv" runat="server">
        <div class="col-lg-12">
            <h1 class="page-header">Confirmation Letter</h1>
        </div>
        <div class="container-fluid padding_null page_content" id="DivIdToPrint">
            <h2 class="page-header">Booking Details</h2>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Booking ID</td>
                        <td>
                            <asp:Label Text="" ID="lblBookingId" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Event</td>
                        <td>
                            <asp:Label Text="" ID="lblEvent" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Venue</td>
                        <td>
                            <asp:Label Text="" ID="lblVenue" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Event Datetime</td>
                        <td>
                            <asp:Label Text="" ID="lblEventDateTime" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Number of ticket</td>
                        <td>
                            <asp:Label Text="" ID="lblNoOfTicket" runat="server" /></td>
                    </tr>
                    <%--<tr>
                    <td>Ticket Type</td>
                    <td>
                        <asp:Label Text="" ID="lblTicketType" runat="server" /></td>
                </tr>--%>
                    <tr>
                        <td>Total Amount</td>
                        <td>
                            <asp:Label Text="" ID="lblTotalAmount" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Discount</td>
                        <td>
                            <asp:Label Text="" ID="lblDiscount" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Net Amount</td>
                        <td>
                            <asp:Label Text="" ID="lblNetAmount" runat="server" /></td>
                    </tr>

                    <tr>
                        <td>Mode of payment</td>
                        <td>
                            <asp:Label Text="" ID="lblModeOfPayment" runat="server" /></td>
                    </tr>

                    <tr>
                        <td>Status</td>
                        <td>
                            <asp:Label Text="" ID="lblStatus" runat="server" /></td>
                    </tr>
                </tbody>
            </table>

            <h2 class="page-header">User Details</h2>
            <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
                <Columns>
                    <asp:BoundField DataField="TicketId" HeaderText="Ticket Id" />
                    <asp:BoundField DataField="IrId" HeaderText="Ir Id" />
                    <asp:BoundField DataField="UName" HeaderText="Name" />
                    <asp:BoundField DataField="TicketType" HeaderText="Ticket Type" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                </Columns>
            </asp:GridView>
                <br /><br /><br />
            <asp:Image ID="imgBarCode" runat="server" />
        </div>
    </div>
    
    <br />
    
</asp:Content>
