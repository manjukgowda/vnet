﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="DeleteEvent.aspx.cs" Inherits="VNETTicketing.SuperAdmin.DeleteEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Delete event</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="deleteeventmodal" eventid="0" data-toggle="modal" data-target="#myModal">Delete event</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="eventTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Event ID</th>
                            <th>Event Name</th>
                            <th>Venue Details</th>
                            <th>Address 1</th>
                            <th>Address 2</th>
                            <th>City</th>
                            <th>Transfer Fee</th>
                            <th>Postal Code</th>
                            <th>Event Start Date</th>
                            <th>Event End Date</th>
                            <%--<th>Booking Start Date</th>
                            <th>Booking End Date</th>--%>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <form id="deleteEventForm" action="#" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete event</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure, you want to delete this event?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="btnDeleteEvent" data-dismiss="modal">Okay</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#deleteeventmodal").hide();
            $("#btnDeleteEvent").click(function () {
                var eventid = $('#deleteeventmodal').attr('eventid');
                $.ajax({
                    type: "POST",
                    url: "EventServices.asmx/DeleteEvent",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ eventid: '" + eventid + "'}",
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) { console.log(data) }
                });
            });
        });


        $('#eventTable').DataTable({
            columns: [
                { 'data': 'EventId' },
               { 'data': 'EventName' },
               { 'data': 'Venue' },
               { 'data': 'Address1' },
               { 'data': 'Address2' },
               { 'data': 'City' },
               { 'data': 'AdminCharge' },
               { 'data': 'PostalCode' },
               { 'data': 'strEventStartDate' },
               { 'data': 'strEventEndDate' }
               //,
               //{ 'data': 'BookingStartDate' },
               //{ 'data': 'BookingEndDate' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'EventServices.asmx/LoadEvents',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'EventId', "bSortable": true },
        { "data": 'EventName', "bSortable": true },
        { "data": 'Venue', "bSortable": true },
        { "data": 'Address1', "bSortable": true },
        { "data": 'Address2', "bSortable": true },
        { "data": 'City', "bSortable": true },
        { "data": 'AdminCharge', "bSortable": true },
        { "data": 'PostalCode', "bSortable": true },
        { "data": 'strEventStartDate', "bSortable": true },
        { "data": 'strEventEndDate', "bSortable": true }
        //,
        //{ 'data': 'BookingStartDate', "bSortable": true },
        //       { 'data': 'BookingEndDate', "bSortable": true }
            ]
        });


        var table = $('#eventTable').dataTable();
        $('#eventTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                $("#deleteeventmodal").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var ss = $(this).children('td:first-child').text();
                $("#deleteeventmodal").attr('eventid', ss);
                $("#deleteeventmodal").show();
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();
    </script>

</asp:Content>

