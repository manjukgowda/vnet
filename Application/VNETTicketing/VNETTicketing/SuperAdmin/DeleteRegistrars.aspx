﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="DeleteRegistrars.aspx.cs" Inherits="VNETTicketing.SuperAdmin.DeleteRegistrars" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Delete registrars</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="deleteusermodal" uid="0" data-toggle="modal" data-target="#myModal">Delete registrar</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>User name</th>
                            <th>Event name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Role</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <form id="deleteAdminForm" action="#" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete registrar</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="btnDeleteRegistrar" data-dismiss="modal">Okay</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#deleteusermodal").hide();
            $("#btnDeleteRegistrar").click(function () {
                var uid = $('#deleteusermodal').attr('uid');
                $.ajax({
                    type: "POST",
                    url: "RegistrarServices.asmx/DeleteUser",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ uid: '" + uid + "'}",
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) { console.log(data) }
                });
            });
        });


        $('#empTable').DataTable({
            columns: [
                { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'EventName' },
               { 'data': 'ValidFrom' },
               { 'data': 'ValidTo' },
               { 'data': 'UserType' },
               { 'data': 'Status' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'RegistrarServices.asmx/LoadRegistrar',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'EventName', "bSortable": false },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'UserType', "bSortable": true },
        { "data": 'Status', "bSortable": true }
            ]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                $("#deleteusermodal").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var ss = $(this).children('td:first-child').text();
                $("#deleteusermodal").attr('uid', ss);
                $("#deleteusermodal").show();
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();
    </script>

</asp:Content>


