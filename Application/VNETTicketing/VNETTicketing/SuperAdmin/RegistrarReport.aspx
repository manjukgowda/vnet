﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="RegistrarReport.aspx.cs" Inherits="VNETTicketing.SuperAdmin.RegistrarReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Registrars reports</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>User name</th>
                            <th>Event name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <link rel="stylesheet" type="text/css" href="../Assets/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/css/buttons.dataTables.min.css">
    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="../Assets/js/jszip.min.js"></script>
    <script type="text/javascript" src="../Assets/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vfs_fonts.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.print.min.js"></script>
    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $('#empTable').DataTable({
            columns: [
              { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'EventName' },
               { 'data': 'ValidFrom' },
               { 'data': 'ValidTo' },
               { 'data': 'UserType' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            lengthMenu: [[10000], [ 10000]],
            dom: 'Bfrtip',
            sAjaxSource: 'RegistrarServices.asmx/LoadRegistrar',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'EventName', "bSortable": false },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'UserType', "bSortable": true }
            ],
            select: true,
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
            }]
        });
    </script>

</asp:Content>
