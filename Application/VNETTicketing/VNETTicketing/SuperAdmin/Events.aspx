﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="VNETTicketing.SuperAdmin.Events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Events</h1>
        </div>
    </div>
    <div id="view1" class="row">
        <form action="#" method="post">
            <div class="login-fields">
                <div class="field col-md-12">
                    <label for="username">Event title:</label>
                    <input type="text" id="eventtitle" name="eventtitle" value="" class="login username-field form-control" runat="server" />
                    <label id="titleerror"></label>
                </div>
                <div class="field col-md-12">
                    <label for="eventprefix">Event prefix:</label>
                    <!-- <input type="text" id="eventprefix" name="eventprefix" value="" class="login username-field form-control" oninput="myFunction()" /> -->
                    <input type="text" id="eventprefix" oninput="myFunction()" class="login username-field form-control" runat="server" />
                    <label id="prefixerror"></label>
                </div>
                <!-- /field -->
                <div class="field col-md-12">
                    <label for="password">Event description:</label>
                    <textarea class="login password-field form-control" id="eventdescription" runat="server"></textarea>

                    <label id="descerror"></label>
                </div>
                <div class="field col-md-12">
                    <label for="password">Venue name:</label>
                    <input type="text" id="eventvenue" name="venue" value="" class="login password-field form-control" runat="server" />
                    <label id="venueerror"></label>
                </div>
                <div class="field col-md-12">
                    <div class="well">This fee is used to charge end-users whenever they wish to transfer events</div>
                </div>
                <div class="field col-md-12">
                    <label for="password">Admin fee:</label>
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            <asp:DropDownList runat="server" CssClass="form-control" ID="drp_currency">
                            </asp:DropDownList>
                            <%--<select class="form-control">
                                <option value="1">$ USD</option>
                                <option value="2">$ SGD</option>
                                <option value="3">€ EUR</option>
                                <option value="4">$ AUD</option>
                                <option value="5">¥ JPY</option>
                                <option value="6">¥ CHN</option>
                                <option value="7">฿ THB</option>
                                <option value="8">RM MYR</option>
                            </select>--%>
                        </div>
                        
                        <div class="col-md-9 col-xs-9">
                            <input min="1" max="10000" type="number" id="ae_fee" placeholder="Fee" name="event fee" value="" class="login password-field form-control" runat="server" />
                            <label id="feeerror"></label>
                        </div>
                    </div>
                    
                </div>
                
                <div class="field col-md-12">
                    <label for="ticketid">Ticket ID:</label>
                        <div class="field col-md-12">
                                <input type="number" id="ticketsid" name="startid" value="" placeholder="From" class="login password-field form-control" runat="server" />
                                <label id="ticketerror"></label>
                            </div>
                          
                    
                    
                </div>
                <div class="field col-md-12">
                    <label for="password">Address line 1:</label>
                    <input type="text" id="eventaddress1" name="address1" value="" class="login password-field form-control" runat="server" />
                    <label id="adderror"></label>
                </div>
                <div class="field col-md-12">
                    <label for="password">Address line 2:</label>
                    <input type="text" id="eventaddress2" name="address2" value="" class="login password-field form-control" runat="server" />
                </div>
                <div class="field col-md-6">
                    <label for="password">City:</label>
                    <input type="text" id="eventcity" name="city" value="" class="login password-field form-control" runat="server" />
                    <label id="cityerror"></label>
                </div>
                <div class="field col-md-6">
                    <label for="password">Postal code:</label>
                    <input type="text" id="eventpin" name="postal" value="" class="login password-field form-control" runat="server" />
                    <label id="posterror"></label>
                </div>
                <div class="field col-md-6">
                    <label for="password">Event start date:</label>
                    <input type="text" id="s_date" name="eventstart" value="" class="login password-field form-control s_date" runat="server" />
                    <label id="starterror"></label>
                </div>
                <div class="field col-md-6">
                    <label for="password">Event end date:</label>
                    <input type="text" id="e_day" name="eventend" value="" class="login password-field form-control e_day" runat="server" />
                    <label id="enderror"></label>
                </div>
                <div class="field col-md-12">
                    <label for="password">Groups:</label>
                    <input type="text" id="txtGroups" name="group" value="" class="login password-field form-control" placeholder="Add comma for multiple group names " runat="server" />
                    <label id="grouperror"></label>
                </div>
                 <div class="field col-md-12">
                    <label for="password">Upload Event Image:</label>
                     <asp:FileUpload ID="fupEventFile"  class="login password-field form-control" runat="server"  />
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="fupEventFile" ValidationExpression="^.*\.(jpg|JPG|jpeg|JPEG|png|PNG)$" runat="server" ErrorMessage="Only image format (jpg, jpeg and png) files can be uploaded" ForeColor="Red" />
                </div>
                <!-- /password -->
                <div class="login-actions">
                    <%--<a id="btnNextView1" class="button btn btn-success btn-large pull-left" href="#">Save changes</a>--%>
                    <label for="password">&nbsp;</label>
                  <br />
                    <asp:Button Text="Save Changes" CssClass="button btn btn-success btn-large pull-left" runat="server" ID="btnCreateEvent" OnClick="btnCreateEvent_Click" />
                    <%--<asp:Button Text="Save Changes" runat="server" CssClass="button btn btn-success btn-large pull-left" ID="btnNext1" OnClientClick="return secondtab();" OnClick="btnNext1_Click" />
                                    <asp:LinkButton Text="Save Chanages" runat="server" ID="lnkbtnNext1" OnClick="lnkbtnLinkButton_Click" OnClientClick="secondtab();" />--%>
                </div>
            </div>
            <!-- /login-fields -->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Event Message</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Event name already exists, try with unique name.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!-- Responsive tabs js -->
        <script src="../Assets/js/responsive-tabs.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../Assets/dist/js/sb-admin-2.js"></script>

        <!-- Nice edit js -->
        <script src="../Assets/js/nicEdit-latest.js"></script>

        <!-- bootstrap datepicker -->
        <script type="text/javascript" src="../Assets/js/bootstrap-datepicker.js"></script>

        <!-- Image gallery js -->
        <script type="text/javascript" src="../Assets/js/jquery.blueimp-gallery.min.js"></script>
        <script type="text/javascript" src="../Assets/js/bootstrap-image-gallery.min.js"></script>
    <script src="../Assets/Validations/CreateEvents.js"></script>

        <script type="text/javascript">

            
            $(document).ready(function () {
                
                $("#ContentPlaceHolder1_ae_fee").keypress(function (event) {
                    if (event.which == 45 || event.which == 189) {
                        event.preventDefault();
                    }
                    if (event.which != 46 && event.which > 31 && (event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
                });

                //$("#ae_fee").keydown(function (event) {
                //    console.log(event.keyCode);
                //    if (event.keyCode == 45) { event.preventDefault(); }
                //});
            });


           // bkLib.onDomLoaded(nicEditors.allTextAreas);
            $(".s_date").datepicker();
            $(".e_day").datepicker();
            //$('#be_date').datepicker();
            //$('#bs_date').datepicker();


            /*get the value of event prefix*/
            /*var bla = $('#eventprefix').val();
            bla.oninput(function () {
                document.getElementByID('eventprefix').innerHTML() = bla;
            });*/
            function myFunction() {
                //var x = $("#ContentPlaceHolder1_eventprefix").val();
                //$("#demo").append(x);
                //$("#demo1").append(x);
                var x = document.getElementById("ContentPlaceHolder1_eventprefix").value;
                document.getElementById("demo").innerHTML = x;
                document.getElementById("demo1").innerHTML = x;
            }
            function openModal() {
                $('#myModal').modal('show');
            }
        </script>
</asp:Content>
