﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for AdminServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AdminServices : System.Web.Services.WebService
    {
        AdminClass obj;
        [WebMethod]
        public void LoadAdmins()
        {
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            //sortingModal.SortColumnName = iSortCol_0 == 0 ? "UId" : iSortCol_0 == 1 ? "Username" : "UserType";
            sortingModal.SortColumnName = iSortCol_0 == 0 ? "UId" : iSortCol_0 == 1 ? "Username" : iSortCol_0 == 2 ? "UserType" : "Status";
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            obj = new AdminClass();
            List<AdminClass.UserViewModel> alluser = obj.GetAllUser();
            if (sortingModal.SearchString != "")
            {
                alluser = alluser.Where(x => x.Username.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.UserType.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.UId.ToString().Contains(sortingModal.SearchString.ToLower()) || x.Status.ToLower().Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredUsers = SortingAndPagingHelper.SortingAndPaging<AdminClass.UserViewModel>(alluser, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = alluser.Count,
                iTotalRecords = filteredUsers.Count(),
                data = filteredUsers
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }

        [WebMethod]
        public string AddUser(string uid, string uname, string password, string isadmin)
        {
            obj = new AdminClass();
            if (obj.IsUserIdExist(uid))
            {
                return "UId is exist!";
            }
            if (obj.IsEmailIdExist(uname))
            {
                return "Email ID is exist!";
            }
            obj.AddUser(new AdminClass.UserViewModel { Password = password, UserTypeId = 2, IsActive = isadmin == "true" ? true : false, Username = uname, UId = uid });
           // MailSender.SendEmail(uname, "Admin account created", "Dear " + uname + ", <br/> <br/> Your admin account has been successfuly created at VNet.<br/><br/>Regards<br/>Vnet");
            return "Admin is created successfully!";
        }
        [WebMethod]
        public bool DeleteUser(string uid)
        {
            obj = new AdminClass();

            return obj.DeleteUser(uid);

        }
        [WebMethod]
        public bool ModifyUser(string uid, string uname,string password, bool isadmin)
        {
            obj = new AdminClass();
            return obj.ModifyUser(uid, uname,password);

        }
        [WebMethod]
        public AdminClass.UserViewModel GetUser(string uid)
        {
            obj = new AdminClass();

            return obj.GetUser(uid);

        }
    }
}
