﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.ContentPages
{
    public partial class SADashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DashboardHelper obj = new DashboardHelper();
                lblAdmins.Text = obj.GetAdminsCount().ToString();
                lblRegistrars.Text = obj.GetRegistrarsCount().ToString();
                lblEvents.Text = obj.GetEventsCount().ToString();
                Page.Title = "Dashboard || The V";

                lblAmount.Text = obj.GetTotalCollections().ToString();
                GridView1.DataSource = obj.GetEventsCollections();
                GridView1.DataBind();
            }
        }
    }
}   