﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="EventFeeAndPromoCodes.aspx.cs" Inherits="VNETTicketing.SuperAdmin.EventFeeAndPromoCodes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Generate multiple prices and promo codes for events</h1>
        </div>
    </div>
    <div id="view4" class="row">

        <form action="#" method="post">
            <%--<div class="well">This fee is used to charge end-users whenever they wish to transfer events</div>--%>
            <div class="login-fields">
                <label for="event" class="lead">Select the event</label>
                <div class="row">
                    <div class="col-md-3 col-xs-3">
                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                        </asp:DropDownList>
                        <label id="ddleventerror"></label>
                    </div>
                </div>
            </div>
            <%--<div class="login-fields">
                <div class="field">
                    <label for="password">Admin fee:</label>
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            <asp:DropDownList class="form-control" runat="server" ID="ddl_currency1">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-9 col-xs-9">
                            <input type="number" id="a_fee" name="admin fee" value="" class="login password-field form-control" />
                        </div>
                    </div>
                </div>
            </div>--%>
        </form>
        <div class="lead">Multiple prices</div>
        <div class="col-md-3">
            <input type="text" id="type" name="type" value="" class="login password-field form-control" placeholder="Type of ticket" runat="server" />
            <label id="tickettypeerror"></label>
        </div>
        <div class="col-md-2">
            <asp:DropDownList runat="server" class="form-control" ID="ddl_currency2">
            </asp:DropDownList>
        </div>
        <div class="col-md-4">
            <input type="number" id="ticketprice" name="price" value="" class="login password-field form-control" placeholder="Price of ticket" runat="server" />
            <label id="priceticketerror"></label>
        </div>
        <div class="col-md-3">
            <asp:Button Text="Confirm" class="btn btn-primary" ID="confirm" runat="server" OnClick="confirm_Click" />
            <%--<button type="button" class="btn btn-primary" id="confirm">Confirm</button>--%>
        </div>
        <asp:GridView ID="grdviewtickettype" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
            <Columns>
                <asp:BoundField DataField="TicketType" HeaderText="Type of ticket" />
                <%--<asp:BoundField DataField="TicketTypePrice" HeaderText="Price of ticket" />--%>
                <asp:TemplateField HeaderText="Price of ticket">
                    <ItemTemplate>
                        <asp:Label ID="lbltypeprice" runat="server" Text='<%#Eval("CurrencyName")+ " " + Eval("TicketTypePrice")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remove">
                    <ItemTemplate>
                        <%--<asp:LinkButton ID="lnkDelete" OnClick ="lnkDelete_Click" CommandArgument='<%#Eval("PriceId")%>' runat="server">Remove</asp:LinkButton>--%>
                        <asp:CheckBox Text=" Is Active" ID="chkDelete" OnCheckedChanged="chkDelete_CheckedChanged" AutoPostBack="true" Checked='<%#Eval("TicketTypeStatus") %>' CommandArgument='<%#Eval("PriceId")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate><span>No records found</span></EmptyDataTemplate>
        </asp:GridView>
        <%--<table class="table table-bordered" style="margin-top: 7%;">
            <thead>
                <tr>
                    <th>Type of ticket</th>
                    <th>Price of ticket</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>--%>
        <div class="col-md-2">
            <br />
            <div class="lead">Promo code</div>
        </div>
        <div class="col-md-10">
            <a class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="newpromo">New</a>
        </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
            <Columns>
                <asp:BoundField DataField="PromoName" HeaderText="Promo code" />
                <asp:TemplateField HeaderText="Price">
                    <ItemTemplate>
                        <asp:Label ID="lblpromo" runat="server" Text='<%#Eval("PromoCurrencyName")+ " " + Eval("PromoPrice")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 
            </Columns>
            <EmptyDataTemplate><span>No records found</span></EmptyDataTemplate>
        </asp:GridView>
        <%--<table class="table table-bordered">
            <thead>
                <tr>
                    <th>Promo code</th>
                    <th>Price</th>
                    <th>Total allocation</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>INFSG7089</td>
                    <td>USD 450</td>
                    <td>1000</td>
                </tr>
                <tr>
                    <td>GENSI1517</td>
                    <td>USD 400</td>
                    <td>500</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>--%>

        <br />
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Promo code details</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="post">
                            <div class="login-fields">
                                <div class="field">
                                    <label for="promocode">Promo code:</label>
                                    <input type="text" id="promotitle" name="promotitle" value="" class="login username-field form-control" />
                                    <label id="promocodeerror"></label>
                                </div>
                                <!-- /field -->
                                <div class="field">
                                    <label for="password">Price:</label>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-3">
                                            <asp:DropDownList class="form-control" runat="server" ID="ddl_currency3">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-9 col-xs-9">
                                            <input type="number" id="ad_fee" name="admin fee" value="" class="login password-field form-control" />
                                            <label id="promopriceerror"></label>
                                        </div>
                                    </div>
                                </div>
                                                         <div class="field">
                                    <label for="promocode">Maximum Allocation:</label>
                                    <input type="text" id="maxlimit" name="promotitle" value="" class="login username-field form-control" />
                                    <label id="maxlimiterror"></label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addToTable">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="closePromo">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <%--<div class="login-actions">
            <a class="button btn btn-success btn-large pull-left" href="#" id="btnSubmit">Submit</a>
        </div>--%>
    </div>
    <script src="../Assets/Validations/eventfeepromocode.js"></script>
    <script>


        /*appending multiple prices into table*/
        //$("#confirm").click(function (e) {
        //    var type = $('#type').val();
        //    var currency = $("#ContentPlaceHolder1_ddl_currency2 option:selected").text();
        //    var mprice = $('#ticketprice').val();

        //    $('table tbody').append('<tr><td>' + type + '</td><td>' + currency + ' ' + mprice + '</td></tr>');

        //    /*Clear the values in text boxes after adding to table*/
        //    document.getElementById("type").value = "";
        //    document.getElementById("ticketprice").value = "";
        //})
        $('#addToTable').click(function () {
            var prom = $('#promotitle').val();
            var promcurrval = $("#ContentPlaceHolder1_ddl_currency3 option:selected").val();
            var promcurrtext = $("#ContentPlaceHolder1_ddl_currency3 option:selected").text();
            //cur = $('select').val(),
            var price = $('#ad_fee').val();
            var allocation = $('#maxlimit').val();//999999;
            var eventid = $("#ContentPlaceHolder1_ddlevents option:selected").val();
            var minamount = "1";
            if (prom != '' && price != '' && allocation != '' && minamount != '') {
                $.ajax({
                    type: "POST",
                    url: "EventServices.asmx/AddPromoCode",
                    datatype: "json",
                    contentType: 'application/json; charset=utf-8',
                    data: "{promocode:'" + prom + "',promocurrid:'" + promcurrval + "',promoval:'" + price + "',promoallocation:'" + allocation + "',promoeventid:'" + eventid + "', minimumamount:'" + minamount + "'}",
                    success: function (data) {
                        console.log(data);
                        $('#ContentPlaceHolder1_GridView1 tbody').append('<tr><td>' + prom + '</td><td>' + promcurrtext + ' ' + price + '</td></tr>');
                    },
                    error: function (data) { console.log(data) }
                });
            }
        });

    </script>
</asp:Content>
