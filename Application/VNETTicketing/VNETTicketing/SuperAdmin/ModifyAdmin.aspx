﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="ModifyAdmin.aspx.cs" Inherits="VNETTicketing.SuperAdmin.ModifyAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Modify admin</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <%--<a class="btn btn-primary" id="modifybtn" uid="0" data-toggle="modal" data-target="#myModal">Modify admin</a>--%>
        <a class="btn btn-primary" id="modifybtn" uid="0" data-toggle="modal">Modify admin</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Email ID</th>
                            <th>Role</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modify admin</h4>
                        </div>
                        <div class="modal-body">
                            <form id="addUserForm" action="#" method="post">
                                <div class="login-fields">
                                    <div class="field">
                                        <label for="username">User ID:</label>
                                        <input type="text" id="uid" name="uid" value="" class="login username-field form-control" />
                                    </div>
                                    <div class="field">
                                        <label for="username">Email ID:</label>
                                        <input type="text" id="Uname" name="Uname" value="" class="login username-field form-control" />
                                        <label id="unameerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Custom password:</label>
                                        <input type="password" id="password" name="password" value="" class="login username-field form-control" />
                                        <label id="cpwderror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="password">Re-enter custom password</label>
                                        <input type="password" id="recpassword" name="recustompassword" value="" class="login username-field form-control" />
                                        <label id="rpwderror"></label>
                                    </div>
                                    <div class="field" style="margin-top: 2%;">
                                        <label for="password">Make admin:</label>
                                        <input type="checkbox" id="adminchk" name="my-checkbox" data-on-text="Active" data-off-text="Inactive" checked>
                                    </div>
                                </div>
                                <!-- /login-fields -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnModifyUser" class="btn btn-danger" data-dismiss="modal">Okay</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnclose" >Close</button>
                        </div>
                        <div><span id="msg"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#modifybtn').click(function () {
                var uid = $('#modifybtn').attr('uid');
                $("#uid").prop('disabled', false);

                $.ajax({
                    type: "POST",
                    url: "AdminServices.asmx/GetUser",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ uid: '" + uid + "'}",
                    success: function (data) {
                        $("#Uname").val(data.d.Username);
                        $("#uid").val(data.d.UId);
                        $("#uid").prop('disabled', true);
                    },
                    error: function (data) { console.log(data) }
                });
                $('#myModal').modal('show')
            });
            $("#modifybtn").hide();
            $("#btnModifyUser").click(function () {

                var flag = true;
                if ($("#Uname").val() == '' || $("#Uname").val() == null) {
                    $('#Uname').css('border-color', 'red');
                    $('#unameerror').fadeIn(1000);
                    $('#unameerror').html('User id can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Uname').css('border-color', ''); }
                if ($("#password").val() == '' || $("#password").val() == null) {
                    $('#password').css('border-color', 'red');
                    $('#cpwderror').fadeIn(1000);
                    $('#cpwderror').html('Custom password can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#cpwderror').css('border-color', ''); }


                if ($("#recpassword").val() == '' || $("#recpassword").val() == null) {
                    $('#recpassword').css('border-color', 'red');
                    $('#rpwderror').fadeIn(1000);
                    $('#rpwderror').html('Re enter password can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else {
                    if ($("#recpassword").val() != $("#password").val()) {
                        flag = false;
                        $('#recpassword').css('border-color', 'red');
                        $('#rpwderror').fadeIn(1000);
                        $('#rpwderror').html('Password and Confirm Password are not matching!').css('color', 'red').fadeOut(10000);
                    }
                    else { $('#recpassword').css('border-color', ''); }
                }

                if (!flag) {
                    return flag;
                }
                else {
                    var uid = $('#modifybtn').attr('uid');
                    $.ajax({
                        type: "POST",
                        url: "AdminServices.asmx/ModifyUser",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ uid: '" + uid + "', uname:'" + $("#Uname").val() + "', password:'" + $("#password").val() + "', isadmin:'" + $('#adminchk').is(':checked') + "'}",
                        success: function (data) {
                            if (data.d == 'User Name is exist!') {
                                $('#msg').val(data.d);

                                $('#myModal').show();
                            }
                            else {
                                $("#uid").val('');
                                $("#Uname").val('');
                                $('#adminchk').prop('checked', true);
                                console.log(data);
                                location.reload();
                                //$('#empTable').DataTable().ajax.reload();
                                //$('#empTable').dataTable().fnDestroy();
                                //$('#empTable').dataTable();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                }
            });
            $("#btnclose").click(function () {
                $('#Uname').css('border-color', '');
            });

        });


        $('#empTable').DataTable({
            columns: [
               { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'UserType' },
               { 'data': 'Status' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'AdminServices.asmx/LoadAdmins',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UserId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'UserType', "bSortable": true },
        { "data": 'Status', "bSortable": true }
            ]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                $("#modifybtn").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var ss = $(this).children('td:first-child').text();
                $("#modifybtn").attr('uid', ss);
                $("#modifybtn").show();
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();
    </script>

</asp:Content>

