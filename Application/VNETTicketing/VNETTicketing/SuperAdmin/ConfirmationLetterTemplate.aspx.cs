﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    public partial class ConfirmationLetterTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindEvents();
            }
        }

        private void BindEvents()
        {
            EventsClass obj = new EventsClass();
            ddlevents.DataSource = obj.GetAllEvents();
            ddlevents.DataTextField = "EventName";
            ddlevents.DataValueField = "EventId";
            ddlevents.DataBind();
            ddlevents.Items.Insert(0, new ListItem { Text = "Select Event", Value = "0", Selected = true });
        }

        protected void ddlevents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = int.Parse(ddlevents.SelectedValue);
            EventsClass obj = new EventsClass();
            txtTemplate.Text = obj.GetEventConfirmationTemplate(eventid);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int eventid = int.Parse(ddlevents.SelectedValue);
            EventsClass obj = new EventsClass();
            obj.AddEventConfirmationTemplate(eventid,txtTemplate.Text);
            ClientScript.RegisterStartupScript(this.GetType(), "ket", "alert('Saved successfully')", true);
        }
    }
}