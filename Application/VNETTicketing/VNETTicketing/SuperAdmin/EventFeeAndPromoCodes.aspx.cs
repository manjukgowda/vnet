﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    public partial class EventFeeAndPromoCodes : System.Web.UI.Page
    {
        EventsClass obj;
        CurrencyClass objcur;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Title = "Event Prices || The V";
                InitializeEventPage();
            }
            if (Request.QueryString["id"] != null)
            {
                ddlevents.SelectedValue = Request.QueryString["id"];
                //Select the event from dropdown based on eventid
            }
        }

        private void InitializeEventPage()
        {
            BindEvents();
            BindCurrencyMode();
            BindTicketType();
         
        }

        private void BindCurrencyMode()
        {
            objcur = new CurrencyClass();
            var cur = objcur.GetAllCurrency();
            //ddl_currency1.DataSource = cur;
            //ddl_currency1.DataTextField = "CurrencyName";
            //ddl_currency1.DataValueField = "CurrencyModeId";
            //ddl_currency1.DataBind();

            ddl_currency2.DataSource = cur;
            ddl_currency2.DataTextField = "CurrencyName";
            ddl_currency2.DataValueField = "CurrencyModeId";
            ddl_currency2.SelectedIndex = 1;
            ddl_currency2.DataBind();

            ddl_currency3.DataSource = cur;
            ddl_currency3.DataTextField = "CurrencyName";
            ddl_currency3.DataValueField = "CurrencyModeId";
            ddl_currency3.DataBind();
        }

        private void BindEvents()
        {
            obj = new EventsClass();
            ddlevents.DataSource = obj.GetAllEvents();
            ddlevents.DataTextField = "EventName";
            ddlevents.DataValueField = "EventId";
            ddlevents.DataBind();
            ddlevents.Items.Insert(0,new ListItem { Text = "Select Event", Value = "0", Selected = true });
        }

        protected void ddlevents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = int.Parse(ddlevents.SelectedValue);
            obj =new EventsClass();
            GridView1.DataSource =  obj.LoadEventPromo(eventid);
            GridView1.DataBind();
            grdviewtickettype.DataSource = obj.LoadEventTicketTypeSuperAdmin(eventid);
            grdviewtickettype.DataBind();
        }

        protected void confirm_Click(object sender, EventArgs e)
        {
            if (ddlevents.SelectedValue != null && ddlevents.SelectedValue != "")
            {
                obj = new EventsClass();
                obj.AddEventTicketType(new EventsClass.EventViewModel { EventId = int.Parse(ddlevents.SelectedValue), TicketType = type.Value, TicketTypePrice = ticketprice.Value, CurrencyMode = int.Parse(ddl_currency2.SelectedValue) });
                BindTicketType();
            }
        }

        private void BindTicketType()
        {
            obj = new EventsClass();
            grdviewtickettype.DataSource = obj.LoadEventTicketTypeSuperAdmin(int.Parse(ddlevents.SelectedValue));
            grdviewtickettype.DataBind();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            int priceID = Convert.ToInt32(btn.CommandArgument);
            obj = new EventsClass();
            obj.RemoveTicketPrice(priceID);
            BindTicketType();
        }

        protected void chkDelete_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = sender as CheckBox;
            int priceID = Convert.ToInt32(chk.Attributes["CommandArgument"].ToString());
            obj = new EventsClass();
            if (chk.Checked)
                obj.EnableTicketPrice(priceID);
            else
                obj.RemoveTicketPrice(priceID);
            BindTicketType();
        }
    }
}