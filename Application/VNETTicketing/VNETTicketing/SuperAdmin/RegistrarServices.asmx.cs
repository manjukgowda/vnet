﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.Registrar;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for RegistrarServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RegistrarServices : System.Web.Services.WebService
    {
        RegistrarClass obj;
        [WebMethod]
        public void LoadRegistrar()
        {
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            sortingModal.SortColumnName = iSortCol_0 == 0 ? "UId" : iSortCol_0 == 1 ? "Username" : iSortCol_0 == 2 ? "EventName" : iSortCol_0 == 3 ? "From" : iSortCol_0 == 4 ? "To" : "Status";
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            obj = new RegistrarClass();
            List<RegistrarClass.UserViewModel> alluser = obj.GetAllUser();
            if (sortingModal.SearchString != "")
            {
                alluser = alluser.Where(x => x.Username.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.EventName.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.Status.ToLower().Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredUsers = SortingAndPagingHelper.SortingAndPaging<RegistrarClass.UserViewModel>(alluser, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = alluser.Count,
                iTotalRecords = filteredUsers.Count(),
                data = filteredUsers
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }

        [WebMethod]
        public string AddUser(string uname, string password, string isregistrar, string eventid, string from, string to)
        {
            obj = new RegistrarClass();
            if (obj.IsUserNameExist(uname))
            {
                return "User Name is exist!";
            }
            var usr = new RegistrarClass.UserViewModel();
            usr.Password = password;
            usr.UserTypeId = (int)(UserTypeEnum.Registrar);
            usr.IsActive = isregistrar == "true" ? true : false;
            usr.Username = uname;
            usr.EventId = Convert.ToInt32(eventid);
            usr.From = Convert.ToDateTime(from);
            usr.To = Convert.ToDateTime(to);
            obj.AddUser(usr);
            MailSender.SendEmail(uname, "Registrar account created", "Dear " + uname + ", <br/> <br/> Your registrar account has been successfuly created at VNet.<br/><br/>Regards<br/>Vnet");

            return "Registrar is created successfully!";
        }
        [WebMethod]
        public bool DeleteUser(string uid)
        {
            obj = new RegistrarClass();
            return obj.DeleteUser(Convert.ToInt32(uid));

        }
        [WebMethod]
        public bool ModifyUser(string uid, string uname, bool isregistrar, string password,string eventid, string from, string to)
        {
            obj = new RegistrarClass();
            return obj.ModifyUser(Convert.ToInt32(uid), uname, password,Convert.ToInt32(eventid), Convert.ToDateTime(from), Convert.ToDateTime(to), isregistrar);
        }
        [WebMethod]
        public RegistrarClass.UserViewModel GetUser(string uid)
        {
            obj = new RegistrarClass();
            return obj.GetUser(Convert.ToInt32(uid));
        }
    }
}
