﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="EventsReport.aspx.cs" Inherits="VNETTicketing.SuperAdmin.EventsReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Event report</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="row">
            <div class="container">
                <table id="eventTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Event ID</th>
                            <th>Event Name</th>
                            <th>Venue Details</th>
                            <th>Address 1</th>
                            <th>Address 2</th>
                            <th>City</th>
                            <th>Event Fee</th>
                            <th>Postal Code</th>
                            <th>Event Start Date</th>
                            <th>Event End Date</th>
                            <%--<th>Booking Start Date</th>
                            <th>Booking End Date</th>--%>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <link rel="stylesheet" type="text/css" href="../Assets/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/css/buttons.dataTables.min.css">
    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.select.min.js"></script>
      <script type="text/javascript" src="../Assets/js/jszip.min.js"></script>
    <script type="text/javascript" src="../Assets/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vfs_fonts.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.print.min.js"></script>
  
    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $('#eventTable').DataTable({
            columns: [
                { 'data': 'EventId' },
               { 'data': 'EventName' },
               { 'data': 'Venue' },
               { 'data': 'Address1' },
               { 'data': 'Address2' },
               { 'data': 'City' },
               { 'data': 'AdminCharge' },
               { 'data': 'PostalCode' },
               { 'data': 'EventStartDate' },
               { 'data': 'EventEndDate' }
               //,
               //{ 'data': 'BookingStartDate' },
               //{ 'data': 'BookingEndDate' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            lengthMenu: [[10000], [10000]],
            dom: 'Bfrtip',
            sAjaxSource: 'EventServices.asmx/LoadEvents',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'EventId', "bSortable": true },
        { "data": 'EventName', "bSortable": true },
        { "data": 'Venue', "bSortable": true },
        { "data": 'Address1', "bSortable": true },
        { "data": 'Address2', "bSortable": true },
        { "data": 'City', "bSortable": true },
        { "data": 'AdminCharge', "bSortable": true },
        { "data": 'PostalCode', "bSortable": true },
        { "data": 'EventStartDate', "bSortable": true },
        { "data": 'EventEndDate', "bSortable": true }
        //,
        //{ 'data': 'BookingStartDate', "bSortable": true },
        //       { 'data': 'BookingEndDate', "bSortable": true }
            ],
            select: true,
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
            }]
            });
    </script>

</asp:Content>


