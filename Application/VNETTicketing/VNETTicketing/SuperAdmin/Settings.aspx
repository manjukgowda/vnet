﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="VNETTicketing.SuperAdmin.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div id="demopage">
            <div class="container1">
                <ul class="rtabs">
                    <li><a href="#view1">Number of tickets for an event</a></li>
                    <li><a href="#view2">Base currency</a></li>
                </ul>
                <div class="panel-container">
                    <div id="view1">
                        <%--<asp:Table ID="empTable" runat="server" CssClass="display table-bordered" width="100%" cellspacing="0">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell Text="Event name"></asp:TableHeaderCell>
                                <asp:TableHeaderCell Text ="Number of tickets"></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>--%>
                        <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Event Id</th>
                                    <th>Event name</th>
                                    <th>Number of tickets</th>
                                    <th></th>
                                </tr>
                            </thead>
             
                            <tfoot>
                                <tr>
                                    <th>Event Id</th>
                                    <th>Event name</th>
                                    <th>Number of tickets</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                     <%--   <div class="login-actions">
                            <button class="button btn btn-primary btn-large center-block" href="index.html" id="btnEventSubmit">Submit</button>
                        </div>--%>
                    </div>
                    <div id="view2">
                        <table id="curTable" class="display table table-bordered" width="100%" cellspacing="0" runat="server">
                            <tr><td colspan="3">
                                <asp:Label ID="lblBaseCurrency" runat="server" Text="Label"></asp:Label>
                        <asp:Repeater ID="Repeater1"  runat="server">
                            <ItemTemplate>

                                <tbody>
                                <tr>
                                     <td><%#Eval("CurrencyModeId") %></td>
                                    <td><%#Eval("CurrencyName") %></td>
                                    <td><i></i><%#Eval("CurrenctRate") %></td>
                                    <td>
                                        <a class="btn btn-primary" curId="0" data-toggle="modal" data-target="#myModal">Change</a>
                                    </td>
                                </tr>
                                    </tbody>
                            </ItemTemplate>
                        </asp:Repeater>
                           </td></tr> </table>
                                        </div>
                   
                </div>
                <br />
            </div>
        </div>
    </div>
        
    
    <!-- Modal -->
    <div id="myTicketModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Number of ticket</h4>
                </div>
                <div class="modal-body">
                    <div class="field">
                        <label>Event Id</label>
                        <input type="text" class="login username-field form-control" name="name" value="" id="txtEventId" disabled/>
                    </div>
                </div>
                                <div class="modal-body">
                    <div class="field">
                        <label>Event Name</label>
                        <input type="text" class="login username-field form-control" name="name" value="" id="txtEventName" disabled/>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="field">
                        <label for="inr">Number of Ticket:</label>
                        <input type="number" name="inr" id="noticket" class="form-control">
                        <label id="noticketerror"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnupdateticket">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Base currency</h4>
                </div>
                <div class="modal-body">
                    <div class="field">
                        <label for="inr">New value:</label>
                        <input type="number" id="inr" name="inr" class="form-control">
                        <label id="newvalerror"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnUpdateRates" type="button" class="btn btn-primary" data-dismiss="modal">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"id="btnClose" >Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>
    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>
    <!-- Responsive tabs js -->
    <script src="../Assets/js/responsive-tabs.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>
    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>
    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <script src="../Assets/js/nicEdit-latest.js"></script>
    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(nicEditors.allTextAreas);
        //$('#empTable').dataTable();
        $('#empTable').DataTable({
            columns: [
                { 'data': 'EventId' },
               { 'data': 'EventName' },
               { 'data': 'TotalTickets' },
            { "defaultContent": "<a data-target='#myTicketModal' data-toggle='modal' class='btn btn-primary'>Update Ticket</a>" }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'EventServices.asmx/LoadEvents',
            sServerMethod: 'Post',
            aoColumnDefs: [
                { "data": 'EventId', "bSortable": true },
        { "data": 'EventName', "bSortable": true },
        { "data": 'TotalTickets', "bSortable": true }
        //{ "sType": "numeric", "bSortable": true }
            ]
        });


        //$.fn.dataTableExt.afnSortData['dom-text'] = function (oSettings, iColumn) {
        //    var aData = [];
        //    $('td:eq(' + iColumn + ') input', oSettings.oApi._fnGetTrNodes(oSettings)).each(function () {
        //        aData.push(this.value);
        //    });
        //    return aData;
        //}

        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                //$(this).children('td:last-child input').remove().text('p');//'<input type="number" name="tickets" class="form-control">').text('hh');
                //$("#modifybtn").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var eid = $(this).children('td').eq(0).text();
                var ename = $(this).children('td').eq(1).text();
                var tick = $(this).children('td').eq(2).text();

                //var data = table.row($(this).parents('tr')).data();
                $("#txtEventName").val(ename);
                $("#txtEventId").val(eid);
                $("#noticket").val(tick);
            }
        });

        $('#ContentPlaceHolder1_curTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var curId = $(this).children('td').eq(0).text();
                $("#btnUpdateRates").attr('curId', curId);
                var rate = $(this).children('td').eq(2).text();
                $('#inr').val(rate);
            }
        });


        $("#btnupdateticket").click(function () {

            var flag = true;

            if ($("#noticket").val() == '' || $("#noticket").val() == null) {
                $('#noticket').css('border-color', 'red');
                $('#noticketerror').fadeIn(1000);
                $('#noticketerror').html('Start date can not be empty!').css('color', 'red').fadeOut(10000);
                flag = false;
            }
            else { $('#noticket').css('border-color', ''); }

            if (!flag) {
                return flag;
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "SettingsServices.asmx/UpdateTicketNumber",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ eventid: '" + $("#txtEventId").val() + "',notickets:'" + $("#noticket").val() + "'}",
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (data) { console.log(data) }
                });
            }
        });
        $("#btnClose").click(function () {
            $('#noticket').css('border-color', '');
        });
        
        $("#btnUpdateRates").click(function () {

            var flag = true;

            if ($("#inr").val() == '' || $("#inr").val() == null) {
                $('#inr').css('border-color', 'red');
                $('#newvalerror').fadeIn(1000);
                $('#newvalerror').html('Start date can not be empty!').css('color', 'red').fadeOut(10000);
                flag = false;
            }
            else { $('#inr').css('border-color', ''); }

            if (!flag) {
                return flag;
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "SettingsServices.asmx/UpdateCurrencyRate",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ curId: '" + $(this).attr('curId') + "',rate:'" + $("#inr").val() + "'}",
                    success: function (data) {
                        window.location.reload();
                    },
                    error: function (data) { console.log(data) }
                });
            }
        });
    </script>
</asp:Content>
