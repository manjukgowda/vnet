﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VNETTicketing.SuperAdmin
{
    public partial class admindata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = File.ReadAllText(HttpContext.Current.Server.MapPath("../Assets/json/admindata.json"));
            Response.Write(json);
        }
    }
}