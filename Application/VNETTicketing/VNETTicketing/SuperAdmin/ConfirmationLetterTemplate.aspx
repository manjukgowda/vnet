﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="ConfirmationLetterTemplate.aspx.cs" Inherits="VNETTicketing.SuperAdmin.ConfirmationLetterTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Confirmation Letter Templates</h1>
        </div>
    </div>

    <%--<div class="well">This fee is used to charge end-users whenever they wish to transfer events</div>--%>
    <div class="login-fields">
        <label for="event" class="lead">Select the event</label>
        <div class="row">
            <div class="col-md-3 col-xs-3">
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ControlToValidate="ddlevents" ID="RequiredFieldValidator1" InitialValue="Select Event" runat="server" ErrorMessage="Select an Event"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="login-fields">
        <label for="event" class="lead">Template</label>
        <div class="row">
            <div class="col-md-3 col-xs-3">
                <asp:TextBox ID="txtTemplate"  TextMode="MultiLine" runat="server" Height="520px" Width="900px"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTemplate" runat="server" ErrorMessage="Template Required"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="login-fields">
         <asp:Button Text="Save" class="btn btn-primary" ID="btnSave" runat="server" OnClick="btnSave_Click" />
        </div>
</asp:Content>
