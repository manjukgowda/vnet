﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="ModifyEvent.aspx.cs" Inherits="VNETTicketing.SuperAdmin.ModifyEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Modify Events</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="modifybtn" eid="0" data-toggle="modal">Modify Events</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Event ID</th>
                            <th>Event Name</th>
                            <th>Venue Details</th>
                            <th>Address 1</th>
                            <th>Address 2</th>
                            <th>City</th>
                            <th>Transfer Fee</th>
                            <th>Postal Code</th>
                            <th>Event Start Date</th>
                            <th>Event End Date</th>
                            <%--<th>Booking Start Date</th>
                            <th>Booking End Date</th>--%>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modify  Event</h4>
                        </div>
                        <div class="modal-body">
                            <form id="addUserForm" action="#" method="post">
                                <div class="login-fields">
                                    <%--<div class="field">
                                        <label for="username">Event ID:</label>
                                        <input type="text" id="eid" name="eid" value="" class="login username-field form-control" />
                                    </div>--%>
                                    <div class="field">
                                        <label for="username">Event title:</label>
                                        <input type="text" id="Ename" name="Ename" value="" class="login username-field form-control" />
                                        <label id="eventtitleerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Venue name:</label>
                                        <input type="text" id="Vdetails" name="Vdetails" value="" class="login username-field form-control" />
                                        <label id="venueerror"></label>
                                    </div>

                                    <div class="field" style="margin-top: 2%;">
                                        <label for="password">Address line 1:</label>
                                        <input type="text" id="Vaddress1" name="Vaddress1" value="" class="login username-field form-control" />
                                        <label id="add1error"></label>
                                    </div>
                                    <div class="field" style="margin-top: 2%;">
                                        <label for="password">Address line 2:</label>
                                        <input type="text" id="Vaddress2" name="Vaddress2" value="" class="login username-field form-control" />
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="username">City:</label>
                                        <input type="text" id="Vcity" name="Vcity" value="" class="login username-field form-control" />
                                        <label id="cityerror"></label>
                                    </div>

                                    <div class="field col-md-6">
                                        <label for="username">Postal Code:</label>
                                        <input type="text" id="Epostal" name="Epostal" value="" class="login username-field form-control" />
                                        <label id="postalerror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="username">Event Start Date:</label>
                                        <input type="text" id="Estart" name="Estart" value="" class="login username-field form-control" />
                                        <label id="sdateerror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="username">Event End Date:</label>
                                        <input type="text" id="Eend" name="Eend" value="" class="login username-field form-control" />
                                        <label id="edateerror"></label>
                                    </div>
                                    <%--<div class="field">
                                        <label for="username">Booking Start Date:</label>
                                        <input type="text" id="Bstart" name="Bstart" value="" class="login username-field form-control" />
                                    </div>
                                    <div class="field">
                                        <label for="username">Booking End Date:</label>
                                        <input type="text" id="Bend" name="Bend" value="" class="login username-field form-control" />
                                    </div>--%>
                                </div>
                                <!-- /login-fields -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnModifyUser" class="btn btn-danger" data-dismiss="modal">Okay</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnmodalclose">Close</button>
                        </div>
                        <div><span id="msg"></span></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../Assets/js/bootstrap-datepicker.js"></script>
    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $("#Estart").datepicker();
            $("#Eend").datepicker();

            $('#modifybtn').click(function () {
                var eid = $('#modifybtn').attr('eid');
                $("#eid").prop('disabled', false);

                $.ajax({
                    type: "POST",
                    url: "EventServices.asmx/GetEvent",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ eid: '" + eid + "'}",
                    success: function (data) {
                        $("#eid").val(data.d.EventId);
                        $("#Ename").val(data.d.EventName);
                        $("#Vdetails").val(data.d.Venuename);
                        $("#Vaddress1").val(data.d.Address1);
                        $("#Vaddress2").val(data.d.Address2);
                        $("#Vcity").val(data.d.City);
                        $("#Epostal").val(data.d.PostalCode);
                        $("#Estart").val(data.d.strEventStartDate);
                        $("#Eend").val(data.d.strEventEndDate);
                        //$("#Bstart").val(data.d.BookingStartDate.toString());
                        //$("#Bend").val(data.d.BookingStartDate.toString());
                    },
                    error: function (data) { console.log(data) }
                });
                $('#myModal').modal('show')
            });
            $("#modifybtn").hide();
            $("#btnModifyUser").click(function () {
                var flag = true;
                if ($("#Ename").val() == '' || $("#Ename").val() == null) {
                    $('#Ename').css('border-color', 'red');
                    $('#eventtitleerror').fadeIn(1000);
                    $('#eventtitleerror').html('Event title can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Ename').css('border-color', ''); }
                if ($("#Vdetails").val() == '' || $("#Vdetails").val() == null) {
                    $('#Vdetails').css('border-color', 'red');
                    $('#venueerror').fadeIn(1000);
                    $('#venueerror').html('Venue name can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Vdetails').css('border-color', ''); }
                if ($("#Vaddress1").val() == '' || $("#Vaddress1").val() == null) {
                    $('#Vaddress1').css('border-color', 'red');
                    $('#add1error').fadeIn(1000);
                    $('#add1error').html('Address 1 can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Vaddress1').css('border-color', ''); }
                if ($("#Vcity").val() == '' || $("#Vcity").val() == null) {
                    $('#Vcity').css('border-color', 'red');
                    $('#cityerror').fadeIn(1000);
                    $('#cityerror').html('City can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Vcity').css('border-color', ''); }
                if ($("#Epostal").val() == '' || $("#Epostal").val() == null) {
                    $('#Epostal').css('border-color', 'red');
                    $('#postalerror').fadeIn(1000);
                    $('#postalerror').html('Postal can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Epostal').css('border-color', ''); }
                if ($("#Estart").val() == '' || $("#Estart").val() == null) {
                    $('#Estart').css('border-color', 'red');
                    $('#sdateerror').fadeIn(1000);
                    $('#sdateerror').html('Event start date can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Estart').css('border-color', ''); }
                if ($("#Eend").val() == '' || $("#Estart").val() == null) {
                    $('#Eend').css('border-color', 'red');
                    $('#edateerror').fadeIn(1000);
                    $('#edateerror').html('Event end datecan not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Eend').css('border-color', ''); }

                if (!flag)
                    return flag;

                if ($("#Ename").val() != '' && $("#Vdetails").val() != '' && $("#Efee").val() != '' && $("#Vaddress1").val() != '' && $("#Vcity").val() != '' && $("#Epostal").val() != '' && $("#Estart").val() != null && $("#Eend").val() != '') {
                    var eid = $('#modifybtn').attr('eid');
                    $.ajax({
                        type: "POST",
                        url: "EventServices.asmx/ModifyEvent",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ eid: '" + eid + "',eventname: '" + $("#Ename").val()  + "', eventvenue: '" + $('#Vdetails').val() + "',eventsdate:'" +
                                $("#Estart").val() + "',eventedate:'" + $("#Eend").val() + "', eventaddress1:'" + $("#Vaddress1").val() + "',eventaddress2:'" + $("#Vaddress2").val() + "', eventcity:'" + $("#Vcity").val() + "',eventpostal:'" + $("#Epostal").val() + "'}",
                        success: function (data) {
                            if (data.d == 'Event Name is exist!') {
                                $('#msg').val(data.d);

                                $('#myModal').show();
                            }
                            else {
                                $("#eid").val('');
                                $("#Ename").val('');
                                $("#Vdetails").val('');
                                $("#Vaddress1").val('');
                                $("#Vaddress2").val('');
                                $("#Vcity").val('');
                                $("#Efee").val('');
                                $("#Epostal").val('');
                                $("#Estart").val('');
                                $("#Eend").val('');
                                console.log(data);
                                location.reload();
                                //$('#empTable').DataTable().ajax.reload();
                                //$('#empTable').dataTable().fnDestroy();
                                //$('#empTable').dataTable();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                }
            });

            $("#btnmodalclose").click(function () {
                $("#Ename").val('').css('border-color', '');
                $("#Vdetails").val('').css('border-color', '');
                $("#Efee").val('').css('border-color', '');
                $("#Vaddress1").val('').css('border-color', '');
                $("#Vcity").val('').css('border-color', '');
                $("#Epostal").val('').css('border-color', '');
                $("#Estart").val('').css('border-color', '');
                $("#Eend").val('').css('border-color', '');
            });
        });


        $('#empTable').DataTable({
            columns: [
                { 'data': 'EventId' },
               { 'data': 'EventName' },
               { 'data': 'Venue' },
               { 'data': 'Address1' },
               { 'data': 'Address2' },
               { 'data': 'City' },
               { 'data': 'AdminCharge' },
               { 'data': 'PostalCode' },
               { 'data': 'strEventStartDate' },
               { 'data': 'strEventEndDate' }
               //,
               //{ 'data': 'BookingStartDate' },
               //{ 'data': 'BookingEndDate' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'EventServices.asmx/LoadEvents',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'EventId', "bSortable": true },
        { "data": 'EventName', "bSortable": true },
        { "data": 'Venue', "bSortable": true },
        { "data": 'Address1', "bSortable": true },
        { "data": 'Address2', "bSortable": true },
        { "data": 'City', "bSortable": true },
        { "data": 'AdminCharge', "bSortable": true },
        { "data": 'PostalCode', "bSortable": true },
        { "data": 'strEventStartDate', "bSortable": true },
        { "data": 'strEventEndDate', "bSortable": true }
        //,
        //{ 'data': 'BookingStartDate', "bSortable": true },
        //       { 'data': 'BookingEndDate', "bSortable": true }
            ]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                $("#modifybtn").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var ss = $(this).children('td:first-child').text();
                $("#modifybtn").attr('eid', ss);
                $("#modifybtn").show();
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();
    </script>

</asp:Content>
