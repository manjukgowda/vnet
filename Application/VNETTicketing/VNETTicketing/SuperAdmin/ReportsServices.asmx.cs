﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for ReportsServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ReportsServices : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void LoadTickets()
        {
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            sortingModal.SortColumnName = iSortCol_0 == 0 ? "BookingId" : iSortCol_0 == 1 ? "EventTitle" : "GroupName";
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            var obj = new ReportsClass();
            List<ReportsClass.TicketsDetails> allTickets = obj.GetAllTickets();
            if (sortingModal.SearchString != "")
            {
               allTickets = allTickets.Where(x => x.BookingId.ToString().ToLower().Contains(sortingModal.SearchString.ToLower()) || x.EventTitle.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.GroupName.Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredUsers = SortingAndPagingHelper.SortingAndPaging<ReportsClass.TicketsDetails>(allTickets, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = allTickets.Count,
                iTotalRecords = filteredUsers.Count(),
                data = filteredUsers
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }
    }
}
