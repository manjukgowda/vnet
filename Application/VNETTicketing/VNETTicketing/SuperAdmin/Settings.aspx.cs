﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadCurrency();
            //LoadSettingData();
            //empTable
            //curTable
            Page.Title = "Settings || The V";

        }

        private void LoadCurrency()
        {
            CurrencyClass obj = new CurrencyClass();
            var list = obj.GetAllCurrency();
            if (list.Count > 0)
            {
                lblBaseCurrency.Text = list[0].CurrencyName + " = ";
            }
            Repeater1.DataSource = list;
            Repeater1.DataBind();
        }

        private void LoadSettingData()
        {
            EventsClass objevents = new EventsClass();
            var allevents = objevents.GetAllEvents();
            foreach (var item in allevents)
            {
                TableRow tbody = new TableRow();
                tbody.TableSection = TableRowSection.TableBody;
                TableCell cell1 = new TableCell();
                cell1.Text = item.EventName;
                TextBox obj = new TextBox();
                obj.ID = "ticket_event" + item.EventId;
                obj.CssClass = "field";
                tbody.Cells.Add(cell1);
                TableCell cell2 = new TableCell();
                cell2.Controls.Add(obj);
                tbody.Cells.Add(cell2);
                //empTable.Rows.Add(tbody);
            }
            //throw new NotImplementedException();
        }
    }
}