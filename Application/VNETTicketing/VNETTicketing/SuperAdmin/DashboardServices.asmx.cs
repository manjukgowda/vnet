﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for DashboardServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DashboardServices : System.Web.Services.WebService
    {

        public class Counters
        {
            public int Admins { get; set; }
            public int Events { get; set; }
            public int Registrars { get; set; }
        }

        [WebMethod]
        public Counters GetCounters()
        {
            Counters c = new Counters();
            DashboardHelper obj = new DashboardHelper();
            c.Admins = obj.GetAdminsCount();
            c.Registrars = obj.GetRegistrarsCount();
            c.Events = obj.GetEventsCount();
            return c;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMorisAreaData()
        {
            DashboardHelper obj = new DashboardHelper();
            List<DashboardHelper.CollectionsData> objreturn = obj.GetEventsCollections();
            //return objreturn;
            var sObj = new System.Web.Script.Serialization.JavaScriptSerializer();
            var data = sObj.Serialize(objreturn);
         //   data = data.Replace("\"EventID\"", "EventID").Replace("\"Users\"", "Users");
            return data;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetEventsCollectedDetails()
        {
            DashboardHelper obj = new DashboardHelper();
            List<DashboardHelper.CollectionsData> objreturn = obj.GetEventsCollections();
            //return objreturn;
            var sObj = new System.Web.Script.Serialization.JavaScriptSerializer();
            var data = sObj.Serialize(objreturn);
            //   data = data.Replace("\"EventID\"", "EventID").Replace("\"Users\"", "Users");
            return data;
        }
    }
}
