﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="VNETTicketing.SuperAdmin.CreateEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Assets/css/create-events.css" rel="stylesheet" type="text/css">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create events</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div id="demopage">
            <div class="container1">
                <ul class="rtabs">
                    <li><a href="#view1">General</a></li>
                    <li><a href="#view4">Admin fee for event transfer and promo code</a></li>
                    <!-- <li><a href="#view5">Ticket design</a></li> -->
                </ul>
                <div class="panel-container">
                    <div id="view1">
                        <form action="#" method="post">
                            <div class="login-fields">
                                <div class="field col-md-12">
                                    <label for="username">Event title:</label>
                                    <input type="text" id="eventtitle" name="eventtitle" value="" class="login username-field form-control" />
                                </div>
                                <div class="field col-md-12">
                                    <label for="eventprefix">Event prefix:</label>
                                    <!-- <input type="text" id="eventprefix" name="eventprefix" value="" class="login username-field form-control" oninput="myFunction()" /> -->
                                    <input type="text" id="myInput" oninput="myFunction()" class="login username-field form-control">
                                </div>
                                <!-- /field -->
                                <div class="field col-md-12">
                                    <label for="password">Event description:</label>
                                    <textarea class="login password-field form-control" id="eventdescription"></textarea>
                                </div>
                                <div class="field col-md-12">
                                    <label for="password">Venue name:</label>
                                    <input type="text" id="eventvenue" name="venue" value="" class="login password-field form-control" />
                                </div>
                             <div class="field col-md-12">
                                    <label for="password">Event fee:</label>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-3">
                                            <select class="form-control">
                                                <option value="1">$ USD</option>
                                                <option value="2">$ SGD</option>
                                                <option value="3">€ EUR</option>
                                                <option value="4">$ AUD</option>
                                                <option value="5">¥ JPY</option>
                                                <option value="6">¥ CHN</option>
                                                <option value="7">฿ THB</option>
                                                <option value="8">RM MYR</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9 col-xs-9">
                                            <input type="number" id="e_fee" placeholder="Fee" name="event fee" value="" class="login password-field form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="field col-md-12">
                                    <label for="bookingid">Booking ID:</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-1 col-xs-3">
                                                <!-- <p id="bookprefix"></p> -->
                                                <p id="demo" class="lead"></p>
                                            </div>
                                            <div class="col-md-5 col-xs-3">
                                                <input type="number" id="bookingsid" name="BookingID" value="" placeholder="From" class="login password-field form-control" />
                                            </div>
                                            <div class="col-md-1 col-xs-3">
                                                <!-- <p id="bookprefix"></p> -->
                                                <p id="demo1" class="lead"></p>
                                            </div>
                                            <div class="col-md-5 col-xs-3">
                                                <input type="number" id="bookingeid" name="BookingID" value="" placeholder="to" class="login password-field form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field col-md-12">
                                    <label for="ticketid">Ticket ID:</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6 col-xs-6">
                                                <input type="number" id="ticketsid" name="startid" value="" placeholder="From" class="login password-field form-control" />
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <input type="number" id="ticketeid" name="endid" value="" placeholder="to" class="login password-field form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field col-md-12">
                                    <label for="password">Address line 1:</label>
                                    <input type="text" id="eventaddress1" name="address1" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-12">
                                    <label for="password">Address line 2:</label>
                                    <input type="text" id="eventaddress2" name="address2" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-6">
                                    <label for="password">City:</label>
                                    <input type="text" id="eventcity" name="city" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-6">
                                    <label for="password">Postal code:</label>
                                    <input type="text" id="eventpin" name="postal" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-6">
                                    <label for="password">Event start date:</label>
                                    <input type="text" id="s_date" name="eventstart" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-6">
                                    <label for="password">Event end date:</label>
                                    <input type="text" id="e_day" name="eventend" value="" class="login password-field form-control" />
                                </div>
                                <div class="field col-md-12">
                                    <label for="password">Groups:</label>
                                    <input type="text" id="groups" name="group" value="" class="login password-field form-control" placeholder="Add comma for multiple group names " />
                                </div>
                                <!-- /password -->
                                <div class="login-actions">
                                    <a id="btnNextView1" class="button btn btn-success btn-large pull-left" href="#">Save changes</a>
                                    <%--<asp:Button Text="Save Changes" runat="server" CssClass="button btn btn-success btn-large pull-left" ID="btnNext1" OnClientClick="return secondtab();" OnClick="btnNext1_Click" />
                                    <asp:LinkButton Text="Save Chanages" runat="server" ID="lnkbtnNext1" OnClick="lnkbtnLinkButton_Click" OnClientClick="secondtab();" />--%>
                                </div>
                            </div>
                            <!-- /login-fields -->

                        </form>
                    </div>
                    <div id="view4">
                        <div class="well">This fee is used to charge end-users whenever they wish to transfer events</div>
                        <form action="#" method="post">
                            <div class="login-fields">
                                <div class="field">
                                    <label for="password">Admin fee:</label>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-3">
                                            <select class="form-control">
                                                <option value="USD">$ USD</option>
                                                <option value="SGD">$ SGD</option>
                                                <option value="EUR">€ EUR</option>
                                                <option value="AUD">$ AUD</option>
                                                <option value="JPY">¥ JPY</option>
                                                <option value="CHN">¥ CHN</option>
                                                <option value="THB">฿ THB</option>
                                                <option value="MYR">RM MYR</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9 col-xs-9">
                                            <input type="number" id="a_fee" name="admin fee" value="" class="login password-field form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="lead">Multiple prices</div>
                                <div class="col-md-3">
                                    <input type="text" id="type" name="type" value="" class="login password-field form-control" placeholder="Type of ticket" />
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" id="multi">
                                        <option value="USD">$ USD</option>
                                        <option value="SGD">$ SGD</option>
                                        <option value="EUR">€ EUR</option>
                                        <option value="AUD">$ AUD</option>
                                        <option value="JPY">¥ JPY</option>
                                        <option value="CHN">¥ CHN</option>
                                        <option value="THB">฿ THB</option>
                                        <option value="MYR">RM MYR</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="number" id="ticketprice" name="price" value="" class="login password-field form-control" placeholder="Price of ticket" />
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary" id="confirm">Confirm</button>
                                </div>
                                <table class="table table-bordered" style="margin-top: 7%;">
                                    <thead>
                                        <tr>
                                            <th>Type of ticket</th>
                                            <th>Price of ticket</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                        <div class="lead">Promo code</div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Promo code</th>
                                    <th>Price</th>
                                    <th>Total allocation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>INFSG7089</td>
                                    <td>USD 450</td>
                                    <td>1000</td>
                                </tr>
                                <tr>
                                    <td>GENSI1517</td>
                                    <td>USD 400</td>
                                    <td>500</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="">New</button>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Promo code details</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="#" method="post">
                                            <div class="login-fields">
                                                <div class="field">
                                                    <label for="promocode">Promo code:</label>
                                                    <input type="text" id="promotitle" name="promotitle" value="" class="login username-field form-control" />
                                                </div>
                                                <!-- /field -->
                                                <div class="field">
                                                    <label for="password">Price:</label>
                                                    <div class="row">
                                                        <div class="col-md-3 col-xs-3">
                                                            <select class="form-control">
                                                                <option value="USD">$ USD</option>
                                                                <option value="SGD">$ SGD</option>
                                                                <option value="EUR">€ EUR</option>
                                                                <option value="AUD">$ AUD</option>
                                                                <option value="JPY">¥ JPY</option>
                                                                <option value="CHN">¥ CHN</option>
                                                                <option value="THB">฿ THB</option>
                                                                <option value="MYR">RM MYR</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-9 col-xs-9">
                                                            <input type="number" id="ad_fee" name="admin fee" value="" class="login password-field form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <label for="password">Total allocation:</label>
                                                    <input type="number" id="allocation" name="allocation" value="" class="login password-field form-control" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addToTable">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="login-actions">
                            <a class="button btn btn-success btn-large pull-left" href="#" id="btnSubmit">Submit</a>
                        </div>
                    </div>

                    <!-- <div id="view5">
                                <h2>Bookmark Support</h2>
                                <p>Tab can also be opened via a bookmark anchor located anywhere on the page.</p>
                                <p id="mark3" style="background:#FFC; border:1px dotted red;padding:8px;">This is a paragraph with id="mark3".</p>
                                <p>By clicking the bookmark link <i>mark3</i> below, you will navigate to the paragraph above even if this panel is hidden before clicking the bookmark.</p>
                            </div> -->

                    <br />
                </div>
            </div>
        </div>

        <!-- Responsive tabs js -->
        <script src="../Assets/js/responsive-tabs.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../Assets/dist/js/sb-admin-2.js"></script>

        <!-- Nice edit js -->
        <script src="../Assets/js/nicEdit-latest.js"></script>

        <!-- bootstrap datepicker -->
        <script type="text/javascript" src="../Assets/js/bootstrap-datepicker.js"></script>

        <!-- Image gallery js -->
        <script type="text/javascript" src="../Assets/js/jquery.blueimp-gallery.min.js"></script>
        <script type="text/javascript" src="../Assets/js/bootstrap-image-gallery.min.js"></script>

        <script type="text/javascript">

            var event = {
                eventTitle: '',
                eventDescription: '',
                eventFee: $('.form-control').find(":selected").val() + "_",
                eventVenue: '',
                eventStartDate: '',
                eventEndDate: '',
                eventAdminFee: '',
                eventImagePath: '',
                eventAddress1: '',
                eventAddress2: '',
                eventCity: '',
                eventPin: '',
                eventStartBDate: '',
                eventEndBDate: '',
                eventprefix: '',
                bookingsid: '',
                bookingeid: '',
                ticketsid: '',
                ticketeid:''
            }

            $(document).ready(function () {
                $("#btnNextView1").click(function () {
                    seteventdetails();
                }
                //    function () {
                //    //                $('.container1  a[href="#view2"]').tab('show');
                //    event.eventTitle = $("#eventtitle").val();
                //    event.eventDescription = $(".nicEdit-main").text();
                //    event.eventFee += $("#e_fee").val();
                //    event.eventVenue = $("#eventvenue").val() + "_" + $("#eventaddress1").val() + "_" + $("#eventaddress2").val() + "_" + $("#eventcity").val() + "_" + $("#eventpin").val();
                //    event.eventStartDate = $("#s_date").val();
                //    event.eventEndDate = $("#e_day").val();
                //    //Move the below code on second tab click
                //    event.eventImagePath='';
                //}
                );

                function seteventdetails() {
                    event.eventTitle = $("#eventtitle").val();
                    event.eventDescription = $(".nicEdit-main").text();
                    event.eventFee += $("#e_fee").val();
                    event.eventVenue = $("#eventvenue").val();
                    event.eventAddress1 = $("#eventaddress1").val();
                    event.eventAddress2 = $("#eventaddress2").val();
                    event.eventCity = $("#eventcity").val();
                    event.eventPin = $("#eventpin").val();
                    event.eventStartDate = $("#s_date").val();
                    event.eventEndDate = $("#e_day").val();
                    //Move the below code on second tab click
                    event.eventImagePath = '';
                    //event.eventStartBDate = $("#bs_date").val(),
                    //event.eventEndBDate = $("#bs_date").val(),

                    event.eventprefix = $("#myInput").val(),
                    event.bookingsid = $("#bookingsid").val(),
                    event.bookingeid = $("#bookingeid").val(),
                    event.ticketsid = $("#ticketsid").val(),
                    event.ticketeid = $("#ticketeid").val()
                }

                $("#btnSubmit").click(function () {
                    event.eventAdminFee = $("#a_fee").val();
                    //console.log(event.eventDescription + "_" + event.eventTitle + "_" + event.eventAdminFee);

                    $.ajax({
                        type: "POST",
                        url: "EventServices.asmx/AddEvent",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ eventname: '" + event.eventTitle + "', eventdesc:'" + event.eventDescription +
                            "',eventFee:'" + event.eventFee + "', eventvenue: '" + event.eventVenue + "',eventsdate:'" +
                            event.eventStartDate + "',eventedate:'" + event.eventEndDate + "', adminfee:'" +
                            event.eventAdminFee + "',eventimage:'" + event.eventImagePath + "', eventaddress1:'" + event.eventAddress1 + "',eventaddress2:'" + event.eventAddress2 + "', eventcity:'" + event.eventCity + "',eventpostal:'" + event.eventPin + "', eventprefix:'" + event.eventprefix + "', bookingsid:'" + event.bookingsid + "', bookingeid:'" + event.bookingeid + "', ticketsid:'" + event.ticketsid + "', ticketeid:'"+event.ticketeid+"'}",
                        //data: "{ eventname: '" + event.eventTitle + "', eventdesc:'" + event.eventDescription + "', eventvenue: '" + event.eventVenue + "', adminfee:'" + eventAdminFee + "'}",
                        success: function (data) {
                            if (data.d == 'UId is exist!') {
                                $('#msg').text(data.d);
                                $('#msg').show();
                                $('#myModal').modal('show')
                            }
                            else {
                                $('#msg').hide();
                                $("#uid").val('');
                                $("#Uname").val('');
                                $("#cpassword").val('');
                                $("#recpassword").val('');
                                $('#registrarchk').prop('checked', true);
                                console.log(data);
                                location.reload();
                                //$('#empTable').DataTable().ajax.reload();
                                //$('#empTable').dataTable().fnDestroy();
                                //$('#empTable').dataTable();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                });
            });


            bkLib.onDomLoaded(nicEditors.allTextAreas);
            $('#s_date').datepicker();
            $('#e_day').datepicker();
            //$('#be_date').datepicker();
            //$('#bs_date').datepicker();
            $('#addToTable').click(function () {
                var prom = $('#promotitle').val(),
                cur = $('select').val(),
                price = $('#ad_fee').val(),
                allocation = $('#allocation').val();

                $('table tbody').append('<tr><td>' + prom + '</td><td>' + cur + ' ' + price + '</td><td>' + allocation + '</td></tr>');
            });

            /*get the value of event prefix*/
            /*var bla = $('#eventprefix').val();
            bla.oninput(function () {
                document.getElementByID('eventprefix').innerHTML() = bla;
            });*/
            function myFunction() {
                var x = document.getElementById("myInput").value;
                document.getElementById("demo").innerHTML = x;
                document.getElementById("demo1").innerHTML = x;
            }
            /*appending multiple prices into table*/
            $("#confirm").click(function (e) {
                var type = $('#type').val();
                var currency = $("#multi option:selected").val();
                var mprice = $('#ticketprice').val();

                $('table tbody').append('<tr><td>' + type + '</td><td>' + currency + ' ' + mprice + '</td></tr>');

                /*Clear the values in text boxes after adding to table*/
                document.getElementById("type").value = "";
                document.getElementById("ticketprice").value = "";
            })
        </script>
</asp:Content>
