﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for SettingsServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SettingsServices : System.Web.Services.WebService
    {
        SettingsClass obj;

        [WebMethod]
        public bool UpdateCurrencyRate(string curId, string rate)
        {
            int currencyId = Convert.ToInt32(curId);
            obj = new SettingsClass();
            return obj.UpdateCurrency(currencyId, rate);
        }

        [WebMethod]
        public bool UpdateTicketNumber(string eventid,string notickets)
        {
            obj = new SettingsClass();
            return obj.UpdateEvent(new SettingsClass.SettingsViewModel { EventId = int.Parse(eventid), TicketReserved = notickets });
        }
    }
}
