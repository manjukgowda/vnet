﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    /// <summary>
    /// Summary description for EventServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EventServices : System.Web.Services.WebService
    {
        EventsClass obj;
        [WebMethod]
        public void LoadEvents()
        {
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            switch (iSortCol_0)
            {
                case 0:
                    sortingModal.SortColumnName = "EventId";
                    break;
                case 1:
                    sortingModal.SortColumnName = "EventName";
                    break;
                case 2:
                    sortingModal.SortColumnName = "Venue";
                    break;
                case 3:
                    sortingModal.SortColumnName = "Address1";
                    break;
                case 4:
                    sortingModal.SortColumnName = "Address2";
                    break;
                case 5:
                    sortingModal.SortColumnName = "City";
                    break;
                case 6:
                    sortingModal.SortColumnName = "AdminCharge";
                    break;
                case 7:
                    sortingModal.SortColumnName = "PostalCode";
                    break;
                case 8:
                    sortingModal.SortColumnName = "EventStartDate";
                    break;
                case 9:
                    sortingModal.SortColumnName = "EventEndDate";
                    break;
                case 10:
                    sortingModal.SortColumnName = "BookingStartDate";
                    break;
                case 11:
                    sortingModal.SortColumnName = "BookingEndDate";
                    break;
                default:
                    break;
            }
            //sortingModal.SortColumnName = iSortCol_0 == 0 ? "UId" : iSortCol_0 == 1 ? "Username" : "UserType";
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            obj = new EventsClass();
            List<EventsClass.EventViewModel> allEvents = obj.GetAllEvents();
            if (sortingModal.SearchString != "")
            {
                allEvents = allEvents.Where(x => x.EventName.ToLower().Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredEvents = SortingAndPagingHelper.SortingAndPaging<EventsClass.EventViewModel>(allEvents, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = allEvents.Count,
                iTotalRecords = filteredEvents.Count(),
                data = filteredEvents
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }

        [WebMethod]
        public string AddEvent(string eventname, string eventdesc, string eventFee, string eventvenue, DateTime eventsdate, DateTime eventedate, string adminfee, string eventimage, string eventaddress1, string eventaddress2, string eventcity, string eventpostal, string eventprefix, string bookingsid, string bookingeid, string ticketsid, string ticketeid)
        {
            obj = new EventsClass();
            int neweventid = 0;
            //if (obj.IsUserIdExist(uid))
            //{
            //    return "UId is exist!";
            //}
            long startBookingId = Convert.ToInt64(bookingsid);
            long startTicketId = Convert.ToInt64(ticketsid);
            obj.AddEventr(new EventsClass.EventViewModel
            {
                AdminCharge = adminfee,
                EventStartDate = eventsdate,
                EventEndDate = (eventedate),
                EventDescription = eventdesc,
                EventName = eventname,
                IsActive = true,
                Status = "active",
                Venuename = eventvenue,
                AdminTransferFee = eventFee,
                Address1 = eventaddress1,
                Address2 = eventaddress2,
                City = eventcity,
                PostalCode = eventpostal,
                StartBookingId = startBookingId,
                StartTicketId = startTicketId,
                EventPrefix = eventprefix
            }, ref neweventid);

            return "Event is created successfully!";
        }
        [WebMethod]
        public bool DeleteEvent(string eventid)
        {
            obj = new EventsClass();
            //int eventId = int.Parse(eventid);
            return obj.DeleteEvent(int.Parse(eventid));
        }

        [WebMethod]
        public bool ModifyEvent(string eid, string eventname, string eventvenue, DateTime eventsdate, DateTime eventedate, string eventaddress1, string eventaddress2, string eventcity, string eventpostal)
        {
            obj = new EventsClass();
            return obj.ModifyEvent(new EventsClass.EventViewModel 
            {
                EventId = Convert.ToInt32(eid),
                EventName = eventname, 
                Venue = eventvenue,
                EventStartDate = (eventsdate),
                EventEndDate = (eventedate),
                Address1 = eventaddress1, 
                Address2 = eventaddress2,
                City = eventcity,
                PostalCode = eventpostal
            });

        }
        [WebMethod]
        public EventsClass.EventViewModel GetEvent(string eid)
        {
            obj = new EventsClass();

            return obj.GetEvent(Convert.ToInt32(eid));

        }

        [WebMethod]
        public void AddPromoCode(string promocode, string promocurrid, string promoval, string promoallocation, string promoeventid, string minimumamount)
        {
            obj = new EventsClass();
            obj.AddEventPromo(new EventsClass.EventViewModel { PromoName = promocode, PromoCurrencyMode = int.Parse(promocurrid), PromoPrice = promoval, PromoAllocation = promoallocation, EventId = int.Parse(promoeventid), PromoMinAmount = minimumamount });
        }

        //[WebMethod]
        //public List<EventsClass.EventViewModel> GetSettingEvent()
        //{
        //    int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
        //    int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
        //    int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
        //    string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
        //    int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
        //    string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

        //    string participant = HttpContext.Current.Request.Params["iParticipant"];
        //    SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
        //    sortingModal.PageSize = iDisplayLength;
        //    switch (iSortCol_0)
        //    {
        //        case 0:
        //            sortingModal.SortColumnName = "EventId";
        //            break;
        //        case 1:
        //            sortingModal.SortColumnName = "EventName";
        //            break;
        //    }
        //    sortingModal.SortOrder = sSortDir_0;
        //    sortingModal.PageSelected = iDisplayStart;
        //    sortingModal.SearchString = rawSearch;
        //    obj = new EventsClass();
        //    return obj.GetAllEvents();
        //}
    }
}
