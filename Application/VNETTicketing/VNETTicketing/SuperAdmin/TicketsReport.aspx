﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="TicketsReport.aspx.cs" Inherits="VNETTicketing.SuperAdmin.TicketsReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tickets report</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Booking Id</th>
                            <th>Event Name</th>
                            <th>Group Name</th>
                            <th>Booked By</th>
                            <th>NoOfTickets</th>
                            <th>Payment Mode</th>
                            <th>Total Amount</th>
                            <th>Discount</th>
                            <th>Net Amount</th>
                            <th>Promo Code</th>
                            <th>Status</th>
                            <th>Booking DateTime</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->
    <link rel="stylesheet" type="text/css" href="../Assets/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Assets/css/buttons.dataTables.min.css">
    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="../Assets/js/jszip.min.js"></script>
    <script type="text/javascript" src="../Assets/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vfs_fonts.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../Assets/js/buttons.print.min.js"></script>

    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $('#empTable').DataTable({
            columns: [
               { 'data': 'BookingId' },
               { 'data': 'EventTitle' },
               { 'data': 'GroupName' },
               { 'data': 'BookedBy' },
               { 'data': 'NoOfTickets' },
               { 'data': 'PaymentMode' },
               { 'data': 'TotalAmount' },
               { 'data': 'Discount' },
               { 'data': 'NetAmount' },
               { 'data': 'PromoCodeApplied' },
               { 'data': 'Status' },
               { 'data': 'DateTime' },
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            iDisplayLength: 10000,
            lengthMenu: [[10000], [10000]],
            dom: 'Bfrtip',
            sAjaxSource: 'ReportsServices.asmx/LoadTickets',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'BookingId', "bSortable": true },
        { "data": 'EventTitle', "bSortable": true },
        { "data": 'GroupName', "bSortable": true },
        { "data": 'BookedBy', "bSortable": true },
        { "data": 'NoOfTickets', "bSortable": true },
        { "data": 'PaymentMode', "bSortable": true },
        { "data": 'TotalAmount', "bSortable": true },
        { "data": 'Discount', "bSortable": true },
        { "data": 'NetAmount', "bSortable": true },
        { "data": 'PromoCodeApplied', "bSortable": true },
        { "data": 'Status', "bSortable": true },
        { "data": 'DateTime', "bSortable": true }
            ],
            select: true,
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
            }]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    </script>

</asp:Content>
