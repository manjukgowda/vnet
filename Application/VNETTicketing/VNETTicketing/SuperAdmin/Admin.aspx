﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SuperAdmin.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="VNETTicketing.SuperAdmin.Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .error
        {
            color: #D8000C;
            background-color: #FFBABA;
            border: double;
            margin: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create admin</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="addusermodal" data-toggle="modal" data-target="#myModal">Create admin</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Email ID</th>
                            <th>Role</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Create admin</h4>
                        </div>
                        <div class="modal-body">
                            <form id="addUserForm" action="#" method="post">
                                <div class="login-fields">
                                    <div class="field">
                                        <label for="username">User ID:</label>
                                        <input type="text" id="uid" name="uid" value="" class="login username-field form-control" />
                                        <label id="uiderror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Email ID:</label>
                                        <input type="text" id="Uname" name="Uname" value="" class="login username-field form-control" />
                                        <label id="unameerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Custom password:</label>
                                        <input type="password" id="cpassword" name="custompassword" value="" class="login username-field form-control" />
                                        <label id="pwderror"></label>
                                    </div>
                                    <!-- /field -->
                                    <div class="field">
                                        <label for="password">Re-enter custom password</label>
                                        <input type="password" id="recpassword" name="recustompassword" value="" class="login username-field form-control" />
                                        <label id="rpwderror"></label>
                                    </div>
<%--                                    <div class="field" style="margin-top: 2%;">
                                        <label for="password">Make admin:</label>
                                        <input type="checkbox" id="adminchk" name="my-checkbox" data-on-text="Active" data-off-text="Inactive" checked>
                                    </div>--%>
                                </div>
                                <!-- /login-fields -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnAddUser" class="btn btn-danger" data-dismiss="modal">Okay</button>
                            <button type="button" id="btnClose" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div><span id="msg" class="error"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <!-- <script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#msg').hide();

            $("#btnAddUser").click(function () {

                var flag = true;
                if ($("#uid").val() == '' || $("#uid").val() == null) {
                    $('#uid').css('border-color', 'red');
                    $('#uiderror').fadeIn(1000);
                    $('#uiderror').html('User id can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#uid').css('border-color', ''); }

                if ($("#Uname").val() == '' || $("#Uname").val() == null) {
                    $('#Uname').css('border-color', 'red');
                    $('#unameerror').fadeIn(1000);
                    $('#unameerror').html('User name can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#Uname').css('border-color', ''); }

                if ($("#cpassword").val() == '' || $("#cpassword").val() == null) {
                    $('#cpassword').css('border-color', 'red');
                    $('#pwderror').fadeIn(1000);
                    $('#pwderror').html('Password can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else { $('#cpassword').css('border-color', ''); }

                if ($("#recpassword").val() == '' || $("#recpassword").val() == null) {
                    $('#recpassword').css('border-color', 'red');
                    $('#rpwderror').fadeIn(1000);
                    $('#rpwderror').html('Re enter password can not be empty!').css('color', 'red').fadeOut(10000);
                    flag = false;
                }
                else {
                    if ($("#recpassword").val() != $("#cpassword").val()) {
                        flag = false;
                        $('#recpassword').css('border-color', 'red');
                        $('#rpwderror').fadeIn(1000);
                        $('#rpwderror').html('Password and Confirm Password are not matching!').css('color', 'red').fadeOut(10000);
                    }
                    else { $('#recpassword').css('border-color', ''); }
                }

                if (!flag) {
                    return flag;
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "AdminServices.asmx/AddUser",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ uid: '" + $("#uid").val() + "', uname:'" + $("#Uname").val() + "', password: '" + $("#cpassword").val() + "', isadmin:'true'}",
                        success: function (data) {
                            if (data.d == 'UId is exist!') {
                                $('#msg').text(data.d);
                                $('#msg').show();
                                $('#myModal').modal('show')
                            }
                            else {
                                $('#msg').hide();
                                $("#uid").val('');
                                $("#Uname").val('');
                                $("#cpassword").val('');
                                $("#recpassword").val('');
                                //$('#adminchk').prop('checked', true);
                                console.log(data);
                                location.reload();
                                //$('#empTable').DataTable().ajax.reload();
                                //$('#empTable').dataTable().fnDestroy();
                                //$('#empTable').dataTable();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                }
            });

            $("#btnClose").click(function () {
                $('#uid').css('border-color', '');
                $('#Uname').css('border-color', '');
                $('#cpassword').css('border-color', '');
                $('#recpassword').css('border-color', '');
            });

        });


        $('#empTable').DataTable({
            columns: [
               { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'UserType' },
               { 'data': 'Status' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: 'AdminServices.asmx/LoadAdmins',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UserId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'UserType', "bSortable": true },
        { "data": 'Status', "bSortable": true }
            ]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();
    </script>

</asp:Content>
