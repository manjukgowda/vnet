﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.SuperAdmin
{
    public partial class Events : System.Web.UI.Page
    {
        EventsClass obj;
        CurrencyClass objcur;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCurrencyMode();
                Page.Title = "Create Events || The V";
            }
        }

        protected void btnCreateEvent_Click(object sender, EventArgs e)
        {
            obj = new EventsClass();
            int neweventid = 0;
            if (obj.IsEventExist(eventtitle.Value))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            else
            {
                string groups = txtGroups.Value;
                string originalFileName = Path.GetFileName(fupEventFile.FileName);
                string fileName = Guid.NewGuid().ToString().Substring(1, 10) + Path.GetExtension(fupEventFile.FileName);
                string path = "~//Images/" + fileName;
                fupEventFile.SaveAs(Server.MapPath(path));

                var newEvent = new EventsClass.EventViewModel();
                newEvent.EventName = eventtitle.Value;
                newEvent.EventPrefix = eventprefix.Value;
                newEvent.EventDescription = eventdescription.Value;
                newEvent.Venuename = eventvenue.Value;
                newEvent.CurrencyMode = Convert.ToInt32(drp_currency.SelectedValue);
                //EventFee = e_fee.Value;
                newEvent.AdminTransferFee = ae_fee.Value;
               // newEvent.StartBookingId = Convert.ToInt64(bookingsid.Value);
                newEvent.StartTicketId = Convert.ToInt64(ticketsid.Value);
                newEvent.Address1 = eventaddress1.Value;
                newEvent.Address2 = eventaddress2.Value;
                newEvent.City = eventcity.Value;
                newEvent.PostalCode = eventpin.Value;
                newEvent.EventStartDate =Convert.ToDateTime( s_date.Value);
                newEvent.EventEndDate =Convert.ToDateTime( e_day.Value);
                newEvent.IsActive = true;
                newEvent.Status = "active";
                newEvent.FileName = originalFileName;
                newEvent.FilePath = path;
                newEvent.GroupNames = groups;
                obj.AddEventr(newEvent, ref neweventid);
                Response.Redirect("EventFeeAndPromoCodes.aspx?id=" + neweventid + "");
            }
        }

        private void BindCurrencyMode()
        {
            objcur = new CurrencyClass();
            var cur = objcur.GetAllCurrency();
            drp_currency.DataSource = cur;
            drp_currency.DataTextField = "CurrencyName";
            drp_currency.DataValueField = "CurrencyModeId";
            drp_currency.DataBind();
        }
    }
}