﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGroups();
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Registrations obj = new Registrations();
            if (obj.RegisterUser(txtName.Text, ddlCountry.SelectedValue,txtEmail.Text,txtPassword.Text))
            {
                //txtEmail.Text = "";
                //txtName.Text ="";
                //txtPassword.Text = "";
                Session["useremail"] = txtEmail.Text;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Registration is successful! Please login)", true);
                Response.Redirect("VerifyEmail.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Email id is exist!)", true);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int noOfMembers = Convert.ToInt32(txtMembers.Text);
            Registrations obj = new Registrations();
            obj.AddGroup(txtGroupName.Text, txtGroupLeader.Text, txtGroupEmailAddress.Text, noOfMembers);
            LoadGroups();
        }

        private void LoadGroups()
        {
            //Registrations obj = new Registrations();
            //ddlGroup.DataSource = obj.LoadGroups();
            //ddlGroup.DataTextField = "GroupName";
            //ddlGroup.DataValueField = "GroupId";
            //ddlGroup.DataBind();
        }
    }
}