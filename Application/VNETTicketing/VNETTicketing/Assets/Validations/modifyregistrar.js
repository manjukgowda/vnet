﻿function validatemodifyregistrar() {
    var flag = true;

    if ($("#ContentPlaceHolder1_ddlEvents").val() == '0' || $("#ContentPlaceHolder1_ddlEvents").val() == null) {
        $('#ContentPlaceHolder1_ddlEvents').css('border-color', 'red');
        $('#ddleventerror').fadeIn(1000);
        $('#ddleventerror').html('Event can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    }
    else { $('#ContentPlaceHolder1_ddlEvents').css('border-color', ''); }

    if ($("#password").val() == '' || $("#password").val() == null) {
        $('#password').css('border-color', 'red');
        $('#cpwderror').fadeIn(1000);
        $('#cpwderror').html('Custom password can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    }
    else { $('#cpwderror').css('border-color', ''); }


    if ($("#recpassword").val() == '' || $("#recpassword").val() == null) {
        $('#recpassword').css('border-color', 'red');
        $('#rpwderror').fadeIn(1000);
        $('#rpwderror').html('Re enter password can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    }
    else {
        if ($("#recpassword").val() != $("#password").val()) {
            flag = false;
            $('#recpassword').css('border-color', 'red');
            $('#rpwderror').fadeIn(1000);
            $('#rpwderror').html('Password and Confirm Password are not matching!').css('color', 'red').fadeOut(10000);
        }
        else { $('#recpassword').css('border-color', ''); }
    }

    if ($("#s_date").val() == '' || $("#s_date").val() == null) {
        $('#s_date').css('border-color', 'red');
        $('#sdateerror').fadeIn(1000);
        $('#sdateerror').html('Start date can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    }
    else { $('#s_date').css('border-color', ''); }

    if ($("#e_date").val() == '' || $("#e_date").val() == null) {
        $('#e_date').css('border-color', 'red');
        $('#edateerror').fadeIn(1000);
        $('#edateerror').html('End date can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    }
    else {
        if (new Date($("#e_date").val()) < new Date($("#s_date").val())) {//compare end <=, not >=
            $('#e_date').css('border-color', 'red');
            $('#edateerror').fadeIn(1000);
            $('#edateerror').html('End date can not less than Start date!').css('color', 'red').fadeOut(10000);
            flag = false;
        }
        else {
            $('#e_date').css('border-color', '');
        }
    }
    return flag;
}