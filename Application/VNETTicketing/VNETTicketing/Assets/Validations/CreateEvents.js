﻿$('#ContentPlaceHolder1_btnCreateEvent').click(function () {
    var etitle = $('#ContentPlaceHolder1_eventtitle').val();
    var eprefix = $('#ContentPlaceHolder1_eventprefix').val();
    var edesc = $('#ContentPlaceHolder1_eventdescription').val();
    var vname = $('#ContentPlaceHolder1_eventvenue').val();
    var efee = $("#ContentPlaceHolder1_ae_fee").val()
    var bstartid = $('#ContentPlaceHolder1_bookingsid').val();
    var bendid = $('#ContentPlaceHolder1_bookingeid').val();
    var tstartid = $('#ContentPlaceHolder1_ticketsid').val();
    var tendid = $('#ContentPlaceHolder1_ticketeid').val();
    var add = $('#ContentPlaceHolder1_eventaddress1').val();
    var city = $('#ContentPlaceHolder1_eventcity').val();
    var pcode = $('#ContentPlaceHolder1_eventpin').val();
    var estartid = $('#ContentPlaceHolder1_s_date').val();
    var eendid = $('#ContentPlaceHolder1_e_day').val();
    var group = $('#ContentPlaceHolder1_txtGroups').val();

    var flag = true;

    if (etitle == '' || etitle == null) {
        $('#ContentPlaceHolder1_eventtitle').css('border-color', 'red');
        $('#titleerror').fadeIn(1000);
        $('#titleerror').html('Event title can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventtitle').css('border-color', '');
    }

    if (eprefix == '' || eprefix == null) {
        $('#ContentPlaceHolder1_eventprefix').css('border-color', 'red');
        $('#prefixerror').fadeIn(1000);
        $('#prefixerror').html('Event Prefix can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventprefix').css('border-color', '');
    }

    if (edesc == '' || edesc == null) {
        $('#ContentPlaceHolder1_eventdescription').css('border-color', 'red');
        $('#descerror').fadeIn(1000);
        $("#descerror").html('Event description can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventdescription').css('border-color', '');
    }

    if (vname == '' || vname == null) {
        $('#ContentPlaceHolder1_eventvenue').css('border-color', 'red');
        $('#venueerror').fadeIn(1000);
        $('#venueerror').html('Venue can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventvenue').css('border-color', '');
    }

    if (efee == '' || efee == null) {
        $('#ContentPlaceHolder1_ae_fee').css('border-color', 'red');
        $('#feeerror').fadeIn(1000);
        $('#feeerror').html('Event fee can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_ae_fee').css('border-color', '');
    }

    //if (bstartid == '' || bstartid == null) {
    //    $('#ContentPlaceHolder1_bookingsid').css('border-color', 'red');
    //    $('#bookerror1').fadeIn(1000);
    //    $('#bookerror1').html('Start sequence is empty').css('color','red').fadeOut(10000);
    //    flag = false;
    //} else {
    //    $('#ContentPlaceHolder1_bookingsid').css('border-color', '');
    //}
    //if (bendid == '' || bendid == null) {
    //    $('#ContentPlaceHolder1_bookingeid').css('border-color', 'red');
    //    $('#bookerror').fadeIn(1000);
    //    $('#bookerror').html('End sequence is empty').css('color','red').fadeOut(10000);
    //    flag = false;
    //} else {
    //    $('#ContentPlaceHolder1_bookingeid').css('border-color', '');
    //}

    if (tstartid == '' || tstartid == null) {
        $('#ContentPlaceHolder1_ticketsid').css('border-color', 'red');
        $('#ticketerror').fadeIn(1000);
        $('#ticketerror').html('Start sequence is empty!').css('color', 'red').fadeOut(10000)
        flag = false;
    } else {
        $('#ContentPlaceHolder1_ticketsid').css('border-color', '');
    }

    //if (tendid == '' || tendid == null) {
    //    $('#ContentPlaceHolder1_ticketeid').css('border-color', 'red');
    //    $('#ticketerror1').fadeIn(1000);
    //    $('#ticketerror1').html('End sequence can not be empty!').css('color','red').fadeOut(10000);
    //    flag = false;
    //} else {
    //    $('#ContentPlaceHolder1_ticketeid').css('border-color', '');
    //}

    if (add == '' || add == null) {
        $('#ContentPlaceHolder1_eventaddress1').css('border-color', 'red');
        $('#adderror').fadeIn(1000);
        $('#adderror').html('Event address can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventaddress1').css('border-color', '');
    }

    if (city == '' || city == null) {
        $('#ContentPlaceHolder1_eventcity').css('border-color', 'red');
        $('#cityerror').fadeIn(1000);
        $('#cityerror').html('City can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventcity').css('border-color', '');
    }

    if (pcode == '' || pcode == null) {
        $('#ContentPlaceHolder1_eventpin').css('border-color', 'red');
        $('#posterror').fadeIn(1000);
        $('#posterror').html('Postal code can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_eventpin').css('border-color', '');
    }

    if (estartid == '' || estartid == null) {
        $('#ContentPlaceHolder1_s_date').css('border-color', 'red');
        $('#starterror').fadeIn(1000);
        $('#starterror').html('Event start date can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_s_date').css('border-color', '');
    }

    if (eendid == '' || eendid == null) {
        $('#ContentPlaceHolder1_e_day').css('border-color', 'red');
        $('#enderror').fadeIn(1000);
        $('#enderror').html('Event end date can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        if (new Date($("#ContentPlaceHolder1_e_day").val()) < new Date($("#ContentPlaceHolder1_s_date").val())) {//compare end <=, not >=
            $('#e_date').css('border-color', 'red');
            $('#enderror').fadeIn(1000);
            $('#enderror').html('End date can not less than Start date!').css('color', 'red').fadeOut(10000);
            flag = false;
        }
        else {
            $('#ContentPlaceHolder1_e_day').css('border-color', '');
        }
    }

    if (group == '' || group == null) {
        $('#ContentPlaceHolder1_txtGroups').css('border-color', 'red');
        $('#grouperror').fadeIn(1000);
        $('#grouperror').html('Group can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_txtGroups').css('border-color', '');
    }



    return flag;
});