﻿$('#ContentPlaceHolder1_confirm').click(function () {
    var tickettype = $('#ContentPlaceHolder1_type').val();
    var priceticket = $('#ContentPlaceHolder1_ticketprice').val();
    var ddlevents = $('#ContentPlaceHolder1_ddlevents').val();

    var flag = true;

    if (tickettype == '' || tickettype == null) {
        $('#ContentPlaceHolder1_type').css('border-color', 'red');
        $('#tickettypeerror').fadeIn(1000);
        $('#tickettypeerror').html('Ticket type can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_type').css('border-color', '');
    }

    if (priceticket == '' || priceticket == null) {
        $('#ContentPlaceHolder1_ticketprice').css('border-color', 'red');
        $('#priceticketerror').fadeIn(1000);
        $('#priceticketerror').html('Price of ticket can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_ticketprice').css('border-color', '');
    }

    if (ddlevents == '0' || priceticket == null) {
        $('#ContentPlaceHolder1_ddlevents').css('border-color', 'red');
        $('#ddleventerror').fadeIn(1000);
        $('#ddleventerror').html('Select valid event!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ContentPlaceHolder1_ddlevents').css('border-color', '');
    }

    return flag;
});


$('#addToTable').click(function () {
    var promotitle = $('#promotitle').val();
    var promoprice = $('#ad_fee').val();
    var maxalloc = $('#maxlimit').val();
    var flag = true;

    if (promotitle == '' || promotitle == null) {
        $('#promotitle').css('border-color', 'red');
        $('#promocodeerror').fadeIn(1000);
        $('#promocodeerror').html('Promo title can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#promotitle').css('border-color', '');
    }

    if (promoprice == '' || promoprice == null) {
        $('#ad_fee').css('border-color', 'red');
        $('#promopriceerror').fadeIn(1000);
        $('#promopriceerror').html('Promo price can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#ad_fee').css('border-color', '');
    }

    if (maxalloc == '' || maxalloc == null) {
        $('#maxlimit').css('border-color', 'red');
        $('#maxlimiterror').fadeIn(1000);
        $('#maxlimiterror').html('Max. Allocation can not be empty!').css('color', 'red').fadeOut(10000);
        flag = false;
    } else {
        $('#maxlimit').css('border-color', '');
    }
    return flag;
});

$('#newpromo,#closePromo').click(function () {
    $('#promotitle').css('border-color', '').val('');
    $('#ad_fee').css('border-color', '').val('');
});