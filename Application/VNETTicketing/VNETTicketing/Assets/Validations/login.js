$(document).ready(function () {
    $('#btnSignIn').click(function (e) {

        var username = $('#username').val();
        var password = $('#password').val();
        var flag = true;
        ////validation
        //if (username == "" || username == null) {
        //    $('#dis').slideDown().html('<span style="color: red;">Please type Username!</span>');
        //    flag = false;
        //}
        //else {
        //    $('#dis').slideUp();
        //}
        //if (password == "" || password == null) {
        //    $('#dis1').slideDown().html('<span style="color: red;">Please type Password!</span>');
        //    flag= false;
        //}
        //else {
        //    $('#dis1').slideUp();
        //}
        //return flag;

        if (username == "" || username == null) {
            $('#username').css('border-color', 'red');
            $('#dis').fadeIn(1000);
            $('#dis').html("Please provide username!").css('color','red').fadeOut(3000);
            flag = false;
        }
        //else if (!(username.match(email_reg))) {
        //    $('#username').css('border-color', 'red');
        //    $('#dis').fadeIn(1000);
        //    $('#dis').html("Email address should be in format abc@def.xyz").fadeOut(5000);
        //}
        else {
            $('#username').css('border-color', '');
        }

        if (password == '' || password == null) {
            $('#password').css('border-color', 'red');
            $('#dis1').fadeIn(1000);
            $('#dis1').html("Please provide password!").css('color', 'red').fadeOut(5000);
            flag = false;
        } else {
            $('#password').css('border-color', '');
        }
        return flag;
    });
});