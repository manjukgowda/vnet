function GetMorisEventsData() {
    var morisarea = "";
    $.ajax({
        type: "POST",
        url: "DashboardServices.asmx/GetMorisAreaData",
        dataType: 'json',
        traditional: true,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            morisarea = data.d;
            console.log(morisarea);
        },
        error: function (data) { console.log(data) }
    });
    return morisarea;
}


function GetEventsCollectionData() {
    var cashCollected = "";
    $.ajax({
        type: "POST",
        url: "DashboardServices.asmx/GetEventsCollectedDetails",
        dataType: 'json',
        traditional: true,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            cashCollected = data.d;
            console.log(morisarea);
        },
        error: function (data) { console.log(data) }
    });
    return cashCollected;
}

$(function () {
    var counter;
    LoadSADashboard();
    Morris.Area({
        element: 'morris-area-chart',
        parseTime: false,
        data: JSON.parse(GetMorisEventsData()),
        xkey: 'EventTitle',
        ykeys: ['Users'],
        labels: ['Users'],
        hideHover: 'auto',
        resize: true
    });


    Morris.Bar({
        element: 'morris-bar-chart',
        data: JSON.parse(GetEventsCollectionData()),
        xkey: 'EventTitle',
        ykeys: ['Users', 'Amount'],
        labels: ['Users', 'Cash collected'],
        hideHover: 'auto',
        resize: true
    });
});

function LoadSADashboard() {
    $.ajax({
        type: "POST",
        url: "DashboardServices.asmx/GetCounters",
        dataType: 'json',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            counter = data.d;
            console.log(counter.Events);
            Morris.Donut({
                element: 'morris-donut-chart',
                data: [{
                    label: "Total events",
                    value: counter.Events
                }, {
                    label: "Total admins",
                    value: counter.Admins
                }, {
                    label: "Total registrars",
                    value: counter.Registrars
                }],
                resize: true
            });
        },
        error: function (data) { console.log(data) }
    });
}
