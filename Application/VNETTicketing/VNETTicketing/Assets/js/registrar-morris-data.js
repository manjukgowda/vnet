﻿var counter;

$(function () {
    LoadAdminDashboard();
});

function LoadAdminDashboard() {
    $.ajax({
        type: "POST",
        url: "RegistrarDashboard.asmx/GetAdminDashboardCounters",
        dataType: 'json',
        traditional: true,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            counter = data.d;
            console.log(counter);
            Morris.Donut({
                element: 'morris-donut-chart',
                data: [{
                    label: "Unsold tickets",
                    value: counter.UnSoldTickets
                }, {
                    label: "Total tickets",
                    value: counter.TotalTickets
                }, {
                    label: "Sold tickets",
                    value: counter.SoldTickets
                }],
                resize: true
            });
            Morris.Donut({

                element: 'morris-donut-chartone',
                data: [{
                    label: "Validated Tickets",
                    value: counter.ValidatedTickets
                }, {
                    label: "Invalidated Tickets",
                    value: counter.InvalidatedTickets
                }],
                resize: true
            });
        },
        error: function (data) { console.log(data) }
    });
}
