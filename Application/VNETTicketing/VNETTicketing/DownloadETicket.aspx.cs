﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing
{
    public partial class DownloadETicket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblPaymentStatus.Text = "";
            string orderid = "";
            string tickid = "";
            if (Request.QueryString["eid"] != null && Request.QueryString["eid"] != "" && Request.QueryString["tid"] != null && Request.QueryString["tid"] != "")
            {
                orderid = Convert.ToString(Request.QueryString["eid"]);
                tickid = Convert.ToString(Request.QueryString["tid"]);

                if (orderid != null && orderid != "" && tickid != null && tickid != "" )
                {
                    ShowDetails(orderid,tickid);
                    
                }
            }
            if (Request.Form["status"] != null && Request.Form["orderid"] != null)
            {
                string status =  Convert.ToString(Request.Form["status"]);
                orderid =  Convert.ToString(Request.Form["orderid"]);

                NewBookingClass obj = new NewBookingClass();

                var booking = obj.GetBookingDetailsToPrint(orderid);

                if (status == "00")
                {
                    lblPaymentStatus.Text = "Your transaction is successfull";
                    lblPaymentStatus.ForeColor = Color.Green;


                    obj.UpdateBookingDetails(orderid, "Online", "Confirmed");

                    pnlPay.Visible = false;
                    pnlDownload.Visible = true;
                   // PreparePdf(orderid, booking.BookingUserDetail[0].TicketId);
                    ShowDetails(orderid);

                    MailSender.SendTicketConfirmationEmail(orderid, booking.BookingUserDetail[0].EventName);
                }
                else if (status == "11")
                {
                    lblPaymentStatus.Text = "Your transaction is failed : " + Request.Form["error_desc"];
                    lblPaymentStatus.ForeColor = Color.Red;
                    pnlPay.Visible = true;
                    pnlDownload.Visible = false;
                    ShowDetails(orderid);

                    MailSender.SendTicketFailureEmail(orderid, booking.BookingUserDetail[0].EventName);

                }
                else if (status == "22")
                {
                    lblPaymentStatus.Text = "Your transaction is pedning";
                    lblPaymentStatus.ForeColor = Color.Red;
                    ShowDetails(orderid);
                }
            }

        }

        public void ShowDetails(string bookId,string ticketid="")
        {
            string server = ConfigurationManager.AppSettings["server"].ToString();
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);

            if (objdetails.Status == "Confirmed")
            {
                if (ticketid == "")
                    ticketid = objdetails.BookingUserDetail[0].TicketId;
                PreparePdf(bookId, ticketid);
                pnlPay.Visible = false;
                pnlDownload.Visible = true;
            }
            else
            {
                pnlPay.Visible = true;
                pnlDownload.Visible = false;
            }

            imgEvent.ImageUrl = server + objdetails.ImagePath.Substring(2);
            lblBookingId.Text = objdetails.BookingId.ToString();
            lblEvent.Text = objdetails.EventName;
            lblVenue.Text = objdetails.Venue;
            lblEventDateTime.Text = objdetails.EventDateTime.ToString();
            lblNoOfTicket.Text = objdetails.NoTicket.ToString();
            lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
            lblDiscount.Text = Convert.ToString(objdetails.Discount);
            lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
            lblModeOfPayment.Text = objdetails.PaymentMode;
            lblStatus.Text = objdetails.Status;
            gvUsers.DataSource = objdetails.BookingUserDetail;
            gvUsers.DataBind();
            //string CustomerName = "Customer";
            //if (objdetails.BookingUserDetail.Count > 0)
            //    CustomerName = objdetails.BookingUserDetail[0].UName;

            //string EmailID = "manjumysore.k@gmail.com";
            //if (objdetails.BookingUserDetail.Count > 0)
            //    EmailID = objdetails.BookingUserDetail[0].EmailId;

            //GenerateBarcode(bookId, objdetails.EventName, CustomerName, EmailID);
            //SessionUser usr = (SessionUser)Session["usr"];
            //EventClass.EventViewModel objpro = (EventClass.EventViewModel)Session["objpro"];

        }

        public void GenerateBarcode(string bookingId, string eventname, string CustomerName, string EmailID)
        {
            string vpath = "~\\Images";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            vpath = "~\\Images\\Barcodes";
            if (!Directory.Exists(Server.MapPath(vpath)))
                Directory.CreateDirectory(Server.MapPath(vpath));
            string guid = Guid.NewGuid().ToString().Substring(1, 10);
            vpath = "~\\Images\\Barcodes\\" + guid + ".png";
            string server = ConfigurationManager.AppSettings["server"].ToString();
            string vpdf = "~\\Images\\Barcodespdf\\" + guid + ".pdf";

            string fullpath = server + "/Images/Barcodes/" + guid + ".png";
            string fullpathpdf = server + "/Images/Barcodespdf/" + guid + ".pdf";

            BarCodeHelper obj = new BarCodeHelper();
            obj.GenerateBarCode(bookingId, Server.MapPath(vpath));

            imgBarCode.ImageUrl = fullpath;
            //Export(vpdf);
            //MailSender.SendTicketConfirmationEmail(CustomerName, eventname, fullpathpdf, EmailID);
        }

        public void Export(string vpdf)
        {
            //try
            //{
            StringWriter stringWriter = new StringWriter();

            HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
            maindiv.RenderControl(htmlTextWriter);

            StringReader stringReader = new StringReader(stringWriter.ToString());

            Document Doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

            HTMLWorker htmlparser = new HTMLWorker(Doc);

            PdfWriter.GetInstance(Doc, new FileStream(Server.MapPath(vpdf), FileMode.Create));

            Doc.Open();

            htmlparser.Parse(stringReader);

            Doc.Close();
            //}
            //catch { }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


        public void PreparePdf(string bookingid, string ticketid)
        {
            PDFHelper obj = new PDFHelper();
            string pdf = obj.GeneratePDF(bookingid, ticketid);
            hlPrint.NavigateUrl = pdf;
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Response.ContentType = "Application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(hlPrint.NavigateUrl));
            string server = ConfigurationManager.AppSettings["server"].ToString();
            string filename = "~\\Images\\Barcodespdf\\" + Path.GetFileName(hlPrint.NavigateUrl);

            Response.TransmitFile(Server.MapPath(filename));
            Response.End();
        }

        protected void btnPAy_Click(object sender, EventArgs e)
        {
            string orderid = Convert.ToString(Request.QueryString["eid"]);
            NewBookingClass obj = new NewBookingClass();
            NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(orderid);


            string amount = objdetails.NetAmount.ToString();
            string desc = objdetails.EventName; //"DIGIReloadCouponRM30";
            string country = PaymentClass.PaymentHelperMethods.GetCountry();//"MY";
            string verifykey = PaymentClass.PaymentHelperMethods.GetVerifyKey();// "373b540e86a08e9f83b690c99ac11857";
            string merchantid = PaymentClass.PaymentHelperMethods.GetMerchantId();//"thevnet_Dev";
            //var vcode = md5(amount & merchantID & orderid & xxxxxxxxxxxx );
            // REPLACE xxxxxxxxxxxx with MOLPay Verify Key
            var vcode = MD5Alg.md5(String.Concat(amount, merchantid, orderid, verifykey));
            // REPLACE xxxxxxxxxxxx with MOLPay Verify Key
            //StringBuilder molipayurl = new StringBuilder("https://www.onlinepayment.com.my/MOLPay/pay/thevnet_Dev/?");
            StringBuilder molipayurl = new StringBuilder(PaymentClass.PaymentHelperMethods.GetMolipayURL());
            molipayurl.Append("amount=" + amount);
            molipayurl.Append("&orderid=" + orderid);
            //molipayurl.Append("&bill_name=" + name);
            molipayurl.Append("&bill_email=" + objdetails.BookingUserDetail[0].EmailId);
            molipayurl.Append("&bill_desc=" + desc);
            molipayurl.Append("&country=" + country);
            molipayurl.Append("&vcode=" + vcode);
            Response.Redirect(molipayurl.ToString());

        }


    }
}