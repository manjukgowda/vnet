﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Routing;

namespace VNETTicketing
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //RouteTable.Routes.MapHttpRoute(
            //    name: "VerifyMail",
            //    routeTemplate: "api/{controller}/{vnetcode}/{emailid}/{code}",
            //    defaults: new { controller = "VNetService" }
            //    );

            RouteTable.Routes.MapHttpRoute(
                name: "VerifyMail",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "VNetService", action = "VerifyEmail" }
                );

            RouteTable.Routes.MapHttpRoute(
                name: "VerifyUser",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "VNetService", action = "VerifyUser" }
                );

            RouteTable.Routes.MapHttpRoute(
                name: "RegisterUser",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "VNetService", action = "RegisterUser" }
                );

            RouteTable.Routes.MapHttpRoute(
            name: "GetEventDetail",
            routeTemplate: "api/{controller}/{action}",
            defaults: new { controller = "VNetService", action = "GetEventDetail" }
            );

            RouteTable.Routes.MapHttpRoute(
            name: "UserLogin",
            routeTemplate: "api/{controller}/{action}",
            defaults: new { controller = "VNetService", action = "UserLogin" }
            );

            RouteTable.Routes.MapHttpRoute(
            name: "GetIridDetail",
            routeTemplate: "api/{controller}/{action}",
            defaults: new { controller = "VNetService", action = "GetIridDetail" }
            );

            RouteTable.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = System.Web.Http.RouteParameter.Optional }
                );
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}