﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="GroupBooking.aspx.cs" Inherits="VNETTicketing.Admin.GroupBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="css/collections.css">
    <style>
        /*#barcode {
            width: 40%;
        }*/

        .submitarea {
            padding-bottom: 2%;
        }

        .modal-header {
            background-color: #f7f7f7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-12">
        <h1 class="page-header">New Booking (Group) </h1>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Event:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                </asp:DropDownList>

            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Ticket Type:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTicketTypes" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketTypes_SelectedIndexChanged">
                </asp:DropDownList>
                <p class="small">Ticket price is based on ticket types</p>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Price:</label>
                <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:HiddenField ID="hdncurrencytypeid" runat="server" />
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <asp:Label ID="lblTicketQty" runat="server"></asp:Label>
            <br />
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Upload CSV sheet containing group details: </label>  <a href="../Assets/Uploads/Group Upload.csv">Download Sample</a>
                <input runat="server" type="file" id="file" class="btn btn-primary" />
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnContinue" runat="server" CssClass="btn btn-primary" Text="Continue" OnClick="btnContinue_Click" />
        </div>
        <div class="col-md-12 col-xs-12">
            <asp:GridView ID="GridView1" runat="server"></asp:GridView>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
        </div>
    </div>
    <div id="modalError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">Only csv file can be uploaded!</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="divNoTickets" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <label for="promocode">The required no. of tickets are not available!</label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function openModalerror() {
            $('#modalError').modal('show');
        }
        function notickets() {
            $('#divNoTickets').modal('show');
        }
    </script>
</asp:Content>
