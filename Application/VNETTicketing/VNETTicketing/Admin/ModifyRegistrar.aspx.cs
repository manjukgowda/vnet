﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.Admin
{
    public partial class ModifyRegistrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var obj = new EventsClass();
                ddlEvents.DataSource = obj.GetAllEvents();
                ddlEvents.DataTextField = "EventName";
                ddlEvents.DataValueField = "EventId";
                ddlEvents.DataBind();
                ddlEvents.Items.Insert(0, new ListItem { Text = "-Select Event-", Value = "0" });
            }
        }
    }
}