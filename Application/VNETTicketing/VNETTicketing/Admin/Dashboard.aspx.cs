﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    DashboardHelper obj = new DashboardHelper();
                    var ds = obj.GetAdminDashboardData();
                    lblAmount.Text = Convert.ToString(ds.TotalAmount);
                    lblInvalidatedTickets.Text = Convert.ToString(ds.InvalidatedTickets);
                    lblValidatedTickets.Text = Convert.ToString(ds.ValidatedTickets);
                    lblTotalTickets.Text = Convert.ToString(ds.TotalTickets);
                }
                catch { }
            }
        }
    }
}