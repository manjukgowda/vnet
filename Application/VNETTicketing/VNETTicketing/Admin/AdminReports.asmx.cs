﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.SuperAdmin;

namespace VNETTicketing.Admin
{
    /// <summary>
    /// Summary description for AdminReports
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AdminReports : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void GetTicketsReports()
        {
            NewBookingClass obj = new NewBookingClass();
            
            int sEcho = Convert.ToInt32(HttpContext.Current.Request.Params["sEcho"]);
            int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayLength"]);
            int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request.Params["iDisplayStart"]);
            string rawSearch = HttpContext.Current.Request.Params["sSearch"].ToString();
            int iSortCol_0 = Convert.ToInt32(HttpContext.Current.Request.Params["iSortCol_0"]);
            string sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"].ToString();

            string participant = HttpContext.Current.Request.Params["iParticipant"];
            SortingAndPagingInfo sortingModal = new SortingAndPagingInfo();
            sortingModal.PageSize = iDisplayLength;
            sortingModal.SortColumnName = iSortCol_0 == 0 ? "Event" : iSortCol_0 == 1 ? "UserName" : iSortCol_0 == 2 ? "IRID" : iSortCol_0 == 3 ? "ModeOfPayment" : iSortCol_0 == 4 ? "TransferedTicket" : iSortCol_0 == 5 ? "TicketType" : "BookingDate";
            sortingModal.SortOrder = sSortDir_0;
            sortingModal.PageSelected = iDisplayStart;
            sortingModal.SearchString = rawSearch;
            var allTickets = obj.GetAdminTicketsReports();
            if (sortingModal.SearchString != "")
            {
                allTickets = allTickets.Where(x => x.Event.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.UserName.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.IRID.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.ModeOfPayment.ToLower().Contains(sortingModal.SearchString.ToLower()) || x.strBookingDate.ToLower().Contains(sortingModal.SearchString.ToLower())).ToList();
            }
            var filteredTickets = SortingAndPagingHelper.SortingAndPaging<NewBookingClass.TicketsReport>(allTickets, sortingModal);
            var result = new
            {
                iTotalDisplayRecords = allTickets.Count,
                iTotalRecords = filteredTickets.Count(),
                data = filteredTickets
            };

            var a = JsonConvert.SerializeObject(result);
            Context.Response.Write(a);
        }
    }
}
