﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.SuperAdmin;

namespace VNETTicketing.Admin
{
    public partial class Transfer : System.Web.UI.Page
    {
        TransferProcess objtrasfer;
        string BookingId;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnadmintransfer_Click(object sender, EventArgs e)
        {
            try
            {
                BookingId = Convert.ToString(txtAdminBarcode.Text);
                NewBookingClass obj = new NewBookingClass();

                NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(BookingId);
                if (objdetails != null)
                {
                    lblBookingId.Text = objdetails.EventBookingId;
                    lblEvent.Text = objdetails.EventName;
                    lblVenue.Text = objdetails.Venue;
                    lblEventDateTime.Text = objdetails.EventDateTime.ToString();
                    lblNoOfTicket.Text = objdetails.NoTicket.ToString();
                    lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
                    lblDiscount.Text = Convert.ToString(objdetails.Discount);
                    lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
                    lblModeOfPayment.Text = objdetails.PaymentMode;
                    lblStatus.Text = objdetails.Status;
                    gvUsers.DataSource = objdetails.BookingUserDetail;
                    gvUsers.DataBind();
                    pInfo.InnerHtml = "Successfully verified";
                    pnladmintrasfer.Visible = true;

                    ddlevents.Items.Clear();
                    EventsClass objevent = new EventsClass();
                    var events = objevent.GetAllEvents();
                    events = events.Where(x => x.EventId != objdetails.EventId).ToList();
                    ddlevents.DataSource = events;
                    ddlevents.DataTextField = "EventName";
                    ddlevents.DataValueField = "EventId";
                    ddlevents.DataBind();
                    //if (events.Count == 0)
                    //{
                        ddlevents.Items.Insert(0, new ListItem("-Select-", "0"));
                    //}
                }
                else
                {
                    pnladmintrasfer.Visible = false;
                    pInfo.InnerHtml = "Invalid barcode";
                }
            }
            catch (Exception ex)
            {
                pnladmintrasfer.Visible = false;
                pInfo.InnerHtml = "Error while verification: Invalid barcode";// +ex.Message;
            }
        }

        protected void ddlevents_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlevents.SelectedValue!= "-Select-")
                {
                    int eventid = Convert.ToInt32(ddlevents.SelectedValue);
                    LoadNoOfTickets(eventid);
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public void LoadNoOfTickets(int eventid)
        {
            objtrasfer = new TransferProcess();
            int available = objtrasfer.GetAvailableTicketsCount(eventid);
            if (available > 0)
            {
                lblTicketQty.Text = available + " tickets available for this event";
            }
            else
            {
                lblTicketQty.Text = "No tickets available for this event";
            }
            //ddlTicketQuantity.Items.Clear();
            //for (int i = 1; i <= available; i++)
            //{
            //    if (i > 10)
            //        return;
            //    ddlTicketQuantity.Items.Add(Convert.ToString(i));
            //}
        }

        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            if (txtAdminBarcode.Text != "0" && txtAdminBarcode.Text != "" )
            {
                objtrasfer = new TransferProcess();
                int eventid = Convert.ToInt32(ddlevents.SelectedValue);
                if(objtrasfer.TransferBooking(BookingId,eventid))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Transfered successfully!')", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Sorry, Ticket transfer failed')", true);
            }
        }

    }
}