﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="UsersDetails.aspx.cs" Inherits="VNETTicketing.Admin.UsersDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="css/collections.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-12">
        <h1 class="page-header">New Booking</h1>
    </div>
    <div class="container-fluid padding_null page_content">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <asp:Repeater runat="server" ID="rpt_participants" OnItemDataBound="rpt_participants_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-sm-12 col-lg-8 col-md-8">

                            <div class="panel panel-default">
                                <div class="panel-heading">Participant details</div>
                                <div class="panel-body">
                                    <div class="login-fields">
                                        <div class="field detailspart">
                                            <div class="field detailspart">
                                                <div class="col-md-5 col-xs-12">
                                                    <label for="irid">IR ID</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    <asp:TextBox ID="txtIRId" CssClass="login irid-field form-control" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtIRId" ValidationGroup="a" runat="server" ErrorMessage="IR Id required"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Button ID="btnVerify" OnClick="btnVerify_Click" CssClass="btn btn-primary" runat="server" Text="Verify" />
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-xs-12">
                                                <label for="fullname">Full name</label>
                                            </div>
                                            <div class="col-md-7 col-xs-12">
                                                <asp:TextBox ID="txtFullName" runat="server" class="login fullname-field form-control" ReadOnly="true"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtFullName" ValidationGroup="a" runat="server" ErrorMessage="Name required"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="col-md-5 col-xs-12">
                                                <label for="fullname">Email Id</label>
                                            </div>
                                            <div class="col-md-7 col-xs-12">
                                                <asp:TextBox ID="txtEmailID" runat="server" class="login fullname-field form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                            <div class="col-md-5 col-xs-12">
                                                <label for="fullname">Mobile Number</label>
                                            </div>
                                            <div class="col-md-7 col-xs-12">
                                                <asp:TextBox ID="txtMobile" runat="server" class="login fullname-field form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="row">
                <div class="col-md-12"></div>
                <asp:Button Text="Continue with payment" runat="server" ValidationGroup="a" ID="btnContinuePayment" CssClass="btn btn-primary" OnClick="btnContinuePayment_Click" />
            </div>
        </div>

        <div id="TicketsNotAvailable" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Oops...</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Sorry, the required no. of tickets are not available at this moment!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="InvalidIrid" class="modal fade" role="dialog"><div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Oops...</h4>
                    </div>
                    <div class="modal-body">
                        <label for="promocode">Sorry, you had entered invalid IRID!</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div></div>
    <script type="text/javascript">
        function TicketsNotAvailable() {
            $('#TicketsNotAvailable').modal('show');
        }
        function InvalidIRID() {
            $('#InvalidIrid').modal('show');
        }
    </script>
</asp:Content>

