﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.User;

namespace VNETTicketing.Admin
{
    public partial class UsersDetails : System.Web.UI.Page
    {
        NewBookingClass obj;
        NewBookingClass.NewBookingDetails sessdetails;
        IridClass objirid;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["newbookingdetails"] != null)
                {
                    List<Participants> participants = new List<Participants>();
                    count = Convert.ToInt32(Request.QueryString["Tickets"]);
                    for (int i = 0; i < count; i++)
                    {
                        Participants p = new Participants();
                        p.Id = i + 1;
                        participants.Add(p);
                    }
                    rpt_participants.DataSource = participants;
                    rpt_participants.DataBind();

                }
                else
                {
                    Response.Redirect("NewBooking.aspx");
                }
            }
        }

        public class Participants
        {
            public int Id { get; set; }
        }

        protected void rpt_participants_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType.ToString() == rpt_participants.ItemType)
            //{
            //    var btn = e.Item.FindControl("btnVerify") as Button;
            //    if (btn != null)
            //    {  // adding button event 
            //        btn.Click += btn_Click;
            //        //btn.Click += new EventHandler(btn_Click);
            //    }
            //}
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                Button btn = sender as Button;
                RepeaterItem item = btn.Parent as RepeaterItem;
                TextBox txtIR = item.FindControl("txtIRId") as TextBox;
                TextBox txtFName = item.FindControl("txtFullName") as TextBox;
                TextBox txtMobile = item.FindControl("txtMobile") as TextBox;
                TextBox txtEmailID = item.FindControl("txtEmailID") as TextBox;
                objirid = new IridClass();
                if (txtIR.Text.Count() > 0)
                {
                    ds = objirid.GetUserDetails(txtIR.Text);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        txtFName.Text = ds.Tables[0].Rows[0]["IRName"].ToString();
                        txtMobile.Text = ds.Tables[0].Rows[0]["ContactNoMobile"].ToString();
                        txtEmailID.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "InvalidIRID();", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "InvalidIRID();", true);
                }

                //txtFName.Text = txtIR.Text + " Name";
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('Please try again');", true);
            }
        }

        protected void btnContinuePayment_Click(object sender, EventArgs e)
        {
            count = Convert.ToInt32(Request.QueryString["Tickets"]);
            var usr = SessionUsers.GetCurrentUser(2);
            if (Session["newbookingdetails"] == null)
            {
                Response.Redirect("NewBooking.aspx");
            }
            sessdetails = (NewBookingClass.NewBookingDetails)Session["newbookingdetails"];
            int eventId = sessdetails.EventId;
            EventClass objEvent = new EventClass();
            int available = objEvent.GetAvailableTicketsCount(eventId);
            if (count <= available)
            {
                List<NewBookingClass.NewBookingDetails> bookingList = new List<NewBookingClass.NewBookingDetails>();
                obj = new NewBookingClass();
                foreach (RepeaterItem item in rpt_participants.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        TextBox txtIR = item.FindControl("txtIRId") as TextBox;
                        TextBox txtFName = item.FindControl("txtFullName") as TextBox;
                        TextBox txtMobile = item.FindControl("txtMobile") as TextBox;
                        TextBox txtEmailID = item.FindControl("txtEmailID") as TextBox;
                        NewBookingClass.NewBookingDetails objtemp = new NewBookingClass.NewBookingDetails();
                        objtemp.IRID = txtIR.Text;
                        objtemp.UName = txtFName.Text;
                        objtemp.EmailId = txtEmailID.Text;

                        objtemp.CurrencyMode = sessdetails.CurrencyMode;
                        objtemp.EventId = sessdetails.EventId;
                        objtemp.GroupId = sessdetails.GroupId;
                        objtemp.NoTicket = sessdetails.NoTicket;
                        objtemp.Price = sessdetails.Price;
                        objtemp.TicketType = sessdetails.TicketType;
                        objtemp.UserId = sessdetails.UserId;
                        objtemp.AR = 0;
                        objtemp.CashPaid = 0;
                        //objtemp.UserIds = sessdetails.UserIds;
                        bookingList.Add(objtemp);
                    }
                }

                Session["BoolingList"] = bookingList;
                //long bookingId = obj.AddUserTransaction(bookingList, Convert.ToInt32(usr.UserId), "Admin");
                //Session["BookingId"] = bookingId;
                Response.Redirect("PaymentConfirmation.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "TicketsNotAvailable();", true);

            }
        }
    }
}