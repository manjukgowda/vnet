﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="CreateRegistrars.aspx.cs" Inherits="VNETTicketing.Admin.CreateRegistrars" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .error {
            color: #D8000C;
            background-color: #FFBABA;
            border: double;
            margin: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Registrars</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="addusermodal" data-toggle="modal" data-target="#myModal">Create registrars</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>Email ID</th>
                            <th>Event name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Create registrar</h4>
                        </div>
                        <div class="modal-body">
                            <form id="addUserForm" action="#" method="post">
                                <div class="login-fields">
                                    <div class="field">
                                        <label for="username">Email ID:</label>
                                        <input type="text" id="Uname" name="Uname" value="" class="login username-field form-control" />
                                        <label id="unameerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Event name:</label>
                                        <asp:DropDownList ID="ddlEvents" class="login username-field form-control" runat="server"></asp:DropDownList>
                                        <label id="ddleventerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Custom password:</label>
                                        <input type="password" id="cpassword" name="custompassword" value="" class="login username-field form-control" />
                                        <label id="cpwderror"></label>
                                    </div>
                                    <!-- /field -->
                                    <div class="field">
                                        <label for="password">Re-enter custom password</label>
                                        <input type="password" id="recpassword" name="recustompassword" value="" class="login username-field form-control" />
                                        <label id="rpwderror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="password">From:</label>
                                        <input type="text" id="s_date" name="s_date" value="" class="login username-field form-control" />
                                        <label id="sdateerror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="password">To:</label>
                                        <input type="text" id="e_date" name="e_date" value="" class="login username-field form-control" />
                                        <label id="edateerror"></label>
                                    </div>
                                    <div class="field" style="margin-top: 5%;">
                                        <label for="password">make registrar:</label>
                                        <input type="checkbox" id="registrarchk" name="my-checkbox" data-on-text="Active" data-off-text="Inactive" checked>
                                    </div>
                                </div>
                                <!-- /login-fields -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnAddRegistrar" class="btn btn-danger" data-dismiss="modal">Okay</button>
                            <button type="button" id="btnClose" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div><span id="msg" class="error"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <script type="text/javascript" src="../Assets/js/bootstrap-datepicker.js"></script>

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>

    <script src="../Assets/Validations/createregistrar.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#msg').hide();
            $("#btnAddRegistrar").click(function () {

                //var flag = true;
                //if ($("#Uname").val() == '' || $("#Uname").val() == null) {
                //    $('#Uname').css('border-color', 'red');
                //    $('#unameerror').fadeIn(1000);
                //    $('#unameerror').html('User name can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#Uname').css('border-color', ''); }

                //if ($("#ContentPlaceHolder1_ddlEvents").val() == '0' || $("#ContentPlaceHolder1_ddlEvents").val() == null) {
                //    $('#ContentPlaceHolder1_ddlEvents').css('border-color', 'red');
                //    $('#ddleventerror').fadeIn(1000);
                //    $('#ddleventerror').html('Event can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#ContentPlaceHolder1_ddlEvents').css('border-color', ''); }

                //if ($("#cpassword").val() == '' || $("#cpassword").val() == null) {
                //    $('#cpassword').css('border-color', 'red');
                //    $('#cpwderror').fadeIn(1000);
                //    $('#cpwderror').html('Custom password can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#cpassword').css('border-color', ''); }


                //if ($("#recpassword").val() == '' || $("#recpassword").val() == null) {
                //    $('#recpassword').css('border-color', 'red');
                //    $('#rpwderror').fadeIn(1000);
                //    $('#rpwderror').html('Re enter password can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#recpassword').css('border-color', ''); }

                //if ($("#s_date").val() == '' || $("#s_date").val() == null) {
                //    $('#s_date').css('border-color', 'red');
                //    $('#sdateerror').fadeIn(1000);
                //    $('#sdateerror').html('Start date can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#s_date').css('border-color', ''); }

                //if ($("#e_date").val() == '' || $("#e_date").val() == null) {
                //    $('#e_date').css('border-color', 'red');
                //    $('#edateerror').fadeIn(1000);
                //    $('#edateerror').html('End date can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#e_date').css('border-color', ''); }

                //if (!flag) {
                //    return flag;
                //}
                var flag = validateregistrar();
                if (!flag) {
                    return flag;
                }
                else {
                    var eventid = $('#ContentPlaceHolder1_ddlEvents').val()
                    $.ajax({
                        type: "POST",
                        url: "../SuperAdmin/RegistrarServices.asmx/AddUser",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ uname:'" + $("#Uname").val() + "', password: '" + $("#cpassword").val() + "', isregistrar:'" + $('#registrarchk').is(':checked') + "', eventid: '" + eventid + "', from: '" + $("#s_date").val() + "', to: '" + $("#e_date").val() + "'}",
                        success: function (data) {
                            if (data.d == 'User Name is exist!') {
                                $('#msg').text(data.d);
                                $('#msg').show();
                                $('#myModal').modal('show')
                            }
                            else {
                                $('#msg').hide();
                                $("#uid").val('');
                                $("#Uname").val('');
                                $("#cpassword").val('');
                                $("#recpassword").val('');
                                $('#registrarchk').prop('checked', true);
                                console.log(data);
                                location.reload();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                }
            });
            $("#btnClose").click(function () {

                $('#Uname').css('border-color', '');
                $('#ContentPlaceHolder1_ddlEvents').css('border-color', '');
                $('#cpassword').css('border-color', '');
                $('#recpassword').css('border-color', '');
                $('#s_date').css('border-color', '');
                $('#e_date').css('border-color', '');
            });
        });


        $('#empTable').DataTable({
            columns: [
                { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'EventName' },
               { 'data': 'ValidFrom' },
               { 'data': 'ValidTo' },
               { 'data': 'UserType' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: '../SuperAdmin/RegistrarServices.asmx/LoadRegistrar',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'EventName', "bSortable": false },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'UserType', "bSortable": true }
            ]
        });


        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();

        $('#s_date').datepicker();
        $('#e_date').datepicker();
    </script>
</asp:Content>
