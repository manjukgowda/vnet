﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="ModifyRegistrar.aspx.cs" Inherits="VNETTicketing.Admin.ModifyRegistrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Modify registrars</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <a class="btn btn-primary" id="modifybtn" uid="0" data-toggle="modal">Modify registrars</a>
        <br>
        <br>
        <div class="row">
            <div class="container">
                <table id="empTable" class="display table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>Email ID</th>
                            <th>Event name</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <%--<tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>User name</th>
                            <th>Role</th>
                        </tr>
                    </tfoot>--%>
                </table>
            </div>
            <!-- Modal -->

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modify  registrars</h4>
                        </div>
                        <div class="modal-body">
                            <form id="addUserForm" action="#" method="post">
                                <div class="login-fields">
                                    <div class="field">
                                        <label for="username">User ID:</label>
                                        <input type="text" id="uid" name="uid" value="" class="login username-field form-control" />
                                    </div>
                                    <div class="field">
                                        <label for="username">Email ID:</label>
                                        <input type="text" id="Uname" name="Uname" value="" class="login username-field form-control" />
                                    </div>
                                    <div class="field">
                                        <label for="username">Event name:</label>
                                        <asp:DropDownList ID="ddlEvents" class="login username-field form-control" runat="server"></asp:DropDownList>
                                        <label id="ddleventerror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="username">Custom password:</label>
                                        <input type="password" id="password" name="password" value="" class="login username-field form-control" />
                                        <label id="cpwderror"></label>
                                    </div>
                                    <div class="field">
                                        <label for="password">Re-enter custom password</label>
                                        <input type="password" id="recpassword" name="recustompassword" value="" class="login username-field form-control" />
                                        <label id="rpwderror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="password">From:</label>
                                        <input type="text" id="s_date" name="s_date" value="" class="login password-field form-control" />
                                        <label id="sdateerror"></label>
                                    </div>
                                    <div class="field col-md-6">
                                        <label for="password">To:</label>
                                        <input type="text" id="e_date" name="e_date" value="" class="login password-field form-control" />
                                        <label id="edateerror"></label>
                                    </div>
                                    <%--<div class="field" style="margin-top: 2%;">
                                        <label for="password">Status:</label>
                                        <input type="checkbox" id="adminchk" name="my-checkbox" data-on-text="Active" data-off-text="Inactive" checked>
                                    </div>--%>
                                </div>
                                <!-- /login-fields -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnModifyUser" class="btn btn-danger" data-dismiss="modal">Okay</button>
                            <button type="button" id="btnclose" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <div><span id="msg"></span></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- jQuery -->
    <%--<script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <%--<script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>

    <!-- data table js -->
    <script type="text/javascript" src="../Assets/js/dataTables.bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <%--<script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
    <script src="../Assets/bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <%--<script src="../Assets/dist/js/sb-admin-2.js"></script>--%>
    <!-- Nice edit js -->
    <!-- <script src="js/nicEdit-latest.js"></script>    -->

    <!-- Data tables js -->
    <script type="text/javascript" src="../Assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../Assets/js/responsive.bootstrap.min.js"></script>
    <script type="text/javascript" src="../Assets/js/dataTables.responsive.min.js"></script>

    <!-- bootstrap datepicker -->
    <script type="text/javascript" src="../Assets/js/bootstrap-datepicker.js"></script>

    <!-- bootstrap switch js -->
    <script type="text/javascript" src="../Assets/js/bootstrap-switch.min.js"></script>
    <script src="../Assets/Validations/modifyregistrar.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#modifybtn').click(function () {
                var uid = $('#modifybtn').attr('uid');

                $.ajax({
                    type: "POST",
                    url: "../SuperAdmin/RegistrarServices.asmx/GetUser",
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: "{ uid: '" + uid + "'}",
                    success: function (data) {
                        $("#Uname").val(data.d.Username);
                        $("#uid").val(data.d.UId);
                        $("#s_date").val(data.d.ValidFrom);
                        $("#e_date").val(data.d.ValidTo);
                        $('#ContentPlaceHolder1_ddlEvents').val(data.d.EventId)
                        $("#uid").prop('disabled', true);
                        $("#Uname").prop('disabled', true);
                    },
                    error: function (data) { console.log(data) }
                });
                $('#myModal').modal('show')
            });
            $("#modifybtn").hide();
            $("#btnModifyUser").click(function () {

                //var flag = true;

                //if ($("#ContentPlaceHolder1_ddlEvents").val() == '0' || $("#ContentPlaceHolder1_ddlEvents").val() == null) {
                //    $('#ContentPlaceHolder1_ddlEvents').css('border-color', 'red');
                //    $('#ddleventerror').fadeIn(1000);
                //    $('#ddleventerror').html('Event can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#ContentPlaceHolder1_ddlEvents').css('border-color', ''); }

                //if ($("#password").val() == '' || $("#password").val() == null) {
                //    $('#password').css('border-color', 'red');
                //    $('#cpwderror').fadeIn(1000);
                //    $('#cpwderror').html('Custom password can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#cpwderror').css('border-color', ''); }


                //if ($("#recpassword").val() == '' || $("#recpassword").val() == null) {
                //    $('#recpassword').css('border-color', 'red');
                //    $('#rpwderror').fadeIn(1000);
                //    $('#rpwderror').html('Re enter password can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#recpassword').css('border-color', ''); }

                //if ($("#s_date").val() == '' || $("#s_date").val() == null) {
                //    $('#s_date').css('border-color', 'red');
                //    $('#sdateerror').fadeIn(1000);
                //    $('#sdateerror').html('Start date can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#s_date').css('border-color', ''); }

                //if ($("#e_date").val() == '' || $("#e_date").val() == null) {
                //    $('#e_date').css('border-color', 'red');
                //    $('#edateerror').fadeIn(1000);
                //    $('#edateerror').html('End date can not be empty!').css('color', 'red').fadeOut(10000);
                //    flag = false;
                //}
                //else { $('#e_date').css('border-color', ''); }
                var flag = validatemodifyregistrar();
                if (!flag) {
                    return flag;
                }
                else {
                    var uid = $('#modifybtn').attr('uid');
                    var eventid = $('#ContentPlaceHolder1_ddlEvents').val();
                    $.ajax({
                        type: "POST",
                        url: "../SuperAdmin/RegistrarServices.asmx/ModifyUser",
                        dataType: 'json',
                        traditional: true,
                        contentType: 'application/json; charset=utf-8',
                        data: "{ uid: '" + uid + "', uname:'" + $("#Uname").val() + "', isregistrar:'" + $('#adminchk').is(':checked') + "', password:'" + $("#password").val() + "', eventid: '" + eventid + "', from: '" + $("#s_date").val() + "', to: '" + $("#e_date").val() + "'}",
                        success: function (data) {
                            if (data.d == 'User Name is exist!') {
                                $('#msg').val(data.d);

                                $('#myModal').show();
                            }
                            else {
                                $("#uid").val('');
                                $("#Uname").val('');
                                $("#password").val('');
                                $('#adminchk').prop('checked', true);
                                console.log(data);
                                location.reload();
                                //$('#empTable').DataTable().ajax.reload();
                                //$('#empTable').dataTable().fnDestroy();
                                //$('#empTable').dataTable();
                            }
                        },
                        error: function (data) { console.log(data) }
                    });
                }
            });
            $("#btnclose").click(function () {
                $('#ContentPlaceHolder1_ddlEvents').css('border-color', '');
                $('#cpassword').css('border-color', '');
                $('#recpassword').css('border-color', '');
                $('#s_date').css('border-color', '');
                $('#e_date').css('border-color', '');
            });
        });


        $('#empTable').DataTable({
            columns: [
                { 'data': 'UId' },
               { 'data': 'Username' },
               { 'data': 'EventName' },
               { 'data': 'ValidFrom' },
               { 'data': 'ValidTo' },
               { 'data': 'UserType' }
            ],
            bServerSide: true,
            bProcessing: true,
            bSortable: true,
            dom: 'Bfrtip',
            sAjaxSource: '../SuperAdmin/RegistrarServices.asmx/LoadRegistrar',
            sServerMethod: 'Post',
            aoColumnDefs: [
        { "data": 'UId', "bSortable": true },
        { "data": 'Username', "bSortable": true },
        { "data": 'EventName', "bSortable": false },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'ValidFrom', "bSortable": true },
        { "data": 'UserType', "bSortable": true }
            ]
        });

        var table = $('#empTable').dataTable();
        $('#empTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                $("#modifybtn").hide();
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var ss = $(this).children('td:first-child').text();
                $("#modifybtn").attr('uid', ss);
                $("#modifybtn").show();
            }
        });
        $("[name='my-checkbox']").bootstrapSwitch();

        $('#s_date').datepicker();
        $('#e_date').datepicker();

    </script>

</asp:Content>


