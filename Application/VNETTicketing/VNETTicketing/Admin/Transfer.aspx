﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="VNETTicketing.Admin.Transfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row dashboard">
                <div class="col-lg-12">
                    <h1 class="page-header">Transfer tickets</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                
                <div class="row">
                    <div class="field">
                        <div class="field">
                            <div class="col-md-8 col-xs-8">
                                <%--<input type="text" id="barcode" name="barcode" value="" placeholder="Barcode" disabled class="login username-field form-control" />--%>
                                <asp:TextBox ID="txtAdminBarcode" runat="server" placeholder="Barcode" CssClass="login username-field form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAdminBarcode" runat="server" ErrorMessage="Please scan the tickets!"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <%--<button class="btn btn-primary" id="scan">Scan</button>--%>
                                <asp:Button ID="btnadmintransfer" runat="server" OnClick="btnadmintransfer_Click" CssClass="btn btn-primary" Text="Scan"/>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row pdetails">
        <p id="pInfo" runat="server" class="lead text-info"></p>
        <asp:Panel ID="pnladmintrasfer" runat="server" Visible ="false">
            
            <div class="row">
                <div class="container-fluid padding_null page_content" id="DivIdToPrint">
                    <h2 class="page-header">Booking Details</h2>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Booking ID</td>
                                <td>
                                    <asp:Label Text="" ID="lblBookingId" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Event</td>
                                <td>
                                    <asp:Label Text="" ID="lblEvent" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Venue</td>
                                <td>
                                    <asp:Label Text="" ID="lblVenue" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Event Datetime</td>
                                <td>
                                    <asp:Label Text="" ID="lblEventDateTime" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Number of ticket</td>
                                <td>
                                    <asp:Label Text="" ID="lblNoOfTicket" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td>
                                    <asp:Label Text="" ID="lblTotalAmount" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Discount</td>
                                <td>
                                    <asp:Label Text="" ID="lblDiscount" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Net Amount</td>
                                <td>
                                    <asp:Label Text="" ID="lblNetAmount" runat="server" /></td>
                            </tr>

                            <tr>
                                <td>Mode of payment</td>
                                <td>
                                    <asp:Label Text="" ID="lblModeOfPayment" runat="server" /></td>
                            </tr>

                            <tr>
                                <td>Status</td>
                                <td>
                                    <asp:Label Text="" ID="lblStatus" runat="server" /></td>
                            </tr>
                        </tbody>
                    </table>

                    <h2 class="page-header">User Details</h2>
                    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered">
                        <Columns>
                            <asp:BoundField DataField="TicketId" HeaderText="Ticket Id" />
                            <asp:BoundField DataField="IrId" HeaderText="Ir Id" />
                            <asp:BoundField DataField="UName" HeaderText="Name" />
                            <asp:BoundField DataField="TicketType" HeaderText="Ticket Type" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="ValidationStatus" HeaderText="Validation Status" />

                        </Columns>
                    </asp:GridView>

                </div>
            
            </div>
            <div class="row">
                <%--<asp:Button ID="btnValidate"  CssClass="btn btn-primary"  runat="server" Text="Validate"  />
                <asp:Button ID="btnInvalidate"  CssClass="btn btn-primary"  runat="server" Text="Invalidate" />--%>
                            <div class="field col-md-4">
                <label for="price">Available Event:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                </asp:DropDownList>

            </div>
            <div class="field col-md-2">
                <br />
    <asp:Label ID="lblTicketQty" runat="server"></asp:Label>
            </div>
            </div>
            <br />
            <div class="row">
                <div class="submit_div">
                    
                
                    <%--<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button>--%>
                    <asp:Button Text="Transfer Ticket" ID="btnTransfer" CssClass="btn btn-primary" runat="server" OnClick="btnTransfer_Click" />
                    <button class="btn btn-danger">Cancel</button>
                <center></center>
            </div>
            </div>
        </asp:Panel>
    </div>


                <%--<div class="row pdetails">
                    <p class="lead text-success">Successfully verified</p>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Participant details</div>
                                <div class="panel-body">
                                    <p><b>Full name</b> : John Alex</p>
                                    <p><b>IR ID</b> : XXXXXXXX23456</p>
                                    <p><b>Ticket number</b> : XXXXXXXX57823</p>
                                    <p><b>Group name</b> : Group 1</p>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Participant details</div>
                                <div class="panel-body">
                                    <p><b>Full name</b> : Mister X</p>
                                    <p><b>IR ID</b> : XXXXXXXX123456</p>
                                    <p><b>Ticket number</b> : XXXXXXXX098765</p>
                                    <p><b>Group name</b> : Group 2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
<%--            <div class="row submit_div">
                <center>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button>
                    <button class="btn btn-danger">Cancel</button>
                </center>
            </div>--%>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Transferee details</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="field">
                                    <div class="col-md-8 col-xs-8">
                                        <input type="text" id="Text1" name="barcode" value="" placeholder="IR ID" class="login username-field form-control" />
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <button class="btn btn-primary" type="button" name="verify">Verify</button>
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Full name</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" id="Text2" name="barcode" value="" placeholder="Full name" disabled class="login username-field form-control" />
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Email Address</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <input type="text" id="Text3" name="barcode" value="" placeholder="Email Address" disabled class="login username-field form-control" />
                                    </div>
                                </div>
                                <div class="field vdetails">
                                    <div class="col-md-6 col-xs-6">
                                        <label>Transfer fee</label>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <p>$ 60</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="window.location.href='congratulations-transfer.html'">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- jQuery -->
    <script src="../Assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../Assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../Assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../Assets/dist/js/sb-admin-2.js"></script>
    <script>
        $(document).ready(function () {
            //$('.pdetails').hide();
            //$("#scan").click(function () {
            //    //alert("skdhzhlk,.mn");
            //    $(".pdetails").show();
            //});
        });
        document.getElementById('conf').onchange = function () {
            document.getElementById('barcode').disabled = !this.checked;
        };
    </script>

</asp:Content>
