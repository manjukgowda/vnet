﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using VNETTicketing.Models.SuperAdmin;
using VNETTicketing.Models.User;

namespace VNETTicketing.Admin
{
    public partial class GroupBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeEventPage();
            }
        }

        private void InitializeEventPage()
        {
            BindEvents();
        }

        private void BindEvents()
        {
            NewBookingClass bookingObj = new NewBookingClass();
            ddlevents.Items.Clear();
            EventClass obj = new EventClass();
            
            var events = obj.GetAllEventsToServices();
            var usr = SessionUsers.GetCurrentUser(2);
            
            ddlevents.DataSource = events;
            ddlevents.DataTextField = "EventName";
            ddlevents.DataValueField = "EventId";
            ddlevents.DataBind();
            if (events.Count == 0)
            {
                ddlevents.Items.Insert(0, new ListItem("-Select-", "0"));
            }
            else
            {
                LoadTicketPrice(Convert.ToInt32(ddlevents.SelectedValue));
                LoadNoOfTickets(Convert.ToInt32(ddlevents.SelectedValue));
            }
        }

        protected void ddlevents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = Convert.ToInt32(ddlevents.SelectedValue);
            LoadTicketPrice(eventid);
            LoadNoOfTickets(eventid);
        }

        public void LoadNoOfTickets(int eventid)
        {
            EventClass obj = new EventClass();
            int available = obj.GetAvailableTicketsCount(eventid);
            if (available > 0)
            {
                lblTicketQty.Text = available + " tickets available for this event";
                lblTicketQty.ForeColor = Color.Green;
            }
            else
            {
                lblTicketQty.Text = "No tickets available for this event";
                lblTicketQty.ForeColor = Color.Red;
            }
        }

        private void LoadTicketPrice(int eventId)
        {
            NewBookingClass obj = new NewBookingClass();
            var tickets = obj.GetTicketTypes(eventId);
            ddlTicketTypes.DataSource = tickets;
            ddlTicketTypes.DataTextField = "TypeOfTicket";
            ddlTicketTypes.DataValueField = "PriceId";
            ddlTicketTypes.DataBind();

            if (tickets.Count == 0)
            {
                ddlTicketTypes.Items.Insert(0, new ListItem("-Select-", "0"));
                txtPrice.Text = "";
            }
            else
            {
                GetTicketPrice(eventId, Convert.ToInt32(ddlTicketTypes.SelectedValue));
            }

        }

        private void GetTicketPrice(int eventId, int priceId)
        {
            NewBookingClass obj = new NewBookingClass();

            var prices = obj.GetTicketTypes(eventId, priceId);
            if (prices.Count == 1)
            {
                txtPrice.Text = prices[0].PriceWithCurrency; hdncurrencytypeid.Value = prices[0].CurrencyTypeId.ToString();
            }
            else
            {
                txtPrice.Text = ""; hdncurrencytypeid.Value = "1";
            }
        }

        protected void ddlTicketTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int eventid = Convert.ToInt32(ddlevents.SelectedValue);
            int priceId = Convert.ToInt32(ddlTicketTypes.SelectedValue);
            GetTicketPrice(eventid, priceId);
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            bool staus = ProcessCSVRequest();
            if(!staus)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalerror();", true);
        }

        public class CSVHeaders
        {
            public string IRID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailAddress { get; set; }
            public string Country { get; set; }
            public string CASH { get; set; }
            public string AR { get; set; }
            public string Reference { get; set; }
            public string AmountPaid { get; set; }
            public string GroupName { get; set; }
            public string TicketId { get; set; }
            public string Status { get; set; }
        }

        public bool ProcessCSVRequest()
        {
            String ext = System.IO.Path.GetExtension(file.PostedFile.FileName);
            if (ext.ToLower() == ".csv" || ext.ToLower() == "xls" || ext.ToLower() == "xlsx")
            {
                if (file.PostedFile.ContentLength > 25)
                {
                    List<CSVHeaders> list = new List<CSVHeaders>();
                    string filePath = "~\\CSV";
                    if (!Directory.Exists(Server.MapPath(filePath)))
                        Directory.CreateDirectory(Server.MapPath(filePath));
                    filePath = "~\\CSV\\" + Guid.NewGuid().ToString().Substring(1, 15) + ".csv";
                    file.PostedFile.SaveAs(Server.MapPath(filePath));

                    var reader = new StreamReader(File.OpenRead(Server.MapPath(filePath)));

                    if (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                    }

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        if (values.Length >= 10)
                        {
                            CSVHeaders header = new CSVHeaders();
                            header.IRID = values[0];
                            header.FirstName = values[1];
                            header.LastName = values[2];
                            header.EmailAddress = values[3];
                            header.Country = values[4];
                            header.CASH = values[5];
                            header.AR = values[6];
                            header.Reference = values[7];
                            header.AmountPaid = values[8];
                            header.GroupName = values[9];
                            list.Add(header);
                        }
                    }
                    Session["list"] = list;
                    GridView1.DataSource = list;
                    GridView1.DataBind();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var usr = SessionUsers.GetCurrentUser(2);
            int eventid = Convert.ToInt32(ddlevents.SelectedValue);
            EventClass objEvent = new EventClass();
            int available = objEvent.GetAvailableTicketsCount(eventid);

            VNETTicketEntities db = new VNETTicketEntities();
            List<CSVHeaders> newList = new List<CSVHeaders>();
            var list = (List<CSVHeaders>)Session["list"];
            if (available >= list.Count)
            {
                NewBookingClass obj = new NewBookingClass();
                foreach (var item in list)
                {
                    List<NewBookingClass.NewBookingDetails> bookingList = new List<NewBookingClass.NewBookingDetails>();
                    NewBookingClass.NewBookingDetails objtemp = new NewBookingClass.NewBookingDetails();
                    objtemp.IRID = item.IRID;
                    objtemp.UName = item.FirstName;
                    objtemp.EventId = Convert.ToInt32(ddlevents.SelectedValue);
                    objtemp.GroupId = obj.GetGroupId(item.GroupName);
                    objtemp.NoTicket = 1;
                    objtemp.EmailId = item.EmailAddress;
                    objtemp.Reference = item.Reference;
                    objtemp.Country = item.Country;
                    objtemp.AR = item.AR == "" ? 0 : Convert.ToInt32(item.AR);
                    objtemp.CashPaid = item.CASH == "" ? 0 : Convert.ToInt32(item.CASH);
                    int ticketTypeId = int.Parse(ddlTicketTypes.SelectedValue);
                    var ticketPrice = db.tblEventTicketTypePrices.FirstOrDefault(x => x.PriceId == ticketTypeId);
                    objtemp.Price = Convert.ToInt32(ticketPrice.Price);
                    objtemp.TicketType = ticketPrice.TypeOfTicket;
                    objtemp.UserId = Convert.ToInt32(usr.UserId);
                    objtemp.NetAmount = item.AmountPaid == "" ? 0 : Convert.ToInt32(item.AmountPaid);
                    objtemp.CurrencyMode = ticketPrice.CurrencyModeId.Value;
                    bookingList.Add(objtemp);
                    long bookingId = obj.AddUserTransaction(bookingList, Convert.ToInt32(usr.UserId), "Admin");
                    item.Status = "Confirmed";
                    item.TicketId = obj.UpdateBookingDetails(bookingId, "Cash", "Confirmed", "", 0, objtemp.NetAmount.Value, objtemp.AR, objtemp.CashPaid);
                    newList.Add(item);
                }
                GridView1.DataSource = newList;
                GridView1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "notickets();", true);
            }
        }
    }
}