﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="NewBooking.aspx.cs" Inherits="VNETTicketing.Admin.NewBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="css/collections.css">
    <style>
        /*#barcode {
            width: 40%;
        }*/

        .submitarea
        {
            padding-bottom: 2%;
        }

        .modal-header
        {
            background-color: #f7f7f7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-12">
        <h1 class="page-header">New Booking</h1>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Event:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlevents" AutoPostBack="True" OnSelectedIndexChanged="ddlevents_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlevents" InitialValue="0"  runat="server" ErrorMessage="Please select events"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Ticket Type:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTicketTypes" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketTypes_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTicketTypes" InitialValue="0" ErrorMessage="Please select ticket type"></asp:RequiredFieldValidator>
                <asp:Label Text="" ID="lbltickettypestatus" runat="server" />
                <p class="small">Ticket price is based on ticket types</p>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="price">Price:</label>
                <asp:TextBox ID="txtPrice" runat="server"  CssClass="form-control"></asp:TextBox>
                <asp:HiddenField id="hdncurrencytypeid" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPrice" ErrorMessage="Price is required!"></asp:RequiredFieldValidator>
            </div>
        </div>
    
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">Group:</label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlGroup" ErrorMessage="Please select group" InitialValue="0"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="field">
                <label for="tickettype">No. of Tickets:</label>

                 <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTicketQuantity">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlTicketQuantity" InitialValue="0" ErrorMessage="Please select no of ticket"></asp:RequiredFieldValidator>
            </div>
            <div class="field">
                <asp:Label ID="lblTicketQty" runat="server"></asp:Label>

            </div>
        </div>
         <div class="modal-footer">
            <asp:Button ID="btnContinue" runat="server" CssClass="btn btn-primary" Text="Continue" OnClick="btnContinue_Click" />

        </div>
    </div>
    <%--<div class="row partdetails">
        <div class="col-md-8 col-xs-12" id="participantpanel">
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Payment</div>
                <div class="panel-body">
                    <p><b>Tickets</b> : 2</p>
                    <p><b>Total price</b> : $399</p>
                    <p class="lead">Payment options</p>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optradio" id="cash" value="cash">Cash</label>
                        <br>
                        <label>
                            <input type="radio" name="optradio" id="credit" value="credit">Credit/Debit card</label>
                        <br>
                        <label>
                            <input type="radio" name="optradio" id="both" value="both">Both</label>
                        <br>
                        <label>
                            <input type="radio" name="optradio" id="ar" value="ar">AR transfer</label>
                    </div>
                    <div class="row cash">
                        <div class="field col-md-12">
                            <label for="password">How much do you pay through cash?</label>
                            <div class="row">
                                <div class="col-md-5 col-xs-3">
                                    <select class="form-control">
                                        <option value="USD">$ USD</option>
                                        <option value="SGD">$ SGD</option>
                                        <option value="EUR">€ EUR</option>
                                        <option value="AUD">$ AUD</option>
                                        <option value="JPY">¥ JPY</option>
                                        <option value="CHN">¥ CHN</option>
                                        <option value="THB">฿ THB</option>
                                        <option value="MYR">RM MYR</option>
                                    </select>
                                </div>
                                <div class="col-md-7 col-xs-9">
                                    <input type="number" id="e_fee" name="event fee" value="" class="login password-field form-control" />
                                </div>
                            </div>
                            <label style="padding-top: 2%;">Balance amount:</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Remarks</div>
                <div class="panel-body">
                    <!-- Display remarks as The ticket is collected by this user on behalf of original user only if some other user collects tickets on behalf of original user. No need for remarks if ticket is collected by original user. -->
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <center>
                                        <button type="submit" class="btn btn-primary" onclick="window.location.href='congratulations-booking.html'">Submit</button>
                                        <button type="submit" class="btn btn-danger">Cancel</button>
                                    </center>
    </div>--%>
</asp:Content>
