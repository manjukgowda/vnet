﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.SuperAdmin;
using VNETTicketing.Models.GeneralUtilities;
using System.Configuration;

namespace VNETTicketing.Admin
{
    public partial class PaymentConfirmation : System.Web.UI.Page
    {
        NewBookingClass.NewBookingDetails sessdetails;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (Session["BookingId"] != null)
                if (Session["newbookingdetails"] != null)
                {
                    //long BookingId = Convert.ToInt64(Session["BookingId"]);
                    sessdetails = (NewBookingClass.NewBookingDetails)Session["newbookingdetails"];
                    //NewBookingClass obj = new NewBookingClass();
                    //var book = obj.GetBookingDetails(BookingId);
                    decimal price = Convert.ToDecimal(sessdetails.Price);
                    decimal BasePrice = CurrencyHelper.ConvertBaseCurrency(sessdetails.CurrencyMode, price);
                    decimal net = BasePrice * Convert.ToDecimal(sessdetails.NoTicket);
                    sessdetails.Price = price;
                    sessdetails.TotalAmount = net;
                    Session["newbookingdetails"] = sessdetails;

                    lblPrice.Text = Convert.ToString(price);
                    lblTickets.Text = Convert.ToString(sessdetails.NoTicket);
                    lblNetAmount.Text = Convert.ToString(net);
                }
            }
        }

        protected void btnSubmitPayment_Command(object sender, CommandEventArgs e)
        {

        }

        protected void btnSubmitPayment_Click(object sender, EventArgs e)
        {
            NewBookingClass obj = new NewBookingClass();
            sessdetails = (NewBookingClass.NewBookingDetails)Session["newbookingdetails"];
            if (ValidationOfIRIds(sessdetails.EventId) == false)
                return;
            List<NewBookingClass.NewBookingDetails> bookingList = (List<NewBookingClass.NewBookingDetails>)Session["BoolingList"];
            var usr = SessionUsers.GetCurrentUser(2);

            if (sessdetails.Discount != null && sessdetails.PromoCodeUsed != null)
            {
                for (int i = 0; i < bookingList.Count; i++)
                {
                    bookingList[i].Discount = sessdetails.Discount;
                    bookingList[i].PromoCodeUsed = sessdetails.PromoCodeUsed;
                    bookingList[i].NetAmount = sessdetails.NetAmount;
                }
            }

            long BookingId = obj.AddUserTransaction(bookingList, Convert.ToInt32(usr.UserId), "Admin");

            //long BookingId = Convert.ToInt64(Session["BookingId"]);
            var book = obj.GetBookingDetails(BookingId);
            var booking = obj.GetBookingDetailsToPrint(book.EventBookingID);

            if (rbtnCash.Checked)
            {
                int totalAmount = Convert.ToInt32(book.TotalAmount);
                int eventId = Convert.ToInt32(book.EventId);
                string promocode = txtPromoCode.Text.Trim();
                decimal DiscountUsed = book.Discount == null? 0: Convert.ToDecimal(book.Discount);
                int NetAmount = totalAmount;
                lblDiscount.Text = "0";
                lblNetAmount.Text = NetAmount + "";
                //if (obj.IsValidPromoCode(eventId, promocode))
                //{
                //    var promo = obj.GetPromoCode(eventId, promocode);
                //    int discount = Convert.ToInt32(promo.CouponValue);
                //    int minimumAmount = Convert.ToInt32(promo.MinimumAmount);

                //    if (totalAmount > minimumAmount)
                //    {
                //        NetAmount = totalAmount - discount;
                //        lblDiscount.Text = discount + " " + promo.tblCurrencyMode.CurrencyName;
                //        lblNetAmount.Text = NetAmount + " " + promo.tblCurrencyMode.CurrencyName;
                //        DiscountUsed = discount * book.NoOfTickets.Value;

                //        lblTitle.InnerHtml = "Success";
                //        lblMessage.Text = "Promocode is applied successfully!";
                //    }
                //}
                book.PaymentMode = "Cash";
                //book.PromoCodeUsed = promocode;
                //book.Discount = DiscountUsed;
                //book.NetAmountPaid = NetAmount;
                book.Status = "Confirmed";
                obj.UpdateBookingDetails(BookingId, "Cash", "Confirmed", promocode, DiscountUsed, NetAmount, 0, NetAmount);
                for (int i = 0; i < booking.BookingUserDetail.Count; i++)
                {
                    MailSender.SendTicketConfirmationEmail(book.EventBookingID, booking.BookingUserDetail[i].EventName);
                }
                Response.Redirect("../DownloadETicket.aspx");
            }
            else if (rbtncard.Checked)
            {
                for (int i = 0; i < booking.BookingUserDetail.Count; i++)
                {
                    MailSender.SendTicketConfirmationEmail(book.EventBookingID, booking.BookingUserDetail[i].EventName);
                }
            }
        }

        protected void btnValidatePromo_Click(object sender, EventArgs e)
        {
            //if (Session["BookingId"] != null)
            {
                NewBookingClass obj = new NewBookingClass();
                //long BookingId = Convert.ToInt64(Session["BookingId"]);
                //var book = obj.GetBookingDetails(BookingId);
                //int totalAmount = Convert.ToInt32(book.TotalAmount);
                //int eventId = Convert.ToInt32(book.EventId);
                string promocode = txtPromoCode.Text.Trim();

                NewBookingClass.NewBookingDetails objdetails = (NewBookingClass.NewBookingDetails)Session["newbookingdetails"];
                int totalAmount = Convert.ToInt32(objdetails.TotalAmount);

                if (obj.IsValidPromoCode(objdetails.EventId, promocode))
                {
                    var promo = obj.GetPromoCode(objdetails.EventId, promocode);
                    //int discount = Convert.ToInt32(promo.CouponValue);
                    int minimumAmount = Convert.ToInt32(promo.MinimumAmount);
                    

                    decimal baseDiscount = CurrencyHelper.ConvertBaseCurrency(promo.CouponCurrencyMode.Value, Convert.ToDecimal(promo.CouponValue));
                    decimal discount = baseDiscount * Convert.ToInt32(objdetails.NoTicket);
                    objdetails.Discount = discount;
                    if (totalAmount > minimumAmount)
                    {
                        decimal netAmount = Convert.ToDecimal(totalAmount) - discount;
                        objdetails.NetAmount = netAmount;
                        objdetails.PromoCodeUsed = promocode;
                        lblDiscount.Text = discount + " " + promo.tblCurrencyMode.CurrencyName;
                        lblNetAmount.Text = netAmount.ToString(); //+ " " + promo.tblCurrencyMode.CurrencyName;
                        lblTitle.InnerHtml = "Success";
                        lblMessage.Text = "Promocode is applied successfully!";
                    }
                    else
                    {
                        lblTitle.InnerHtml = "Alert";
                        lblMessage.Text = "This promocode is applied only for above " + minimumAmount + " " + promo.tblCurrencyMode.CurrencyName;
                    }
                    Session["newbookingdetails"] = objdetails;
                }
                else
                {
                    lblTitle.InnerHtml = "Error";
                    lblMessage.Text = "This is not a valid promo code for this event!/Sorry, limit for promo code is reached!";
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalerror();", true);
            }
        }

        private bool ValidationOfIRIds(int eventid)
        {
            List<string> IrIds = new List<string>();
            List<NewBookingClass.NewBookingDetails> bookingList = (List<NewBookingClass.NewBookingDetails>)Session["BoolingList"];

            foreach (var item in bookingList)
            {
                if (IrIds.Contains(item.IRID) == false)
                {
                    IrIds.Add(item.IRID);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('You are trying to buy more than one ticket for " + item.IRID + "!')", true);
                    return false;
                }
            }

            NewBookingClass obj = new NewBookingClass();
            foreach (var irid in IrIds)
            {
                if (obj.IsIRIdDuplicated(eventid, irid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('Ticket is already booked for " + irid + "!')", true);
                    return false;
                }
            }

            return true;
        }

    }
}