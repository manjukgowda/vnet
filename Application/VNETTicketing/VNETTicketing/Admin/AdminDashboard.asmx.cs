﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing.Admin
{
    /// <summary>
    /// Summary description for AdminDashboard
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AdminDashboard : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public DashboardStats GetAdminDashboardCounters()
        {
            DashboardHelper obj = new DashboardHelper();
            DashboardStats c  = obj.GetAdminDashboardData();
          
            return c;
        }
    }
}
