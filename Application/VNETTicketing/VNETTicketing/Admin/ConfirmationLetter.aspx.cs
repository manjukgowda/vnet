﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnBarcode.Barcode;
using VNETTicketing.Models.Admin;
using VNETTicketing.Models.GeneralUtilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;


namespace VNETTicketing.Admin
{
    public partial class ConfirmationLetter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShowDetails();
        }

        public void ShowDetails()
        {
            if (Session["BookingId"] != null)
            {
                NewBookingClass obj = new NewBookingClass();
                long BookingId = Convert.ToInt64(Session["BookingId"]);
                string bookId = obj.GetBookingDetails(BookingId).EventBookingID;

                NewBookingClass.NewBookingDetails objdetails = obj.GetBookingDetailsToPrint(bookId);

                lblBookingId.Text = objdetails.BookingId.ToString();
                lblEvent.Text = objdetails.EventName;
                lblVenue.Text = objdetails.Venue;
                lblEventDateTime.Text = objdetails.EventDateTime.ToString();
                lblNoOfTicket.Text = objdetails.NoTicket.ToString();
                lblTotalAmount.Text = Convert.ToString(objdetails.TotalAmount);
                lblDiscount.Text = Convert.ToString(objdetails.Discount);
                lblNetAmount.Text = Convert.ToString(objdetails.NetAmount);
                lblModeOfPayment.Text = objdetails.PaymentMode;
                lblStatus.Text = objdetails.Status;
                gvUsers.DataSource = objdetails.BookingUserDetail;
                gvUsers.DataBind();
                string CustomerName = "Customer";
                if (objdetails.BookingUserDetail.Count > 0)
                    CustomerName = objdetails.BookingUserDetail[0].UName;
                string EmailID = "manjumysore.k@gmail.com";
                if (objdetails.BookingUserDetail.Count > 0)
                    EmailID = objdetails.BookingUserDetail[0].EmailId;

                GenerateBarcode(BookingId.ToString(), objdetails.EventName, CustomerName, EmailID);

            }
        }

        public void GenerateBarcode(string bookingId, string eventname, string CustomerName, string EmailID)
        {

            //string vpath = "~\\Images";
            //if (!Directory.Exists(Server.MapPath(vpath)))
            //    Directory.CreateDirectory(Server.MapPath(vpath));
            //vpath = "~\\Images\\Barcodes";
            //if (!Directory.Exists(Server.MapPath(vpath)))
            //    Directory.CreateDirectory(Server.MapPath(vpath));
            //string guid = Guid.NewGuid().ToString().Substring(1, 10);
            //vpath = "~\\Images\\Barcodes\\" + guid + ".png";
            //string server = ConfigurationManager.AppSettings["server"].ToString();
            //string vpdf = "~\\Images\\Barcodespdf\\" + guid + ".pdf";

            //string fullpath = server + "/Images/Barcodes/" + guid + ".png";
            //string fullpathpdf = server + "/Images/Barcodespdf/" + guid + ".pdf";

            //BarCodeHelper obj = new BarCodeHelper();
            //obj.GenerateBarCode(bookingId, Server.MapPath(vpath));

            //imgBarCode.ImageUrl = fullpath;
            //Export(vpdf);
            //MailSender.SendTicketConfirmationEmail(bookingId,CustomerName, eventname, fullpathpdf, EmailID);
        }

        public void Export(string vpdf)
        {

            StringWriter stringWriter = new StringWriter();

            HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter);
            maindiv.RenderControl(htmlTextWriter);

            StringReader stringReader = new StringReader(stringWriter.ToString());

            Document Doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

            HTMLWorker htmlparser = new HTMLWorker(Doc);

            PdfWriter.GetInstance(Doc, new FileStream(Server.MapPath(vpdf), FileMode.Create));

            Doc.Open();

            htmlparser.Parse(stringReader);

            Doc.Close();

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}