﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VNETTicketing.Models.GeneralUtilities;

namespace VNETTicketing
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            LoginClass log = new LoginClass();
            string uname = username.Text;
            string pwd = password.Text;
            int userId = 0;
            int userTypeId = 0;
            if (log.IsValidUser(uname, pwd, ref userTypeId, ref userId))
            {
                Session["LoggedInUserId"] = userId;
                if (userTypeId == 1)
                {
                    Response.Redirect("/SuperAdmin/SADashboard.aspx");
                }
                else if (userTypeId == 2)
                {
                    Response.Redirect("/Admin/Dashboard.aspx");
                }
                else if (userTypeId == 3)
                {
                    Response.Redirect("/Registrar/Dashboard.aspx");
                }
                else if (userTypeId == 4)
                {
                    Response.Redirect("/User/Events.aspx");
                }
            }
            else
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "key", "alert('Invalid username/password')",true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalerror();", true);
            }
        }
    }
}